#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

//using namespace imago;

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
//#include <iostream.h>
#include <string.h>
//#include <iomanip.h>
//#include <fstream.h>
#include <assert.h>

#include "image.h"
//#include "median_filter.h"


#define SUCCESSFUL_RETURN 0 //2 and (-2) are also normal returns
#define UNSUCCESSFUL_RETURN (-1)


#define SQRT2 1.414213562
#define BLACK 0
#define WHITE 1

#define MASK_OUTPUT
//#define COMMENT_OUT_ALL_PRINTS

#define nLenMax 6000 
#define nWidMax 6000 

#define nLenMin 50 
#define nWidMin 50

#define nImageSizeMax (nLenMax*nWidMax)

///////////////////////////////////////////////////////////////////
#define nLarge 1000000
#define fLarge 1.0E+10 //6

#define feps 0.000001
//#define pi 3.14159
#define PI 3.1415926535

//////////////////////////////////////////////////////////////////
#define nLenSubimageToPrintMin 0 //342 
#define nLenSubimageToPrintMax 4000  //2500 //1800 //426

#define nWidSubimageToPrintMin 2010 //1580 //1930 //1906  
//#define nWidSubimageToPrintMax 2160 //1760 //2180 
#define nWidSubimageToPrintMax -1 

//#define nWidSubimageToPrintMin 9 //10 //3712 //1980 //499 //229 //415 //478 //191 //344 //193 //416 //814 
//#define nWidSubimageToPrintMax 9 //3714 //502 //416 //192 //361 //195 //240 //420
///////////////////////////////////////////////////////////////////////
#define nIntensityStatMin 10 //85 
#define nIntensityStatMinForEqualization 0 //85 

#define nIntensityStatForReadMax 65535

#define nIntensityStatMax 255 

#define nIntensityToReplaceBlackObjectPixels 20 // > nIntensityCloseToBlackMax
#define fLowestPercentageOfBlackPixelsInTheLastRow 0.4

#define fLargestPercentageOfTheLastRowsWithInsufficientNumberOfBlackPixels 0.1

//#define nNumOfHistogramBinsStat (nIntensityStatMax) //(nIntensityStatMax - nIntensityStatMin + 1)
#define nNumOfHistogramBinsStat (nIntensityStatMax + 1) //(nIntensityStatMax - nIntensityStatMin + 1)

#define nIntensityThresholdForDense 150  // > nIntensityStatMin) and < nIntensityStatMax
//////////////////////////////////////////////////////////////////

#define nLengthOfTheSamePixelIntensitiesMin 100

#define nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min 4 //40 //20 //10 //5 //3
//#define nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min 90 //350 //250 //200 //150

#define nNumOfBoundariesForOneWid_Max 2
#define nLenOfALabel_Max 400

#define fCoefDistOf1stObjectPixel_ToImageEdgeMax 0.65 //0.7 //0.57 //0.55 // 0.6 

#define nWidDiffFor1stObjectPixelVerification 110 
#define nNumOfAddedWidMax 6 //15 //10 //6 //2

#define nLenDiffFor1stObjectPixelVerification 250 


//#define nDistBetweenBoundariesOfNeighborWids_Max 150 //90 //70 //50
#define nDistBetweenBoundariesOfNeighborWids_Max 200 //

#define fShareOfWidthForTheSamePixelIntensities_1 0.5
#define fShareOfWidthForTheSamePixelIntensities_2 0.25

///////////////////////////////////////////////////////////////////////////////////////
//object
#define nIntensityCloseToBlackMax 9 //8 // < nAverIntensOfNeighboing_ObjectPixels_LongMin and < nAverIntensOfNeighboing_ObjectPixels_ShortMin

#define nLenLongOfNeighboring_ObjectPixelsFrTheSameRow 70 //40 //60

#define nLenShortOfNeighboring_ObjectPixelsFrTheSameRow 5 //6 //9 //30

#define nNumOfBlackNeighboing_ObjectPixels_LongMax 4 //5 //<=nLenLongOfNeighboring_ObjectPixelsFrTheSameRow
#define nNumOfBlackNeighboing_ObjectPixels_ShortMax 2 //3

#define nAverIntensOfNeighboing_ObjectPixels_LongMin 10 //12 // > nIntensityCloseToBlackMax
#define nAverIntensOfNeighboing_ObjectPixels_ShortMin 10 //15 // > nIntensityCloseToBlackMax

//background
#define nLenShortOfNeighboring_BackgroundPixelsFrTheSameRow 10 //20
#define nNumOf_NonBlackNeighboing_BackgroundPixels_ShortMax 3

#define nAverIntensOfNeighboing_BackgroundPixels_ShortMax 5 // < nIntensityCloseToBlackMax

/////////////////////////////////////////////////////////////

#define nWidthOfBoundaryDentMax 70 //50

#define nDiffOfLensCurAndPrevForADentMin 10 //20 
//#define nDiffOfLensCurAndPrevForADentMin 20 

#define nRedForDentFillingOut (nIntensityStatMax)
#define nGreenForDentFillingOut (nIntensityStatMax)
#define nBlueForDentFillingOut (nIntensityStatMax)

/////////////////////////////////////////////////////
//padding and interpolation
//#define nScaleForPadding 2                                                                                                       
//#define nWidScaleForPadding 100
#define nWidScaleForPadding 50
//#define nWidScaleForPadding 20 
//#define nWidScaleForPadding 10 

#define nLenForPadding 50

//for B splines
#define nNumOf_T_PointsMax 1000 //2000

//for median filter
#define nRadiusOfMedianFilter 12

#define nCoeff_DeviationFromBoundaryForMedianFilter_Into 20 //combined with nRadiusOfMedianFilter
//#define nCoeff_DeviationFromBoundaryForMedianFilter_Into 10 //5 //combined with nRadiusOfMedianFilter

#define nCoeff_DeviationFromBoundaryForMedianFilter_Out 10 //5 //combined with nRadiusOfMedianFilter

//#define nNumOfIterForMedianFilter_BondaryMax 50 //100 //400 //200 //
#define nNumOfIterForMedianFilter_BondaryMax 15 //20 

//#define nNumOfIterForMedianFilter_InsideMax 2 //15 //20 
#define nNumOfIterForMedianFilter_InsideMax 10 //30 //15 //20 

#define nNumOfIterForSmoothingMax 1 //3 //1 //2

#define nNumOfChangedPixelsMin 5

#define INCLUDE_INTERNAL_OBJECT_AREA

#define USE_SPLINES_FOR_BOUNDARY_INTERVALS

//#define USE_MEDIAN_FILTER
#define USE_MEDIAN_FILTER
/////////////////////////////////////////////////////////

typedef struct
{
	int
		nRed;

	int
		nGreen;
	int
		nBlue;

} WEIGHTED_RGB_COLORS;

typedef struct
{
	int nSideOfObjectLocation; //-1 - left, 1 - right

	int nIntensityOfBackground_Red; //
	int nIntensityOfBackground_Green; //
	int nIntensityOfBackground_Blue; //

	int nWidth;
	int nLength;

	int *nLenObjectBoundary_Arr;

	//	int nRed_Arr[nImageSizeMax];
	//	int nGreen_Arr[nImageSizeMax];
	//	int nBlue_Arr[nImageSizeMax];
	int *nRed_Arr;
	int *nGreen_Arr;
	int *nBlue_Arr;

	int *nIsAPixelBackground_Arr;

} COLOR_IMAGE;


typedef struct
{
	int nSideOfObjectLocation; //-1 - left, 1 - right

	int nWidth;
	int nLength;

	int nIs_WidthDivisibleBynScaleForPadding; //1 -- divisible, 0 -- no

	int nNumOfPaddedLengths;

	int *nScaledPaddedWid_Arr;
	int *nScaledPaddedLen_Arr;

	int *nLenObjectBoundary_Arr;

	//int *nPixel_ValidOrNotArr; //1 - valid, 0 - invalid
	int *nMaskImage_Arr; //0-object, 1-background

} MASK_IMAGE;

typedef struct
{
	float fT;

	float fCoefLenArr[4];
	float fCoefWidArr[4];

} COEFS_OF_B_SPLINE_BETWEEN_2_POINTS;

//
typedef struct
{
	int nX_Arr[4];
	int nY_Arr[4];

} FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS; //4 points are used to define a spline beween 2 central poins

typedef struct
{
	int nX_Arr[2];
	int nY_Arr[2];

} TWO_X_Y_COORDINS_FOR_LINE_BETWEEN_2_POINTS; //4 points are used to define a spline beween 2 central poins

