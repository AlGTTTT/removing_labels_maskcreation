
#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

#include "Removing_Labels_function.cpp"

int main()
{
	int doRemovingOfLabels_ColorImage(

		const Image& image_in,

		//const PARAMETERS_REMOVAL_OF_LABELS *sPARAMETERS_REMOVAL_OF_LABELSf,

		Image& image_out);

	int
		nRes;

		////////////////////////////////////////////////////////////////////////////////////////////////
/*
	fout_lr = fopen("wMain_RemovingLabels.txt", "w");

	if (fout_lr == NULL)
	{
		printf("\n\n fout_lr == NULL");
		getchar(); exit(1);
	} //if (fout_lr == NULL)
*/
	Image image_in;

	//image_in.read("01-001-0-LMLO_c.jpg");
	image_in.read("01-350-0-LMLO_Rec_c_abL.jpg");

	Image image_out;
	
	nRes = doRemovingOfLabels_ColorImage(
				image_in, // Image& image_in,

		image_out); // Image& image_out);

	if (nRes == UNSUCCESSFUL_RETURN) // -1
	{
		printf("\n\n The image has not been processed properly for label removal and mask creation.");
		printf("\n Please adjust the algorithm.");

		fprintf(fout_lr,"\n\n The image has not been processed properly for label removal and mask creation.");
		fprintf(fout_lr,"\n Please adjust the algorithm.");

		fflush(fout_lr); //debugging;

		//printf("\n\n Press any key to exit");	getchar(); 
		return UNSUCCESSFUL_RETURN; //
	} // if (nRes == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n After 'doRemovingOfLabels_ColorImage'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	image_out.write("01-350-0-LMLO_Rec_c_abL_Mask.png");
	//image_out.write("01-001-0-LMLO_c_Mask.png");

	printf("\n\n The mask image without labels has been saved");
	fprintf(fout_lr,"\n\n The mask image without labels has been saved");
	fflush(fout_lr); //debugging;

	//printf("\n\n Please press any key to exit");
	//getchar(); //exit(1);
	return SUCCESSFUL_RETURN;
} //int main()

//printf("\n\n Please press any key:"); getchar();