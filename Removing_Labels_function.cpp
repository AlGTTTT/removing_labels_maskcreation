//#include "Removing_Labels_MaskCreation_parameters.h"

#include "Removing_Labels_MaskCreation_2.h"

using namespace imago;

FILE *fout_lr;

int
	nPrintInsideShadingBoundaryOfObject_By_Padding_Points_And_B_Spline_Glob = 0, // no printing
	iIterGlob,
	nWidGlob;

int doRemovingOfLabels_ColorImage(
	const Image& image_in,

	//const PARAMETERS_REMOVAL_OF_LABELS *sParameters_Removal_Of_Labelsf,

	Image& image_out)
{

	int Initializing_Color_To_CurSize(
		const int nImageWidthf,
		const int nImageLengthf,

		COLOR_IMAGE *sColor_Imagef);

	int Initializing_MaskImage(
		const int nImageWidthf,
		const int nImageLengthf,

		const COLOR_IMAGE *sColor_Imagef,
		MASK_IMAGE *sMask_Imagef);

	int DeterminingSideOfObjectLocation_AndBackground(
		//const PARAMETERS_REMOVAL_OF_LABELS *sParameters_Removal_Of_Labelsf,

		COLOR_IMAGE *sColor_Imagef);

	int DeterminingLenOfObjectBoundary(

		COLOR_IMAGE *sColor_Imagef);

	int Erasing_Labels(

		COLOR_IMAGE *sColor_Imagef);

	int Detecting_BackgroundPixels(

		COLOR_IMAGE *sColor_Imagef);

	int FillingOut_BlackObjectPixels(

		COLOR_IMAGE *sColor_Imagef);

#ifndef USE_MEDIAN_FILTER
	int Smoothing_ObjectBoundaries(

		MASK_IMAGE *sMask_Imagef);

	int PaddedLengthsForMask(
		MASK_IMAGE *sMask_Imagef);

	int ShadingBoundaryOfObject_By_Padding_Points_And_B_Spline(
		MASK_IMAGE *sMask_Imagef);

	void FillingOut_ObjectPaddedAreaByWhitePixels(
		MASK_IMAGE *sMask_Imagef);
#endif //#ifndef USE_MEDIAN_FILTER

#ifdef USE_MEDIAN_FILTER
	void MedianFiterTo_BoundaryArea(

		int &nNumOfChangedPixelsCloseToBoundaryTotf,
		MASK_IMAGE *sMask_Imagef);

	int MedianFiterTo_BlackAreaBehindBoundary(

		int &nWidToStartForBlackAreaBehindBoundaryf,

		int &nWid_BlackPixelsBehindBoundaryMinf,
		int &nWid_BlackPixelsBehindBoundaryMaxf,

		int &nNumOfChangedPixelsBehindBoundaryTotf,

		MASK_IMAGE *sMask_Imagef);
#endif // #ifdef USE_MEDIAN_FILTER

	int
		nRes;

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	int
		i,
		j,

		//nSizeOfImage, // = nImageWidth*nImageHeight,

		nIndexOfPixelCur,

		iLen,
		iWid,

		nRed,
		nGreen,
		nBlue,

		nIntensityOfMaskImage,
		nIntensity_Read_Test_ImageMax = -nLarge,

#ifdef USE_MEDIAN_FILTER
		iIte_MedFilf,
		nWid_BlackPixelsBehindBoundaryMaxf, //int &nWid_BlackPixelsBehindBoundaryMaxf,

		nNumOfChangedPixelsBehindBoundaryTotf, //int &nNumOfChangedPixelsBehindBoundaryTotf,
		nWidToStartForBlackAreaBehindBoundaryf,
		nNumOfChangedPixelsCloseToBoundaryTotf,
		nWid_BlackPixelsBehindBoundaryMinf, //int &nWid_BlackPixelsBehindBoundaryMinf,
#endif // USE_MEDIAN_FILTER


#ifndef USE_MEDIAN_FILTER
		iIte_Smoothf,
#endif // #ifndef USE_MEDIAN_FILTER

		nImageWidth,
		nImageHeight;
	////////////////////////////////////////////////////////////////////////
	fout_lr = fopen("wMain_Remov_LabelsMaskCreation.txt", "w");

	if (fout_lr == NULL)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error: fout_lr == NULL");
		fprintf(fout_lr, "\n\n An error: fout_lr == NULL");

		//getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (fout_lr == NULL)

	  // size of image
	nImageWidth = image_in.width();

	nImageWidth = image_in.width();
	nImageHeight = image_in.height();

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);
	fprintf(fout_lr, "\n\n nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nImageWidth > nLenMax || nImageWidth < nLenMin)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in reading the image width: nImageWidth = %d", nImageWidth);
		printf("\n\n An error in reading the image width: nImageWidth = %d", nImageWidth);
		//getchar(); exit(1);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nImageWidth > nLenMax || nImageWidth < nLenMin)

	if (nImageHeight > nWidMax || nImageHeight < nWidMin)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in reading the image height: nImageHeight = %d", nImageHeight);
		fprintf(fout_lr, "\n\n An error in reading the image height: nImageHeight = %d", nImageHeight);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nImageHeight > nWidMax || nImageHeight < nWidMin)

	int bytesOfWidth = image_in.pitchInBytes();

	Image imageToSave(nImageWidth, nImageHeight, bytesOfWidth);

	///////////////////////////////////////////////////////
	COLOR_IMAGE sColor_Image; //

	nRes = Initializing_Color_To_CurSize(
		nImageHeight, //const int nImageWidthf,
		nImageWidth, //const int nImageLengthf,

		&sColor_Image); // COLOR_IMAGE *sColor_Imagef);

	//nSizeOfImage = nImageWidth*nImageHeight;

	//finding 'nIntensity_Read_Test_ImageMax' to decide if the intensity rescaling to (0,255) is needed
	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j*nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)
			{
#ifndef COMMENT_OUT_ALL_PRINTS

				fprintf(fout_lr, "\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
				printf("\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
				//printf("\n\n Please press any key to exit");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} // if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)

			if (nRed > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nRed;

			if (nGreen > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nGreen;

			if (nBlue > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nBlue;

		} // for (int i = 0; i < nImageWidth; i++)

	}//for (int j = 0; j < nImageHeight; j++)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
	fprintf(fout_lr, "\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	////////////////////////////////////////////////////////////////////////////////////////////

	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j*nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			iLen = i;
			iWid = j;

			if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			{
				//rescaling

				sColor_Image.nRed_Arr[nIndexOfPixelCur] = nRed / 256;

				sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nGreen / 256;
				sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nBlue / 256;

				//////////////////////////////////////////////////////////////////////////////////////////////////////
				sColor_Image.nRed_Arr[nIndexOfPixelCur] = nRed;
				sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nGreen;
				sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nBlue;

			} //if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			else
			{
				// no rescaling

				sColor_Image.nRed_Arr[nIndexOfPixelCur] = nRed;
				sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nGreen;
				sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nBlue;
			} //else

		} // for (int i = 0; i < nImageWidth; i++)
	}//for (int j = 0; j < nImageHeight; j++)

	 ///////////////////////////////////////////////////////////////////////////
	nRes = DeterminingSideOfObjectLocation_AndBackground(

		&sColor_Image); //COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		delete[] sColor_Image.nRed_Arr;
		delete[] sColor_Image.nGreen_Arr;
		delete[] sColor_Image.nBlue_Arr;

		delete[] sColor_Image.nLenObjectBoundary_Arr;
		delete[] sColor_Image.nIsAPixelBackground_Arr;

		return UNSUCCESSFUL_RETURN;
	}// if (nRes == UNSUCCESSFUL_RETURN)

	nRes = DeterminingLenOfObjectBoundary(

		&sColor_Image); //COLOR_IMAGE *sColor_Imagef) //[nImageSizeMax]

	if (nRes == UNSUCCESSFUL_RETURN) // -1
	{
		delete[] sColor_Image.nRed_Arr;
		delete[] sColor_Image.nGreen_Arr;
		delete[] sColor_Image.nBlue_Arr;

		delete[] sColor_Image.nLenObjectBoundary_Arr;
		delete[] sColor_Image.nIsAPixelBackground_Arr;

		return UNSUCCESSFUL_RETURN;
	}// if (nRes ==0)

	 //////////////////////////////////////////////////////////
	nRes = Erasing_Labels(

		&sColor_Image); //COLOR_IMAGE *sColor_Imagef);
						/////////////////////////////////////////////////////////////////////////////////////////////

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		delete[] sColor_Image.nRed_Arr;
		delete[] sColor_Image.nGreen_Arr;
		delete[] sColor_Image.nBlue_Arr;

		delete[] sColor_Image.nLenObjectBoundary_Arr;
		delete[] sColor_Image.nIsAPixelBackground_Arr;

		return UNSUCCESSFUL_RETURN;
	}// if (nRes == UNSUCCESSFUL_RETURN)

	nRes = Detecting_BackgroundPixels(

		&sColor_Image); //COLOR_IMAGE *sColor_Imagef)
						/////////////////////////////////////////////////////////////////////////////////////////////
	if (nRes == UNSUCCESSFUL_RETURN)
	{
		delete[] sColor_Image.nRed_Arr;
		delete[] sColor_Image.nGreen_Arr;
		delete[] sColor_Image.nBlue_Arr;

		delete[] sColor_Image.nLenObjectBoundary_Arr;
		delete[] sColor_Image.nIsAPixelBackground_Arr;

		return UNSUCCESSFUL_RETURN;
	}// if (nRes == UNSUCCESSFUL_RETURN)

	nRes = FillingOut_BlackObjectPixels(

		&sColor_Image); //COLOR_IMAGE *sColor_Imagef)

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		delete[] sColor_Image.nRed_Arr;
		delete[] sColor_Image.nGreen_Arr;
		delete[] sColor_Image.nBlue_Arr;

		delete[] sColor_Image.nLenObjectBoundary_Arr;
		delete[] sColor_Image.nIsAPixelBackground_Arr;

		return UNSUCCESSFUL_RETURN;
	}// if (nRes == UNSUCCESSFUL_RETURN)
	 /////////////////////////////////////////////////////////////////////////////////////////////
	MASK_IMAGE sMask_Image;

	nRes = Initializing_MaskImage(
		nImageHeight, //const int nImageWidthf,
		nImageWidth, //const int nImageLengthf,

		&sColor_Image, // COLOR_IMAGE *sColor_Imagef);
		&sMask_Image); // MASK_IMAGE *sMask_Imagef);

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		delete[] sColor_Image.nRed_Arr;
		delete[] sColor_Image.nGreen_Arr;
		delete[] sColor_Image.nBlue_Arr;

		delete[] sColor_Image.nLenObjectBoundary_Arr;
		delete[] sColor_Image.nIsAPixelBackground_Arr;

		/////////////////////////////////////////////////
		delete[] sMask_Image.nScaledPaddedWid_Arr;
		delete[] sMask_Image.nScaledPaddedLen_Arr;

		delete[] sMask_Image.nLenObjectBoundary_Arr;
		delete[] sMask_Image.nMaskImage_Arr;

		return UNSUCCESSFUL_RETURN;
	}// if (nRes == UNSUCCESSFUL_RETURN)

#ifdef USE_MEDIAN_FILTER
	nWidToStartForBlackAreaBehindBoundaryf = 0;

	for (iIte_MedFilf = 0; iIte_MedFilf < nNumOfIterForMedianFilter_BondaryMax; iIte_MedFilf++)
	{
		iIterGlob = iIte_MedFilf;
		MedianFiterTo_BoundaryArea(

			nNumOfChangedPixelsCloseToBoundaryTotf, //int &nNumOfChangedPixelsCloseToBoundaryTotf,
			&sMask_Image); // MASK_IMAGE *sMask_Imagef);

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n iIte_MedFilf = %d, nNumOfChangedPixelsCloseToBoundaryTotf = %d", iIte_MedFilf, nNumOfChangedPixelsCloseToBoundaryTotf);

		fprintf(fout_lr, "\n\n iIte_MedFilf = %d, nNumOfChangedPixelsCloseToBoundaryTotf = %d", iIte_MedFilf, nNumOfChangedPixelsCloseToBoundaryTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		if (nNumOfChangedPixelsCloseToBoundaryTotf < nNumOfChangedPixelsMin)
			break;
	}//for (iIte_MedFilf = 0; iIte_MedFilf < nNumOfIterForMedianFilter_BondaryMax; iIte_MedFilf++)

	 ///////////////////////////////////////////////////////////////////////////
	for (iIte_MedFilf = 0; iIte_MedFilf < nNumOfIterForMedianFilter_InsideMax; iIte_MedFilf++)
	{
		iIterGlob = iIte_MedFilf;

		nRes = MedianFiterTo_BlackAreaBehindBoundary(

			nWidToStartForBlackAreaBehindBoundaryf, //int &nWidToStartForBlackAreaBehindBoundaryf,

			nWid_BlackPixelsBehindBoundaryMinf, //int &nWid_BlackPixelsBehindBoundaryMinf,
			nWid_BlackPixelsBehindBoundaryMaxf, //int &nWid_BlackPixelsBehindBoundaryMaxf,

			nNumOfChangedPixelsBehindBoundaryTotf, //int &nNumOfChangedPixelsBehindBoundaryTotf,
			&sMask_Image); // MASK_IMAGE *sMask_Imagef);

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n iIte_MedFilf = %d, nWidToStartForBlackAreaBehindBoundaryf = %d, nNumOfChangedPixelsBehindBoundaryTotf = %d",
			iIte_MedFilf, nWidToStartForBlackAreaBehindBoundaryf, nNumOfChangedPixelsBehindBoundaryTotf);

		printf("\n nWid_BlackPixelsBehindBoundaryMinf = %d, nWid_BlackPixelsBehindBoundaryMaxf = %d",
			nWid_BlackPixelsBehindBoundaryMinf, nWid_BlackPixelsBehindBoundaryMaxf);

		fprintf(fout_lr, "\n\n iIte_MedFilf = %d, nWidToStartForBlackAreaBehindBoundaryf = %d, nNumOfChangedPixelsBehindBoundaryTotf = %d",
			iIte_MedFilf, nWidToStartForBlackAreaBehindBoundaryf, nNumOfChangedPixelsBehindBoundaryTotf);

		fprintf(fout_lr, "\n nWid_BlackPixelsBehindBoundaryMinf = %d, nWid_BlackPixelsBehindBoundaryMaxf = %d",
			nWid_BlackPixelsBehindBoundaryMinf, nWid_BlackPixelsBehindBoundaryMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		if (nRes == -2)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n iIte_MedFilf = %d : break by nRes == -2, no black pixels behind the boundary", iIte_MedFilf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		} //if (nRes == -2)

		if (nNumOfChangedPixelsBehindBoundaryTotf < nNumOfChangedPixelsMin)
			break;
	}//for (iIte_MedFilf = 0; iIte_MedFilf < nNumOfIterForMedianFilter_InsideMax; iIte_MedFilf++)

#endif //#ifdef USE_MEDIAN_FILTER


#ifndef USE_MEDIAN_FILTER

	for (iIte_Smoothf = 0; iIte_Smoothf < nNumOfIterForSmoothingMax; iIte_Smoothf++)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n The smoothing iterations: iIte_Smoothf = %d", iIte_Smoothf);
		fprintf(fout_lr, "\n\n The smoothing iterations: iIte_Smoothf = %d", iIte_Smoothf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS


		nRes = Smoothing_ObjectBoundaries(
			&sMask_Image); // MASK_IMAGE *sMask_Imagef);

		if (nRes == UNSUCCESSFUL_RETURN)
		{
			delete[] sColor_Image.nRed_Arr;
			delete[] sColor_Image.nGreen_Arr;
			delete[] sColor_Image.nBlue_Arr;

			delete[] sColor_Image.nLenObjectBoundary_Arr;
			delete[] sColor_Image.nIsAPixelBackground_Arr;

			/////////////////////////////////////////////////
			delete[] sMask_Image.nScaledPaddedWid_Arr;
			delete[] sMask_Image.nScaledPaddedLen_Arr;

			delete[] sMask_Image.nLenObjectBoundary_Arr;
			delete[] sMask_Image.nMaskImage_Arr;

			return UNSUCCESSFUL_RETURN;
		}// if (nRes == UNSUCCESSFUL_RETURN)

	} //for (iIte_Smoothf = 0; iIte_Smoothf < nNumOfIterForSmoothingMax; iIte_Smoothf++)


	nRes = PaddedLengthsForMask(
		&sMask_Image); // MASK_IMAGE *sMask_Imagef);

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		delete[] sColor_Image.nRed_Arr;
		delete[] sColor_Image.nGreen_Arr;
		delete[] sColor_Image.nBlue_Arr;

		delete[] sColor_Image.nLenObjectBoundary_Arr;
		delete[] sColor_Image.nIsAPixelBackground_Arr;

		/////////////////////////////////////////////////
		delete[] sMask_Image.nScaledPaddedWid_Arr;
		delete[] sMask_Image.nScaledPaddedLen_Arr;

		delete[] sMask_Image.nLenObjectBoundary_Arr;
		delete[] sMask_Image.nMaskImage_Arr;

		return UNSUCCESSFUL_RETURN;
	}// if (nRes == UNSUCCESSFUL_RETURN)

	nRes = ShadingBoundaryOfObject_By_Padding_Points_And_B_Spline(
		&sMask_Image); // MASK_IMAGE *sMask_Imagef);

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		delete[] sColor_Image.nRed_Arr;
		delete[] sColor_Image.nGreen_Arr;
		delete[] sColor_Image.nBlue_Arr;

		delete[] sColor_Image.nLenObjectBoundary_Arr;
		delete[] sColor_Image.nIsAPixelBackground_Arr;

		/////////////////////////////////////////////////
		delete[] sMask_Image.nScaledPaddedWid_Arr;
		delete[] sMask_Image.nScaledPaddedLen_Arr;

		delete[] sMask_Image.nLenObjectBoundary_Arr;
		delete[] sMask_Image.nMaskImage_Arr;

		return UNSUCCESSFUL_RETURN;
	}// if (nRes == UNSUCCESSFUL_RETURN)

	FillingOut_ObjectPaddedAreaByWhitePixels(
		&sMask_Image); // MASK_IMAGE *sMask_Imagef);

#endif //#ifndef USE_MEDIAN_FIL
					   
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef MASK_OUTPUT 
 // Save to file
	for (j = 0; j < imageToSave.height(); j++)
	{
		for (i = 0; i < imageToSave.width(); i++)
		{
			iLen = i;
			iWid = j;

			nIndexOfPixelCur = iLen + iWid*nImageWidth;

			nRed = sColor_Image.nRed_Arr[nIndexOfPixelCur];
			nGreen = sColor_Image.nGreen_Arr[nIndexOfPixelCur];
			nBlue = sColor_Image.nBlue_Arr[nIndexOfPixelCur];

			imageToSave(j, i, R) = nRed;
			imageToSave(j, i, G) = nGreen;
			imageToSave(j, i, B) = nBlue;

			imageToSave(j, i, A) = image_in(j, i, A);
			//image_out(j, i, A) = image_in(j, i, A);

		} // for (i = 0; i < imageToSave.width(); i++)

	} // for (j = 0; j < imageToSave.height(); j++)
#endif // #ifndef MASK_OUTPUT

#ifdef MASK_OUTPUT 
	  // Save to file
	for (j = 0; j < imageToSave.height(); j++)
	{
		for (i = 0; i < imageToSave.width(); i++)
		{
			iLen = i;
			iWid = j;

			nIndexOfPixelCur = iLen + iWid*nImageWidth;

			nIntensityOfMaskImage = sMask_Image.nMaskImage_Arr[nIndexOfPixelCur];

			if (nIntensityOfMaskImage == 1)
			{
				imageToSave(j, i, R) = nIntensityStatMax; // nRed;
				imageToSave(j, i, G) = nIntensityStatMax, //nGreen;
					imageToSave(j, i, B) = nIntensityStatMax; // nBlue;
			} //if (nRed > sColor_Image.nIntensityOfBackground_Red || nGreen > sColor_Image.nIntensityOfBackground_Green ||
			else
			{
				imageToSave(j, i, R) = sColor_Image.nIntensityOfBackground_Red;
				imageToSave(j, i, G) = sColor_Image.nIntensityOfBackground_Green;
				imageToSave(j, i, B) = sColor_Image.nIntensityOfBackground_Blue;

			}//else

			imageToSave(j, i, A) = image_in(j, i, A);
			//image_out(j, i, A) = image_in(j, i, A);

		} // for (i = 0; i < imageToSave.width(); i++)

	} // for (j = 0; j < imageToSave.height(); j++)

#endif // #ifdef MASK_OUTPUT
	  ////////////////////////////////////////////////////////////////////////////

	image_out = imageToSave;

	delete[] sColor_Image.nRed_Arr;
	delete[] sColor_Image.nGreen_Arr;
	delete[] sColor_Image.nBlue_Arr;

	delete[] sColor_Image.nLenObjectBoundary_Arr;
	delete[] sColor_Image.nIsAPixelBackground_Arr;

	/////////////////////////////////////////////////
	delete[] sMask_Image.nScaledPaddedWid_Arr;
	delete[] sMask_Image.nScaledPaddedLen_Arr;

	delete[] sMask_Image.nLenObjectBoundary_Arr;
	delete[] sMask_Image.nMaskImage_Arr;

	return SUCCESSFUL_RETURN;
} //int doRemovingOfLabels_ColorImage(...
  ///////////////////////////////////////////////////////////////////////

int DeterminingSideOfObjectLocation_AndBackground(
	COLOR_IMAGE *sColor_Imagef)

{
	int
		nIndexOfPixelCurf,
		nWidCurf = (int)((float)(sColor_Imagef->nWidth)*fShareOfWidthForTheSamePixelIntensities_1),
		iLenf,

		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		nRedPrevf,
		nGreenPrevf,
		nBluePrevf,

		nLengthOfTheSamePixelIntensitiesf;

////////////////////////////////////////////////////////////////////////////
	//left, initially
	nLengthOfTheSamePixelIntensitiesf = 0;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'DeterminingSideOfObjectLocation_AndBackground' - left\n");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
	{
		nIndexOfPixelCurf = iLenf + (nWidCurf*sColor_Imagef->nLength); // nLenMax);

		nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
		nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
		nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

		if (iLenf == 0)
		{
			nRedPrevf = nRedCurf;
			nGreenPrevf = nGreenCurf;
			nBluePrevf = nBlueCurf;

		}  //if (iLenf == 0)

		if (nRedCurf != nRedPrevf || nGreenCurf != nGreenPrevf || nBlueCurf != nBluePrevf)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n No object to the left: iLenf = %d, nWidCurf = %d, nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d", iLenf, nWidCurf, nRedCurf, nGreenCurf, nBlueCurf);
			fprintf(fout_lr, "\n nRedPrevf = %d, nGreenPrevf = %d, nBluePrevf = %d", nRedPrevf, nGreenPrevf, nBluePrevf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			break;
		} // if (nRedCurf != nRedPrevf || nGreenCurf != nGreenPrevf || nBlueCurf != nBluePrevf)

		nLengthOfTheSamePixelIntensitiesf += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n Left: iLenf = %d, nWidCurf = %d, nLengthOfTheSamePixelIntensitiesf = %d, nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d",
			iLenf, nWidCurf, nLengthOfTheSamePixelIntensitiesf, nRedCurf, nGreenCurf, nBlueCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		if (nLengthOfTheSamePixelIntensitiesf == nLengthOfTheSamePixelIntensitiesMin)
		{
			sColor_Imagef->nSideOfObjectLocation = 1; // object to the right
			sColor_Imagef->nIntensityOfBackground_Red = nRedCurf;
			sColor_Imagef->nIntensityOfBackground_Green = nGreenCurf;
			sColor_Imagef->nIntensityOfBackground_Blue = nBlueCurf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n Object to the right: Color_Imagef->nSideOfObjectLocation = 1, sColor_Imagef->nIntensityOfBackground_Red = %d, ..._Green = %d, ....Blue = %d",
				sColor_Imagef->nIntensityOfBackground_Red, sColor_Imagef->nIntensityOfBackground_Green, sColor_Imagef->nIntensityOfBackground_Blue);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return SUCCESSFUL_RETURN;
		} // if (nLengthOfTheSamePixelIntensitiesf == nLengthOfTheSamePixelIntensitiesMin)

	} //for (iLenf = 0; iLenf <  sColor_Imagef->nLength; iLenf++)
	  ////////////////////////////////////////////////////////////////////

	  //right
	nLengthOfTheSamePixelIntensitiesf = 0;
#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'DeterminingSideOfObjectLocation_AndBackground' - right\n");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)
	{
		nIndexOfPixelCurf = iLenf + (nWidCurf*sColor_Imagef->nLength); // nLenMax);

		nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
		nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
		nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

		if (iLenf == sColor_Imagef->nLength - 1)
		{
			nRedPrevf = nRedCurf;
			nGreenPrevf = nGreenCurf;
			nBluePrevf = nBlueCurf;

		}  //if (iLenf == sColor_Imagef->nLength - 1)

		if (nRedCurf != nRedPrevf || nGreenCurf != nGreenPrevf || nBlueCurf != nBluePrevf)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n No object to the right: iLenf = %d, nWidCurf = %d, nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d", iLenf, nWidCurf, nRedCurf, nGreenCurf, nBlueCurf);
			fprintf(fout_lr, "\n nRedPrevf = %d, nGreenPrevf = %d, nBluePrevf = %d", nRedPrevf, nGreenPrevf, nBluePrevf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN; // no left or right object location
		} // if (nRedCurf != nRedPrevf || nGreenCurf != nGreenPrevf || nBlueCurf != nBluePrevf)

		nLengthOfTheSamePixelIntensitiesf += 1;
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n Right: iLenf = %d, nWidCurf = %d, nLengthOfTheSamePixelIntensitiesf = %d, nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d",
			iLenf, nWidCurf, nLengthOfTheSamePixelIntensitiesf, nRedCurf, nGreenCurf, nBlueCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		if (nLengthOfTheSamePixelIntensitiesf == nLengthOfTheSamePixelIntensitiesMin)
		{
			sColor_Imagef->nSideOfObjectLocation = -1; // // object to the left
			sColor_Imagef->nIntensityOfBackground_Red = nRedCurf;
			sColor_Imagef->nIntensityOfBackground_Green = nGreenCurf;
			sColor_Imagef->nIntensityOfBackground_Blue = nBlueCurf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n Object to the left: Color_Imagef->nSideOfObjectLocation = -1, sColor_Imagef->nIntensityOfBackground_Red = %d, ..._Green = %d, ....Blue = %d",
				sColor_Imagef->nIntensityOfBackground_Red, sColor_Imagef->nIntensityOfBackground_Green, sColor_Imagef->nIntensityOfBackground_Blue);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return SUCCESSFUL_RETURN;
		} // if (nLengthOfTheSamePixelIntensitiesf == nLengthOfTheSamePixelIntensitiesMin)

	} //for (iLenf = sColor_Imagef->nLength; iLenf >= 0; iLenf--)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n An error in 'DeterminingSideOfObjectLocation_AndBackground': no left or right object location");
	fprintf(fout_lr, "\n\n An error in 'DeterminingSideOfObjectLocation_AndBackground': no left or right object location");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	return UNSUCCESSFUL_RETURN;
} //int DeterminingSideOfObjectLocation_AndBackground(...
  ////////////////////////////////////////////////////////////////////////////////

int DeterminingLenOfObjectBoundary(

	COLOR_IMAGE *sColor_Imagef)
{
	int CharacteristicsOf_ObjectNeighborsInARow(

		const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

		const int nWidCurf,
		const int nLenCurf,

		const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
		/////////////////////////////////////////////////////////////
		int &nNumOfBlackNeighboingPixelsFrTheSameRowf,
		int &nAverIntensityOverNeighboingPixelsFrTheSameRowf,
		int &nDiffBetweenMaxAndMinf);

	int Verifying_IfAPixelBelongsToANarrowBoundary(
		const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

		const int nWidCurf,
		const int nLenCurf,

		const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
		const int nLenOf_BackgroundNeighboringPixelsFrTheSameRowf);

	int
		nResf,

		nNumOfBlackNeighboingPixelsFrTheSameRowf,
		nAverIntensityOverNeighboingPixelsFrTheSameRowf,
		nDiffBetweenMaxAndMinf,
		nIs_ExitFromALinePossiblef = 0, //not possible initially

		nIndexOfPixelCurf,
		nIndexOfPixelBelowf,
		nIndexOfPixelSidef,

		//nWidCurf = (int)(sColor_Imagef->nWidth*fShareOfWidthForTheSamePixelIntensities_1),
		iWidf,
		iLenf,
		nLenSidef,

		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		nForCharacteristicsOfNeighborsf,
		nRedPrevf = 0,
		nNumOfNonZeroBeginningsInARowf = 0,
		nNumOfNonZeroEndsInARowf = 0,

		nRedBelowf,
		nGreenBelowf,
		nBlueBelowf,

		nRedSidef,
		nGreenSidef,
		nBlueSidef,

		nNumOfRightBoundariesf,
		nNumOfLeftBoundariesf,

		nLenOfObjectBoundaryCurf,

	//	nLenOf_1stObjectBoundaryCurf,
		//nLenOf_2ndObjectBoundaryCurf,

		//nIsObjectPixelBelowf = 0, // no

		//nNumOfAddedWidf = 0,

		nWidBelowf,
		nDiffOfLens_Maxf = nDistBetweenBoundariesOfNeighborWids_Max + nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min,

		nDistOf1stObjectPixel_ToImageEdgeMaxf = (int)(fCoefDistOf1stObjectPixel_ToImageEdgeMax*(float)(sColor_Imagef->nLength)),
		nNumOfNonZeroObjectBoundariesf = 0,
		nWidForfNonZeroObjectBoundariesf = nLarge,

		nDiffOfLensf,

		nDiffOfLensForBoundariesf,

		nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n //////////////////////////////////////////////////////////////////////////////");
	fprintf(fout_lr, "\n\n 'DeterminingLenOfObjectBoundary': sColor_Imagef->nSideOfObjectLocation = %d, sColor_Imagef->nIntensityOfBackground_Red = %d, ..._Green = %d, ....Blue = %d",
		sColor_Imagef->nSideOfObjectLocation, sColor_Imagef->nIntensityOfBackground_Red, sColor_Imagef->nIntensityOfBackground_Green, sColor_Imagef->nIntensityOfBackground_Blue);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
	{

		if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n /////////////////////////////////////////////////////////////////////////");
			fprintf(fout_lr, "\n Next iWidf = %d within the range: ", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			nNumOfNonZeroBeginningsInARowf = 0;
			nNumOfNonZeroEndsInARowf = 0;

			nRedPrevf = 0;
		}  //if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax)

		for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
		{
			if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax && iLenf >= nLenSubimageToPrintMin && iLenf <= nLenSubimageToPrintMax)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				if (nRedCurf != sColor_Imagef->nIntensityOfBackground_Red && nRedPrevf == sColor_Imagef->nIntensityOfBackground_Red)
				{
					nNumOfNonZeroBeginningsInARowf += 1;
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n A next nNumOfNonZeroBeginningsInARowf = %d, iWidf = %d ", nNumOfNonZeroBeginningsInARowf, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				} //if (nRedCurf != sColor_Imagef->nIntensityOfBackground_Red && nRedPrevf == sColor_Imagef->nIntensityOfBackground_Red)

				if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nRedPrevf != sColor_Imagef->nIntensityOfBackground_Red)
				{
					nNumOfNonZeroEndsInARowf += 1;
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n A next nNumOfNonZeroEndsInARowf = %d, iWidf = %d ", nNumOfNonZeroEndsInARowf, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				} //if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nRedPrevf != sColor_Imagef->nIntensityOfBackground_Red)

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "%d:%d,", iLenf, nRedCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				nRedPrevf = nRedCurf;

				if (iLenf == sColor_Imagef->nLength - 1)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n The final nNumOfNonZeroBeginningsInARowf = %d, nNumOfNonZeroEndsInARowf = %d for iWidf = %d ", nNumOfNonZeroBeginningsInARowf, nNumOfNonZeroEndsInARowf, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				}//if (iLenf == sColor_Imagef->nLength - 1)

			} //if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax && iLenf >= nLenSubimageToPrintMin && iLenf <= nLenSubimageToPrintMax)

		} //for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)

	} // for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)

#ifndef COMMENT_OUT_ALL_PRINTS
	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//object to the left  
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n The object is to the left");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		nWidForfNonZeroObjectBoundariesf = -1; // initially
		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] == -1)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error: the object boundary has not been found at iWidf - 1 = %d", iWidf - 1);
				printf("\n\n An error: the object boundary has not been found at iWidf - 1 = %d", iWidf - 1);

				//printf("\n\n Please adjust the settings and press any key to exit"); getchar();  exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			} //if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] == -1)

			nNumOfRightBoundariesf = 0;
			nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf = 0;

			nIs_ExitFromALinePossiblef = 0; //initially
			//nLenOf_1stObjectBoundaryCurf = -1;
			//nLenOf_2ndObjectBoundaryCurf = -1;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n The length boundaries for a left object, iWidf = %d\n", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];
				/*
				if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax && iLenf >= nLenSubimageToPrintMin && iLenf <= nLenSubimageToPrintMax)
				{
				fprintf(fout_lr, "\n\n iLenf = %d, iWidf = %d, nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d", iLenf, iWidf, nRedCurf, nGreenCurf, nBlueCurf);
				fprintf(fout_lr, "\n Prev: nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf = %d", nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf);
				}// if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax && iLenf >= nLenSubimageToPrintMin && iLenf <= nLenSubimageToPrintMax)
				*/

				//	if (nRedCurf != sColor_Imagef->nIntensityOfBackground_Red || nGreenCurf != sColor_Imagef->nIntensityOfBackground_Green ||
				//	nBlueCurf != sColor_Imagef->nIntensityOfBackground_Blue)
				if (nRedCurf >= nIntensityCloseToBlackMax || nGreenCurf >= nIntensityCloseToBlackMax ||
					nBlueCurf >= nIntensityCloseToBlackMax)
				{
					nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf = 0;

					continue;
				} // if (nRedCurf >= nIntensityCloseToBlackMax || nGreenCurf >= nIntensityCloseToBlackMax ||...

				nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf += 1;

				if (nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf == 1)
				{
					nIs_ExitFromALinePossiblef = 1; //possible
				} // if (nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf == 1)
				  //new
				if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] != -1)
				{
					nDiffOfLensf = iLenf - sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1];

					if (nDiffOfLensf - nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min > nDiffOfLens_Maxf && iWidf > nWidForfNonZeroObjectBoundariesf &&
						nWidForfNonZeroObjectBoundariesf >= 0 && nIs_ExitFromALinePossiblef == 1) // to the right
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n Exit for the right boundary of iWidf = %d, iLenf = %d, prev sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, nDiffOfLensf = %d, sColor_Imagef->nLength = %d",
							iWidf, iLenf, iWidf - 1, sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1], nDiffOfLensf, sColor_Imagef->nLength);

						fprintf(fout_lr, "\n Current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						break; // for (iLenf = 0; iLenf < sColor_Imagef->nLength;
					} // if (nDiffOfLensf > nDiffOfLens_Maxf && iWidf > nWidForfNonZeroObjectBoundariesf)

				}// if (iWidf > 0 && ...)

				 //////////////////////////////////////////////////////////////////////////////////////////////////////
				 //for 1st line with object boundary or for a line with no object boundary
				if (iLenf > 0 && (nWidForfNonZeroObjectBoundariesf == -1 || iWidf == nWidForfNonZeroObjectBoundariesf) &&
					nIs_ExitFromALinePossiblef == 1)
				{
					if (iLenf >= nDistOf1stObjectPixel_ToImageEdgeMaxf)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n The pixel can not be the 1st object pixel of an object to the left, iWidf = %d, iLenf = %d > nDistOf1stObjectPixel_ToImageEdgeMaxf = %d",
							iWidf, iLenf, nDistOf1stObjectPixel_ToImageEdgeMaxf);
						fprintf(fout_lr, "\n Current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);

						fprintf(fout_lr, "\n Exit from the line iWidf = %d", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						nIs_ExitFromALinePossiblef = 0;
						break; // for (iLenf = 0; iLenf < sColor_Imagef->nLength;
					} // if (iLenf >= nDistOf1stObjectPixel_ToImageEdgeMaxf)

					if (iLenf == 1)
						nForCharacteristicsOfNeighborsf = 1;
					else
						nForCharacteristicsOfNeighborsf = iLenf - 1;

					nResf = CharacteristicsOf_ObjectNeighborsInARow(

						sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

						iWidf, //const int nWidCurf,

							   //iLenf, //const int nLenCurf,
						nForCharacteristicsOfNeighborsf, //iLenf, //const int nLenCurf,

						nLenLongOfNeighboring_ObjectPixelsFrTheSameRow, //const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
																		/////////////////////////////////////////////////////////////
						nNumOfBlackNeighboingPixelsFrTheSameRowf, //int &nNumOfBlackNeighboingPixelsFrTheSameRowf,
						nAverIntensityOverNeighboingPixelsFrTheSameRowf, //int &nAverIntensityOverNeighboingPixelsFrTheSameRowf,
						nDiffBetweenMaxAndMinf); // int &nDiffBetweenMaxAndMinf);

					if (nResf == UNSUCCESSFUL_RETURN)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n UNSUCCESSFUL_RETURN in 'DeterminingLenOfObjectBoundary' 1");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						return UNSUCCESSFUL_RETURN;
					}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n Left object: iWidf = %d, iLenf = %d, nNumOfBlackNeighboingPixelsFrTheSameRowf = %d",
						iWidf, iLenf, nNumOfBlackNeighboingPixelsFrTheSameRowf);
					fprintf(fout_lr, "\n\n nAverIntensityOverNeighboingPixelsFrTheSameRowf = %d, nDiffBetweenMaxAndMinf = %d",
						nAverIntensityOverNeighboingPixelsFrTheSameRowf, nDiffBetweenMaxAndMinf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					if (nNumOfBlackNeighboingPixelsFrTheSameRowf >= nNumOfBlackNeighboing_ObjectPixels_LongMax)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 1: discarding a possible right boundary by nNumOfBlackNeighboingPixelsFrTheSameRowf = %d >= nNumOfBlackNeighboing_ObjectPixels_LongMax = %d",
							nNumOfBlackNeighboingPixelsFrTheSameRowf, nNumOfBlackNeighboing_ObjectPixels_LongMax);

						fprintf(fout_lr, "\n Current iLenf = %d, sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iLenf, iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						nIs_ExitFromALinePossiblef = 0; // the exit from a line is impossible until 'nIs_ExitFromALinePossiblef = 1'
						continue; // for the line
					}//if (nNumOfBlackNeighboingPixelsFrTheSameRowf >= nNumOfBlackNeighboing_ObjectPixels_LongMax)

					if (nAverIntensityOverNeighboingPixelsFrTheSameRowf <= nAverIntensOfNeighboing_ObjectPixels_LongMin)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 1: discarding a possible right boundary by nAverIntensityOverNeighboingPixelsFrTheSameRowf = %d <= nAverIntensOfNeighboing_ObjectPixels_LongMin = %d",
							nAverIntensityOverNeighboingPixelsFrTheSameRowf, nAverIntensOfNeighboing_ObjectPixels_LongMin);

						fprintf(fout_lr, "\n Current iLenf = %d, sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iLenf, iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						nIs_ExitFromALinePossiblef = 0; // the exit from a line is impossible until 'nIs_ExitFromALinePossiblef = 1'
						continue; // for the line
					}//if (nAverIntensityOverNeighboingPixelsFrTheSameRowf <= nAverIntensOfNeighboing_ObjectPixels_LongMin)

					 //nNumOfRightBoundariesf += 1;

					if (nWidForfNonZeroObjectBoundariesf == -1)
					{
						//verifying the 1st possible object boundary: pixels below and/or to the left should be object pixels
						//Below
						nWidBelowf = iWidf;

						nWidBelowf = nWidBelowf + nWidDiffFor1stObjectPixelVerification;

						if (nWidBelowf >= sColor_Imagef->nWidth)
						{
#ifndef COMMENT_OUT_ALL_PRINTS
							printf("\n\n An error in verification of the 1st object pixel, nWidBelowf = %d >= sColor_Imagef->nWidth = %d, iWidf = %d, iLenf = %d",
								nWidBelowf, sColor_Imagef->nWidth, iWidf, iLenf);
							fprintf(fout_lr, "\n\n An error in verification of the 1st object pixel, nWidBelowf = %d >= sColor_Imagef->nWidth = %d, iWidf = %d, iLenf = %d",
								nWidBelowf, sColor_Imagef->nWidth, iWidf, iLenf);

							//printf("\n\n Please adjust the settings and press any key to exit"); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS							

							return UNSUCCESSFUL_RETURN;
						} // if (nWidBelowf >= sColor_Imagef->nWidth)

						  //nIndexOfPixelBelowf = nLenOfObjectBoundaryCurf + (nWidBelowf*sColor_Imagef->nLength); // nLenMax);
						nIndexOfPixelBelowf = iLenf + (nWidBelowf*sColor_Imagef->nLength); // nLenMax);

						nRedBelowf = sColor_Imagef->nRed_Arr[nIndexOfPixelBelowf];
						nGreenBelowf = sColor_Imagef->nGreen_Arr[nIndexOfPixelBelowf];
						nBlueBelowf = sColor_Imagef->nBlue_Arr[nIndexOfPixelBelowf];
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n Below: iLenf = %d, nWidBelowf = %d, nRedBelowf = %d, nGreenBelowf = %d, nBlueBelowf = %d",
							iLenf, nWidBelowf, nRedBelowf, nGreenBelowf, nBlueBelowf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						if (nRedBelowf >= nIntensityCloseToBlackMax || nGreenBelowf >= nIntensityCloseToBlackMax ||
							nBlueBelowf >= nIntensityCloseToBlackMax)
						{
#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n The 1st right boundary is verified by an object pixel below");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							goto Mark1stObjectPixel_LeftObject;
						} // if (nRedBelowf != sColor_Imagef->nIntensityOfBackground_Red || ...
						else // if (nRedBelowf < nIntensityCloseToBlackMax && nGreenBelowf < nIntensityCloseToBlackMax...
						{
							//the pixel to the left for the left object
							nLenSidef = iLenf - nLenDiffFor1stObjectPixelVerification;
#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n Checking up if the side pixel to the right belongs to the object, iWidf = %d, nLenSidef = %d, iLenf = %d",
								iWidf, nLenSidef, iLenf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							if (nLenSidef < 0)
							{
								nLenSidef = 0; // sColor_Imagef->nLength - 1;

#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n The distance to the left image edge < nLenDiffFor1stObjectPixelVerification");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
							} //if (nLenSidef < 0)

							nIndexOfPixelSidef = nLenSidef + (iWidf*sColor_Imagef->nLength); // nLenMax);
							nRedSidef = sColor_Imagef->nRed_Arr[nIndexOfPixelSidef];
							nGreenSidef = sColor_Imagef->nGreen_Arr[nIndexOfPixelSidef];
							nBlueSidef = sColor_Imagef->nBlue_Arr[nIndexOfPixelSidef];
#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n Left side: iLenf = %d, iWidf = %d, nLenSidef = %d, nRedSidef = %d, nGreenSidef = %d, nBlueSidef = %d",
								iLenf, iWidf, nLenSidef, nRedSidef, nGreenSidef, nBlueSidef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							if (nRedSidef >= nIntensityCloseToBlackMax || nGreenSidef >= nIntensityCloseToBlackMax ||
								nBlueSidef >= nIntensityCloseToBlackMax)
							{
#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n The 1st right boundary is verified by an object pixel to the left side");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								goto Mark1stObjectPixel_LeftObject;
							} // if (nRedSidef != sColor_Imagef->nIntensityOfBackground_Red || ...
							else
							{
#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n The 1st right boundary is Not verified by object pixels both below and to the left side");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
								goto Mark_No_1stObjectPixel_LeftObject;
							} //else

						} // else //if (nRedBelowf < nIntensityCloseToBlackMax && nGreenBelowf < nIntensityCloseToBlackMax...

					Mark_No_1stObjectPixel_LeftObject: nNumOfNonZeroObjectBoundariesf = 0;

						//nIsObjectPixelBelowf = 0;
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n The pixel can not be the 1st object pixel by a background below and to the left, iWidf = %d, iLenf = %d < nDistOf1stObjectPixel_ToImageEdgeMaxf = %d",
							iWidf, iLenf, nDistOf1stObjectPixel_ToImageEdgeMaxf);

						fprintf(fout_lr, "\n Current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d (object to the left), iLenf = %d",
							iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], iLenf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
						continue;
						//break; // for (iLenf = 0; iLenf < sColor_Imagef->nLength;
						///////////////////////////////////////////////////

					Mark1stObjectPixel_LeftObject:	nWidForfNonZeroObjectBoundariesf = iWidf; // inside 'if (nNumOfNonZeroObjectBoundariesf == 1)'
						nNumOfRightBoundariesf += 1;
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n The first nonzero next possible right boundary of the object by 'CharacteristicsOf_ObjectNeighborsInARow': ");

						fprintf(fout_lr, "\n nWidForfNonZeroObjectBoundariesf = %d, the 1st nNumOfRightBoundariesf = %d, iLenf = %d",
							nWidForfNonZeroObjectBoundariesf, nNumOfRightBoundariesf, iLenf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					} //if (nWidForfNonZeroObjectBoundariesf == -1)

					sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = iLenf - 1;

					if (nNumOfNonZeroObjectBoundariesf == 0)
					{
						nNumOfNonZeroObjectBoundariesf = 1;
					}//if (nNumOfNonZeroObjectBoundariesf == 0)

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n The next possible right boundary of the object in the 1st object line by 'CharacteristicsOf_ObjectNeighborsInARow':");

					fprintf(fout_lr, "\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, a new nNumOfRightBoundariesf = %d, sColor_Imagef->nLength = %d",
						iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], nNumOfRightBoundariesf, sColor_Imagef->nLength);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					nIs_ExitFromALinePossiblef = 0;
					continue;
				} //if (iLenf < sColor_Imagef->nLength - 1 && ( nWidForfNonZeroObjectBoundariesf == -1 || ... 

				  ///////////////////////////
				if (iLenf > 0 && nIs_ExitFromALinePossiblef == 1 && nWidForfNonZeroObjectBoundariesf != -1 &&
					iWidf > nWidForfNonZeroObjectBoundariesf)
				{
					//nLenOfObjectBoundaryCurf = iLenf - nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min;
					nLenOfObjectBoundaryCurf = iLenf - 1;

					if (nLenOfObjectBoundaryCurf < 0)
						nLenOfObjectBoundaryCurf = 0;

					if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] != -1)
					{
						nDiffOfLensForBoundariesf = nLenOfObjectBoundaryCurf - sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1];

						if (nDiffOfLensForBoundariesf > nDistBetweenBoundariesOfNeighborWids_Max && iWidf > nWidForfNonZeroObjectBoundariesf) // to the right  
																																			  //&& nIs_ExitFromALinePossiblef == 1) // already included
						{
#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n Discarding a possible right boundary: iWidf = %d, nLenOfObjectBoundaryCurf = %d, prev sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, nDiffOfLensForBoundariesf = %d, sColor_Imagef->nLength = %d",
								iWidf, nLenOfObjectBoundaryCurf, iWidf - 1, sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1], nDiffOfLensForBoundariesf, sColor_Imagef->nLength);

							fprintf(fout_lr, "\n Current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							nIs_ExitFromALinePossiblef = 0;
							break; // for (iLenf = 0; iLenf < sColor_Imagef->nLength;
						} // if (nDiffOfLensForBoundariesf > nDistBetweenBoundariesOfNeighborWids_Max && iWidf > nWidForfNonZeroObjectBoundariesf) 

					}// if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] != -1)

					if (nLenOfObjectBoundaryCurf > 0) //nLenOfObjectBoundaryCurf = iLenf - nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min;
					{
						nResf = CharacteristicsOf_ObjectNeighborsInARow(

							sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

							iWidf, //const int nWidCurf,
							nLenOfObjectBoundaryCurf, //const int nLenCurf,

							nLenLongOfNeighboring_ObjectPixelsFrTheSameRow, //const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
																			/////////////////////////////////////////////////////////////
							nNumOfBlackNeighboingPixelsFrTheSameRowf, //int &nNumOfBlackNeighboingPixelsFrTheSameRowf,
							nAverIntensityOverNeighboingPixelsFrTheSameRowf, //int &nAverIntensityOverNeighboingPixelsFrTheSameRowf,
							nDiffBetweenMaxAndMinf); // int &nDiffBetweenMaxAndMinf);

						if (nResf == UNSUCCESSFUL_RETURN)
						{
#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n UNSUCCESSFUL_RETURN in 'DeterminingLenOfObjectBoundary' 2");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							return UNSUCCESSFUL_RETURN;
						}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n iWidf = %d, nLenOfObjectBoundaryCurf = %d, nNumOfBlackNeighboingPixelsFrTheSameRowf = %d",
							iWidf, nLenOfObjectBoundaryCurf, nNumOfBlackNeighboingPixelsFrTheSameRowf);
						fprintf(fout_lr, "\n nAverIntensityOverNeighboingPixelsFrTheSameRowf = %d, nDiffBetweenMaxAndMinf = %d",
							nAverIntensityOverNeighboingPixelsFrTheSameRowf, nDiffBetweenMaxAndMinf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		
						if (nNumOfBlackNeighboingPixelsFrTheSameRowf >= nNumOfBlackNeighboing_ObjectPixels_LongMax)
						{

#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n Going to 'Verifying_IfAPixelBelongsToANarrowBoundary' (left): nNumOfBlackNeighboingPixelsFrTheSameRowf = %d >= nNumOfBlackNeighboing_ObjectPixels_LongMax = %d",
								nNumOfBlackNeighboingPixelsFrTheSameRowf, nNumOfBlackNeighboing_ObjectPixels_LongMax);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//??
							nResf = Verifying_IfAPixelBelongsToANarrowBoundary(
								sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

								iWidf, //const int nWidCurf,
								nLenOfObjectBoundaryCurf, //const int nLenCurf,

								nLenShortOfNeighboring_ObjectPixelsFrTheSameRow, //const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
								nLenShortOfNeighboring_BackgroundPixelsFrTheSameRow); // const int nLenOf_BackgroundNeighboringPixelsFrTheSameRowf);

/*
							if (nResf == UNSUCCESSFUL_RETURN)
							{
#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n UNSUCCESSFUL_RETURN in 'DeterminingLenOfObjectBoundary' 3");

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
								return UNSUCCESSFUL_RETURN;
							}//if (nResf == UNSUCCESSFUL_RETURN)
*/
							if (nResf == SUCCESSFUL_RETURN)
							{
#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n A possible right boundary by 'Verifying_IfAPixelBelongsToANarrowBoundary' 1:");

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								goto MarkRightBoundary;
							} //if (nResf == SUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n 2: discarding a possible right boundary by nNumOfBlackNeighboingPixelsFrTheSameRowf = %d >= nNumOfBlackNeighboing_ObjectPixels_LongMax = %d",
								nNumOfBlackNeighboingPixelsFrTheSameRowf, nNumOfBlackNeighboing_ObjectPixels_LongMax);

							fprintf(fout_lr, "\n Current nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", nLenOfObjectBoundaryCurf, iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
							nIs_ExitFromALinePossiblef = 0;

							continue;
						}//if (nNumOfBlackNeighboingPixelsFrTheSameRowf >= nNumOfBlackNeighboing_ObjectPixels_LongMax)

						if (nAverIntensityOverNeighboingPixelsFrTheSameRowf <= nAverIntensOfNeighboing_ObjectPixels_LongMin)
						{

#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n Going to 'Verifying_IfAPixelBelongsToANarrowBoundary' (left): nAverIntensityOverNeighboingPixelsFrTheSameRowf = %d <= nAverIntensOfNeighboing_ObjectPixels_LongMin = %d",
								nAverIntensityOverNeighboingPixelsFrTheSameRowf, nAverIntensOfNeighboing_ObjectPixels_LongMin);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							nResf = Verifying_IfAPixelBelongsToANarrowBoundary(
								sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

								iWidf, //const int nWidCurf,
								nLenOfObjectBoundaryCurf, //const int nLenCurf,

								nLenShortOfNeighboring_ObjectPixelsFrTheSameRow, //const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
								nLenShortOfNeighboring_BackgroundPixelsFrTheSameRow); // const int nLenOf_BackgroundNeighboringPixelsFrTheSameRowf);

/*
							if (nResf == UNSUCCESSFUL_RETURN)
							{
#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n UNSUCCESSFUL_RETURN in 'DeterminingLenOfObjectBoundary' 4");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								return UNSUCCESSFUL_RETURN;
							}//if (nResf == UNSUCCESSFUL_RETURN)
*/
							if (nResf == SUCCESSFUL_RETURN)
							{
#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n A possible right boundary by 'Verifying_IfAPixelBelongsToANarrowBoundary' 2:");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								goto MarkRightBoundary;
							} //if (nResf == SUCCESSFUL_RETURN)
#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n 2: discarding a possible right boundary by nAverIntensityOverNeighboingPixelsFrTheSameRowf = %d <= nAverIntensOfNeighboing_ObjectPixels_LongMin = %d",
								nAverIntensityOverNeighboingPixelsFrTheSameRowf, nAverIntensOfNeighboing_ObjectPixels_LongMin);

							fprintf(fout_lr, "\n Current nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", nLenOfObjectBoundaryCurf, iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							nIs_ExitFromALinePossiblef = 0;
							continue;
							//break; //
						}//if (nAverIntensityOverNeighboingPixelsFrTheSameRowf <= nAverIntensOfNeighboing_ObjectPixels_LongMin)
					} //if (nLenOfObjectBoundaryCurf > 0)

				MarkRightBoundary:				nNumOfRightBoundariesf += 1;

					nNumOfNonZeroObjectBoundariesf += 1;
					sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = nLenOfObjectBoundaryCurf;
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n The next possible right boundary of the object: sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, a new nNumOfRightBoundariesf = %d, sColor_Imagef->nLength = %d",
						iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], nNumOfRightBoundariesf, sColor_Imagef->nLength);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

/*
					if (nNumOfRightBoundariesf == 1)
					{
						//nLenOf_1stObjectBoundaryCurf = nLenOfObjectBoundaryCurf;
					} //if (nNumOfRightBoundariesf == 1)
					else if (nNumOfRightBoundariesf == 2)
					{
					//	nLenOf_2ndObjectBoundaryCurf = nLenOfObjectBoundaryCurf;
					} //if (nNumOfRightBoundariesf == 2)
*/
				} // if (iLenf > 0 && nIs_ExitFromALinePossiblef == 1 && nWidForfNonZeroObjectBoundariesf != -1 && ...

				  //////////////////////////
				if (iLenf == sColor_Imagef->nLength - 1 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf] == -1)
				{
					sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = 0;

					nNumOfRightBoundariesf += 1;
					sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = 0;
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n The right edge of the image: sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, nNumOfRightBoundariesf = %d, sColor_Imagef->nLength = %d",
						iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], nNumOfRightBoundariesf, sColor_Imagef->nLength);

					fprintf(fout_lr, "\n iWidf = %d, iLenf = %d", iWidf, iLenf);

					fprintf(fout_lr, "\n\n nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf = %d, nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min = %d",
						nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf, nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				} // if (iLenf == sColor_Imagef->nLength - 1 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf] == -1)

			} //for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)

			if (nWidForfNonZeroObjectBoundariesf == -1)
			{
				sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = 0;
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n nWidForfNonZeroObjectBoundariesf == -1: sColor_Imagef->nLenObjectBoundary_Arr[%d] is adjusted to %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
				fprintf(fout_lr, "\n\n Final current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				goto MarkFinalBoundaryForLeftObject;
			} //if (nWidForfNonZeroObjectBoundariesf == -1)

			if (sColor_Imagef->nLenObjectBoundary_Arr[iWidf] == -1)
			{
				sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = 0; // sColor_Imagef->nLength - 1;
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n The right object boundary has not been found for iWidf = %d; sColor_Imagef->nLenObjectBoundary_Arr[%d] is adjusted to the left image edge: %d",
					iWidf, iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
				fprintf(fout_lr, "\n\n Final current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				goto MarkFinalBoundaryForLeftObject;
			} //if (sColor_Imagef->nLenObjectBoundary_Arr[iWidf] == -1)

#ifndef COMMENT_OUT_ALL_PRINTS			
			fprintf(fout_lr, "\n\n Final current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		MarkFinalBoundaryForLeftObject:	continue;
		} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)

	} // if (sColor_Imagef->nSideOfObjectLocation == -1)

	  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	  //object to the right
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n The object is to the right");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		nWidForfNonZeroObjectBoundariesf = -1; // initially
		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] == -1)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error: the object boundary has not been found at iWidf - 1 = %d", iWidf - 1);
				printf("\n\n An error: the object boundary has not been found at iWidf - 1 = %d", iWidf - 1);

				//printf("\n\n Please adjust the settings and press any key to exit"); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS				

				return UNSUCCESSFUL_RETURN;
			} //if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] == -1)

			nNumOfLeftBoundariesf = 0;
			nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf = 0;

			nIs_ExitFromALinePossiblef = 0; //initially
			//nLenOf_1stObjectBoundaryCurf = -1;
			//nLenOf_2ndObjectBoundaryCurf = -1;
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n The length boundaries for a right object, iWidf = %d\n", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax && iLenf >= nLenSubimageToPrintMin && iLenf <= nLenSubimageToPrintMax)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n iLenf = %d, iWidf = %d, nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d", iLenf, iWidf, nRedCurf, nGreenCurf, nBlueCurf);

					fprintf(fout_lr, "\n Prev: nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf = %d", nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				}// if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax && iLenf >= nLenSubimageToPrintMin && iLenf <= nLenSubimageToPrintMax)

				if (nRedCurf >= nIntensityCloseToBlackMax || nGreenCurf >= nIntensityCloseToBlackMax ||
					nBlueCurf >= nIntensityCloseToBlackMax)
				{
					nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf = 0;

					continue;
				} // if (nRedCurf >= nIntensityCloseToBlackMax || nGreenCurf >= nIntensityCloseToBlackMax ||...

				nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf += 1;

				if (nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf == 1)
				{
					nIs_ExitFromALinePossiblef = 1; //possible
				} // if (nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf == 1)

				if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] != -1)
				{
					nDiffOfLensf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] - iLenf;

					//if (nDiffOfLensf > nDiffOfLens_Maxf && iWidf > nWidForfNonZeroObjectBoundariesf)
					if (nDiffOfLensf - nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min > nDiffOfLens_Maxf && iWidf > nWidForfNonZeroObjectBoundariesf &&
						nWidForfNonZeroObjectBoundariesf >= 0 && nIs_ExitFromALinePossiblef == 1)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n Exit for the left boundary of iWidf = %d, iLenf = %d, prev sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, nDiffOfLensf = %d, sColor_Imagef->nLength = %d",
							iWidf, iLenf, iWidf - 1, sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1], nDiffOfLensf, sColor_Imagef->nLength);

						fprintf(fout_lr, "\n Current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						break; // for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)
					} // if (nDiffOfLensf - nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min > nDiffOfLens_Maxf && ...

				} //if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] != -1)
				  //////////////////////////////////////////////////////////////////////////////////////////////////////

				  //for 1st line with object boundary or for a line with no object boundary
				if (iLenf < sColor_Imagef->nLength - 1 && (nWidForfNonZeroObjectBoundariesf == -1 || iWidf == nWidForfNonZeroObjectBoundariesf) &&
					nIs_ExitFromALinePossiblef == 1)
				{
					if (iLenf <= sColor_Imagef->nLength - nDistOf1stObjectPixel_ToImageEdgeMaxf)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n The pixel can not be the 1st object pixel of an object to the right, iWidf = %d, iLenf = %d <= sColor_Imagef->nLength - nDistOf1stObjectPixel_ToImageEdgeMaxf = %d",
							iWidf, iLenf, sColor_Imagef->nLength - nDistOf1stObjectPixel_ToImageEdgeMaxf);

						fprintf(fout_lr, "\n Current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);

						fprintf(fout_lr, "\n Exit from the line iWidf = %d", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						nIs_ExitFromALinePossiblef = 0;
						break; // for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)
					} // if (iLenf <= sColor_Imagef->nLength - nDistOf1stObjectPixel_ToImageEdgeMaxf && ...)


					if (iLenf == sColor_Imagef->nLength - 2)
						nForCharacteristicsOfNeighborsf = sColor_Imagef->nLength - 2;
					else
						nForCharacteristicsOfNeighborsf = iLenf + 1;

					nResf = CharacteristicsOf_ObjectNeighborsInARow(

						sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

						iWidf, //const int nWidCurf,
							   //iLenf, //const int nLenCurf,
						nForCharacteristicsOfNeighborsf, //const int nLenCurf,//because not an object pixel would be included

						nLenLongOfNeighboring_ObjectPixelsFrTheSameRow, //const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
																		/////////////////////////////////////////////////////////////
						nNumOfBlackNeighboingPixelsFrTheSameRowf, //int &nNumOfBlackNeighboingPixelsFrTheSameRowf,
						nAverIntensityOverNeighboingPixelsFrTheSameRowf, //int &nAverIntensityOverNeighboingPixelsFrTheSameRowf,
						nDiffBetweenMaxAndMinf); // int &nDiffBetweenMaxAndMinf);

					if (nResf == UNSUCCESSFUL_RETURN)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n UNSUCCESSFUL_RETURN in 'DeterminingLenOfObjectBoundary' 6");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						return UNSUCCESSFUL_RETURN;
					}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n Right object: iWidf = %d, iLenf = %d, nNumOfBlackNeighboingPixelsFrTheSameRowf = %d",
						iWidf, iLenf, nNumOfBlackNeighboingPixelsFrTheSameRowf);
					fprintf(fout_lr, "\n nAverIntensityOverNeighboingPixelsFrTheSameRowf = %d, nDiffBetweenMaxAndMinf = %d",
						nAverIntensityOverNeighboingPixelsFrTheSameRowf, nDiffBetweenMaxAndMinf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					if (nNumOfBlackNeighboingPixelsFrTheSameRowf >= nNumOfBlackNeighboing_ObjectPixels_LongMax)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 1: discarding a possible left boundary by nNumOfBlackNeighboingPixelsFrTheSameRowf = %d >= nNumOfBlackNeighboing_ObjectPixels_LongMax = %d",
							nNumOfBlackNeighboingPixelsFrTheSameRowf, nNumOfBlackNeighboing_ObjectPixels_LongMax);

						fprintf(fout_lr, "\n Current iLenf = %d, sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iLenf, iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						nIs_ExitFromALinePossiblef = 0; // the exit from a line is impossible until 'nIs_ExitFromALinePossiblef = 1'
						continue; // for the line
					}//if (nNumOfBlackNeighboingPixelsFrTheSameRowf >= nNumOfBlackNeighboing_ObjectPixels_LongMax)

					if (nAverIntensityOverNeighboingPixelsFrTheSameRowf <= nAverIntensOfNeighboing_ObjectPixels_LongMin)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 1: discarding a possible left boundary by nAverIntensityOverNeighboingPixelsFrTheSameRowf = %d <= nAverIntensOfNeighboing_ObjectPixels_LongMin = %d",
							nAverIntensityOverNeighboingPixelsFrTheSameRowf, nAverIntensOfNeighboing_ObjectPixels_LongMin);

						fprintf(fout_lr, "\n Current iLenf = %d, sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iLenf, iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						nIs_ExitFromALinePossiblef = 0; // the exit from a line is impossible until 'nIs_ExitFromALinePossiblef = 1'
						continue; // for the line
					}//if (nAverIntensityOverNeighboingPixelsFrTheSameRowf <= nAverIntensOfNeighboing_ObjectPixels_LongMin)
					 //////////////////////////////////////////////////////////////////////

					if (nWidForfNonZeroObjectBoundariesf == -1)
					{
						//verifying the 1st possible object boundary: pixels below and/or to the rightt should be object pixels
						//Below
						nWidBelowf = iWidf;

						nWidBelowf = nWidBelowf + nWidDiffFor1stObjectPixelVerification;

						if (nWidBelowf >= sColor_Imagef->nWidth)
						{
#ifndef COMMENT_OUT_ALL_PRINTS
							printf("\n\n An error in verification of the 1st object pixel, nWidBelowf = %d >= sColor_Imagef->nWidth = %d, iWidf = %d, iLenf = %d",
								nWidBelowf, sColor_Imagef->nWidth, iWidf, iLenf);
							fprintf(fout_lr, "\n\n An error in verification of the 1st object pixel, nWidBelowf = %d >= sColor_Imagef->nWidth = %d, iWidf = %d, iLenf = %d",
								nWidBelowf, sColor_Imagef->nWidth, iWidf, iLenf);

							//printf("\n\n Please adjust the settings and press any key to exit");	getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS							

							return UNSUCCESSFUL_RETURN;
							//return UNSUCCESSFUL_RETURN;
						} // if (nWidBelowf >= sColor_Imagef->nWidth)

						nIndexOfPixelBelowf = iLenf + (nWidBelowf*sColor_Imagef->nLength); // nLenMax);

						nRedBelowf = sColor_Imagef->nRed_Arr[nIndexOfPixelBelowf];
						nGreenBelowf = sColor_Imagef->nGreen_Arr[nIndexOfPixelBelowf];
						nBlueBelowf = sColor_Imagef->nBlue_Arr[nIndexOfPixelBelowf];

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n Below: iLenf = %d, nWidBelowf = %d, nRedBelowf = %d, nGreenBelowf = %d, nBlueBelowf = %d",
							iLenf, nWidBelowf, nRedBelowf, nGreenBelowf, nBlueBelowf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
						if (nRedBelowf >= nIntensityCloseToBlackMax || nGreenBelowf >= nIntensityCloseToBlackMax ||
							nBlueBelowf >= nIntensityCloseToBlackMax)
						{
#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n The 1st left boundary is verified by an object pixel below");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							goto Mark1stObjectPixel_RightObject;
						} // if (nRedBelowf != sColor_Imagef->nIntensityOfBackground_Red || ...
						else // if (nRedBelowf < nIntensityCloseToBlackMax && nGreenBelowf < nIntensityCloseToBlackMax...
						{
							//the pixel to the right for the right object
							nLenSidef = iLenf + nLenDiffFor1stObjectPixelVerification;
#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n Checking up if the side pixel to the right belongs to the object, iWidf = %d, nLenSidef = %d, iLenf = %d",
								iWidf, nLenSidef, iLenf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							if (nLenSidef > sColor_Imagef->nLength - 1)
							{
								nLenSidef = sColor_Imagef->nLength - 1;
#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n The distance to the right image edge < nLenDiffFor1stObjectPixelVerification");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							} //if (nLenSidef > sColor_Imagef->nLength - 1)

							nIndexOfPixelSidef = nLenSidef + (iWidf*sColor_Imagef->nLength); // nLenMax);
							nRedSidef = sColor_Imagef->nRed_Arr[nIndexOfPixelSidef];
							nGreenSidef = sColor_Imagef->nGreen_Arr[nIndexOfPixelSidef];
							nBlueSidef = sColor_Imagef->nBlue_Arr[nIndexOfPixelSidef];

#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n Right side: iLenf = %d, iWidf = %d, nLenSidef = %d, nRedSidef = %d, nGreenSidef = %d, nBlueSidef = %d",
								iLenf, iWidf, nLenSidef, nRedSidef, nGreenSidef, nBlueSidef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							if (nRedSidef >= nIntensityCloseToBlackMax || nGreenSidef >= nIntensityCloseToBlackMax ||
								nBlueSidef >= nIntensityCloseToBlackMax)
							{
#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n The 1st left boundary is verified by an object pixel to the right side");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								goto Mark1stObjectPixel_RightObject;
							} // if (nRedSidef != sColor_Imagef->nIntensityOfBackground_Red || ...
							else
							{
#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n The 1st left boundary is Not verified by object pixels both below and to the right side");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								goto Mark_No_1stObjectPixel_RightObject;
							} //

						} // else //if (nRedBelowf < nIntensityCloseToBlackMax && nGreenBelowf < nIntensityCloseToBlackMax...

					Mark_No_1stObjectPixel_RightObject: nNumOfNonZeroObjectBoundariesf = 0;

						//nIsObjectPixelBelowf = 0;
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n The pixel can not be the 1st object pixel by a background below, iWidf = %d, iLenf = %d < nDistOf1stObjectPixel_ToImageEdgeMaxf = %d",
							iWidf, iLenf, nDistOf1stObjectPixel_ToImageEdgeMaxf);

						fprintf(fout_lr, "\n Current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d (object to the right), iLenf = %d",
							iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], iLenf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
						continue;
						//////////////////////////////////////////////////

					Mark1stObjectPixel_RightObject:	nWidForfNonZeroObjectBoundariesf = iWidf; // inside 'if (nNumOfNonZeroObjectBoundariesf == 1)'
						nNumOfLeftBoundariesf += 1;
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n The first nonzero next possible left boundary of the object by 'CharacteristicsOf_ObjectNeighborsInARow': ");

						fprintf(fout_lr, "\n nWidForfNonZeroObjectBoundariesf = %d, the 1st nNumOfLeftBoundariesf = %d, iLenf = %d",
							nWidForfNonZeroObjectBoundariesf, nNumOfLeftBoundariesf, iLenf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					} //if (nWidForfNonZeroObjectBoundariesf == -1)

					sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = iLenf + 1;

					if (nNumOfNonZeroObjectBoundariesf == 0)
					{
						nNumOfNonZeroObjectBoundariesf = 1;
					}//if (nNumOfNonZeroObjectBoundariesf == 0)

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n The next possible left boundary of the object in the 1st object line by 'CharacteristicsOf_ObjectNeighborsInARow':");

					fprintf(fout_lr, "\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, a new nNumOfLeftBoundariesf = %d, sColor_Imagef->nLength = %d",
						iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], nNumOfLeftBoundariesf, sColor_Imagef->nLength);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					nIs_ExitFromALinePossiblef = 0;
					continue;
				} //if (iLenf < sColor_Imagef->nLength - 1 && ( nWidForfNonZeroObjectBoundariesf == -1 || ... 

				  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
				if (iLenf < sColor_Imagef->nLength - 1 && nIs_ExitFromALinePossiblef == 1 && nWidForfNonZeroObjectBoundariesf != -1 &&
					iWidf > nWidForfNonZeroObjectBoundariesf)
				{
					nLenOfObjectBoundaryCurf = iLenf + 1;

					if (nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)
						nLenOfObjectBoundaryCurf = sColor_Imagef->nLength - 1;

					if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] != -1)
					{
						nDiffOfLensForBoundariesf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] - nLenOfObjectBoundaryCurf;

						if (nDiffOfLensForBoundariesf > nDistBetweenBoundariesOfNeighborWids_Max && iWidf > nWidForfNonZeroObjectBoundariesf)
						{
#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n Discarding a possible left boundary: iWidf = %d, nLenOfObjectBoundaryCurf = %d, prev sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, nDiffOfLensForBoundariesf = %d, sColor_Imagef->nLength = %d",
								iWidf, nLenOfObjectBoundaryCurf, iWidf - 1, sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1], nDiffOfLensForBoundariesf, sColor_Imagef->nLength);

							fprintf(fout_lr, "\n Current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							nIs_ExitFromALinePossiblef = 0;
							continue;
						} // if (nDiffOfLensForBoundariesf > nDistBetweenBoundariesOfNeighborWids_Max && iWidf > nWidForfNonZeroObjectBoundariesf && ...)

					}// if (iWidf > 0 && && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] != -1)

					if (nLenOfObjectBoundaryCurf < sColor_Imagef->nLength - 1) // nLenOfObjectBoundaryCurf == iLenf + nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min;
					{
						nResf = CharacteristicsOf_ObjectNeighborsInARow(
							sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

							iWidf, //const int nWidCurf,
							nLenOfObjectBoundaryCurf, //const int nLenCurf,

							nLenLongOfNeighboring_ObjectPixelsFrTheSameRow, //const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,

																			/////////////////////////////////////////////////////////////
							nNumOfBlackNeighboingPixelsFrTheSameRowf, //int &nNumOfBlackNeighboingPixelsFrTheSameRowf,
							nAverIntensityOverNeighboingPixelsFrTheSameRowf, //int &nAverIntensityOverNeighboingPixelsFrTheSameRowf,
							nDiffBetweenMaxAndMinf); // int &nDiffBetweenMaxAndMinf);

						if (nResf == UNSUCCESSFUL_RETURN)
						{
#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n UNSUCCESSFUL_RETURN in 'DeterminingLenOfObjectBoundary' 7");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							return UNSUCCESSFUL_RETURN;
						}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n After 'CharacteristicsOf_ObjectNeighborsInARow' 2: iWidf = %d, nLenOfObjectBoundaryCurf = %d, nNumOfBlackNeighboingPixelsFrTheSameRowf = %d",
							iWidf, nLenOfObjectBoundaryCurf, nNumOfBlackNeighboingPixelsFrTheSameRowf);
						fprintf(fout_lr, "\n nAverIntensityOverNeighboingPixelsFrTheSameRowf = %d, nDiffBetweenMaxAndMinf = %d",
							nAverIntensityOverNeighboingPixelsFrTheSameRowf, nDiffBetweenMaxAndMinf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						if (nNumOfBlackNeighboingPixelsFrTheSameRowf >= nNumOfBlackNeighboing_ObjectPixels_LongMax)
						{

#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n Going to 'Verifying_IfAPixelBelongsToANarrowBoundary' (right): nNumOfBlackNeighboingPixelsFrTheSameRowf = %d >= nNumOfBlackNeighboing_ObjectPixels_LongMax = %d",
								nNumOfBlackNeighboingPixelsFrTheSameRowf, nNumOfBlackNeighboing_ObjectPixels_LongMax);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							nResf = Verifying_IfAPixelBelongsToANarrowBoundary(
								sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

								iWidf, //const int nWidCurf,
								nLenOfObjectBoundaryCurf, //const int nLenCurf,

								nLenShortOfNeighboring_ObjectPixelsFrTheSameRow, //const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
								nLenShortOfNeighboring_BackgroundPixelsFrTheSameRow); // const int nLenOf_BackgroundNeighboringPixelsFrTheSameRowf);

/*
							if (nResf == UNSUCCESSFUL_RETURN)
							{
#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n UNSUCCESSFUL_RETURN in 'DeterminingLenOfObjectBoundary' 8");

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
								return UNSUCCESSFUL_RETURN;
							}//if (nResf == UNSUCCESSFUL_RETURN)
*/
							if (nResf == SUCCESSFUL_RETURN)
							{
#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n A possible left boundary by 'Verifying_IfAPixelBelongsToANarrowBoundary' 3:");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								goto MarkLeftBoundary;
							} //if (nResf == SUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n 2: discarding a possible left boundary by nNumOfBlackNeighboingPixelsFrTheSameRowf = %d >= nNumOfBlackNeighboing_ObjectPixels_LongMax = %d",
								nNumOfBlackNeighboingPixelsFrTheSameRowf, nNumOfBlackNeighboing_ObjectPixels_LongMax);

							fprintf(fout_lr, "\n Current nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", nLenOfObjectBoundaryCurf, iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							nIs_ExitFromALinePossiblef = 0; // the exit from a line is impossible until 'nIs_ExitFromALinePossiblef = 1'
							continue;
						}//if (nNumOfBlackNeighboingPixelsFrTheSameRowf >= nNumOfBlackNeighboing_ObjectPixels_LongMax)

						if (nAverIntensityOverNeighboingPixelsFrTheSameRowf <= nAverIntensOfNeighboing_ObjectPixels_LongMin)
						{
#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n Going to 'Verifying_IfAPixelBelongsToANarrowBoundary' (left): nAverIntensityOverNeighboingPixelsFrTheSameRowf = %d <= nAverIntensOfNeighboing_ObjectPixels_LongMin = %d",
								nAverIntensityOverNeighboingPixelsFrTheSameRowf, nAverIntensOfNeighboing_ObjectPixels_LongMin);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							nResf = Verifying_IfAPixelBelongsToANarrowBoundary(
								sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

								iWidf, //const int nWidCurf,
								nLenOfObjectBoundaryCurf, //const int nLenCurf,

								nLenShortOfNeighboring_ObjectPixelsFrTheSameRow, //const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
								nLenShortOfNeighboring_BackgroundPixelsFrTheSameRow); // const int nLenOf_BackgroundNeighboringPixelsFrTheSameRowf);

/*
							if (nResf == UNSUCCESSFUL_RETURN)
							{
#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n UNSUCCESSFUL_RETURN in 'DeterminingLenOfObjectBoundary' 10");

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
								return UNSUCCESSFUL_RETURN;
							}//if (nResf == UNSUCCESSFUL_RETURN)
*/
							if (nResf == SUCCESSFUL_RETURN)
							{
#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n A possible left boundary by 'Verifying_IfAPixelBelongsToANarrowBoundary' 4:");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								goto MarkLeftBoundary;
							} //if (nResf == SUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n 2: discarding a possible left boundary by nAverIntensityOverNeighboingPixelsFrTheSameRowf = %d <= nAverIntensOfNeighboing_ObjectPixels_LongMin = %d",
								nAverIntensityOverNeighboingPixelsFrTheSameRowf, nAverIntensOfNeighboing_ObjectPixels_LongMin);

							fprintf(fout_lr, "\n Current nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", nLenOfObjectBoundaryCurf, iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							nIs_ExitFromALinePossiblef = 0; // the exit from a line is impossible until 'nIs_ExitFromALinePossiblef = 1'
							continue;
						}//if (nAverIntensityOverNeighboingPixelsFrTheSameRowf <= nAverIntensOfNeighboing_ObjectPixels_LongMin)

					} //if (nLenOfObjectBoundaryCurf < sColor_Imagef->nLength - 1) 

				MarkLeftBoundary:				nNumOfLeftBoundariesf += 1;
					nNumOfNonZeroObjectBoundariesf += 1;
					sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = nLenOfObjectBoundaryCurf;

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n The next possible left boundary of the object: sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, a new nNumOfLeftBoundariesf = %d, sColor_Imagef->nLength = %d",
						iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], nNumOfLeftBoundariesf, sColor_Imagef->nLength);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					if (iWidf == -1) // no printing
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n nDiffOfLensf = %d, nDiffOfLensForBoundariesf = %d, nDistBetweenBoundariesOfNeighborWids_Max = %d, sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] = %d",
							nDiffOfLensf, nDiffOfLensForBoundariesf, nDistBetweenBoundariesOfNeighborWids_Max, sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					} // if (iWidf == -1)

					if (nNumOfLeftBoundariesf == 1)
					{
					//	nLenOf_1stObjectBoundaryCurf = nLenOfObjectBoundaryCurf;
					} //if (nNumOfLeftBoundariesf == 1)
					else if (nNumOfLeftBoundariesf == 2)
					{
						//nLenOf_2ndObjectBoundaryCurf = nLenOfObjectBoundaryCurf;
					} //if (nNumOfLeftBoundariesf == 2)

				} //if (iLenf < sColor_Imagef->nLength - 1 && nIs_ExitFromALinePossiblef == 1 && nWidForfNonZeroObjectBoundariesf != -1 && ...

				  //////////////
				if (iLenf == 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf] == -1)
				{
					sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = sColor_Imagef->nLength - 1;
					nNumOfLeftBoundariesf += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n The left edge of the image: sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, nNumOfLeftBoundariesf = %d, sColor_Imagef->nLength = %d",
						iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], nNumOfLeftBoundariesf, sColor_Imagef->nLength);

					fprintf(fout_lr, "\n iWidf = %d, iLenf = %d", iWidf, iLenf);

					fprintf(fout_lr, "\n\n nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf = %d, nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min = %d",
						nLengthOfTheSamePixelIntensities_ForObjectBoundary_Curf, nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				} // if (iLenf == 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf] == -1)

			} //for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)

			if (nWidForfNonZeroObjectBoundariesf == -1)
			{
				sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = sColor_Imagef->nLength - 1;

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n nWidForfNonZeroObjectBoundariesf == -1: sColor_Imagef->nLenObjectBoundary_Arr[%d] is adjusted to %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
				fprintf(fout_lr, "\n\n Final current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d for the object to the right", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				goto MarkFinalBoundaryForRightObject;
			} //if (nWidForfNonZeroObjectBoundariesf == -1)

			if (sColor_Imagef->nLenObjectBoundary_Arr[iWidf] == -1)
			{
				sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = sColor_Imagef->nLength - 1;

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n The left object boundary has not been found for iWidf = %d; sColor_Imagef->nLenObjectBoundary_Arr[%d] is adjusted to the right image edge: %d",
					iWidf, iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);

				fprintf(fout_lr, "\n\n Final current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d for the object to the right", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				goto MarkFinalBoundaryForRightObject;
			} //if (sColor_Imagef->nLenObjectBoundary_Arr[iWidf] == -1)

		MarkFinalBoundaryForRightObject: continue;
		} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)

	} // if (sColor_Imagef->nSideOfObjectLocation == 1)

	return SUCCESSFUL_RETURN;
} //int DeterminingLenOfObjectBoundary(...
  ////////////////////////////////////////////////////////////////////////////////////

int CharacteristicsOf_ObjectNeighborsInARow(

	const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

	const int nWidCurf,
	const int nLenCurf,

	const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
	/////////////////////////////////////////////////////////////
	int &nNumOfBlackNeighboingPixelsFrTheSameRowf,
	int &nAverIntensityOverNeighboingPixelsFrTheSameRowf,
	int &nDiffBetweenMaxAndMinf)
{
	int
		nIndexOfPixelCurf,

		//iWidf,
		iLenf,

		nLenMinf,
		nLenMaxf,

		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		nRedAverf = 0,
		nGreenAverf = 0,
		nBlueAverf = 0,

		nAverOfRedGreenBluef = 0,
		nAverOfRedGreenBlueMinf = nLarge,
		nAverOfRedGreenBlueMaxf = -nLarge,

		nLenOfPixelSegmentOfOneRowf;

	float
		fRatioOfLengthsf;

	if (nWidCurf < 0 || nWidCurf > sColor_Imagef->nWidth - 1 || nLenCurf <= 0 || nLenCurf >= sColor_Imagef->nLength - 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'CharacteristicsOf_ObjectNeighborsInARow' 1: ...->nWidth - 1 = %d, nWidCurf = %d, ...->nLength - 1 = %d, nLenCurf = %d",
			sColor_Imagef->nWidth - 1, nWidCurf, sColor_Imagef->nLength - 1, nLenCurf);

		fprintf(fout_lr, "\n\n An error in 'CharacteristicsOf_ObjectNeighborsInARow' 1: ...->nWidth - 1 = %d, nWidCurf = %d, ...->nLength - 1 = %d, nLenCurf = %d",
			sColor_Imagef->nWidth - 1, nWidCurf, sColor_Imagef->nLength - 1, nLenCurf);
		//printf("\n\n Please press any key to exit");	getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nWidCurf < 0 || nWidCurf > sColor_Imagef->nWidth - 1 || nLenCurf <= 0 || nLenCurf >= sColor_Imagef->nLength - 1)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n ////////////////////////////////////////////////////////////");
	fprintf(fout_lr, "\n 'CharacteristicsOf_ObjectNeighborsInARow' 1: nWidCurf = %d, nLenCurf = %d, nLenOf_ObjectNeighboringPixelsFrTheSameRowf = %d, sColor_Imagef->nSideOfObjectLocation = %d",
		nWidCurf, nLenCurf, nLenOf_ObjectNeighboringPixelsFrTheSameRowf, sColor_Imagef->nSideOfObjectLocation);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	nNumOfBlackNeighboingPixelsFrTheSameRowf = 0;
	nAverIntensityOverNeighboingPixelsFrTheSameRowf = 0;
	nDiffBetweenMaxAndMinf = -nLarge;

	///////////////////////////////////////////////////////////////
	//object to the left   
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{
		//start counting from the left
		nLenMinf = nLenCurf - nLenOf_ObjectNeighboringPixelsFrTheSameRowf;
		if (nLenMinf < 0)
			nLenMinf = 0;

		nLenOfPixelSegmentOfOneRowf = nLenCurf - nLenMinf + 1;


#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_lr, "\n 'CharacteristicsOf_ObjectNeighborsInARow' (left):  nLenMinf = %d, nLenCurf = %d, nLenOfPixelSegmentOfOneRowf = %d", nLenMinf, nLenCurf, nLenOfPixelSegmentOfOneRowf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		for (iLenf = nLenMinf; iLenf <= nLenCurf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (nWidCurf*sColor_Imagef->nLength); // nLenMax);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			nAverOfRedGreenBluef = (nRedCurf + nGreenCurf + nBlueCurf) / 3;

			if (nAverOfRedGreenBluef < nAverOfRedGreenBlueMinf)
			{
				nAverOfRedGreenBlueMinf = nAverOfRedGreenBluef;
			}// if (nAverOfRedGreenBluef < nAverOfRedGreenBlueMinf)

			if (nAverOfRedGreenBluef > nAverOfRedGreenBlueMaxf)
			{
				nAverOfRedGreenBlueMaxf = nAverOfRedGreenBluef;
			}// if (nAverOfRedGreenBluef > nAverOfRedGreenBlueMaxf)

			nRedAverf += nRedCurf;
			nGreenAverf += nGreenCurf;
			nBlueAverf += nBlueCurf;

			//if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && 
			//nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
			if (nRedCurf < nIntensityCloseToBlackMax && nGreenCurf < nIntensityCloseToBlackMax &&
				nBlueCurf < nIntensityCloseToBlackMax)
			{
				nNumOfBlackNeighboingPixelsFrTheSameRowf += 1;
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'CharacteristicsOf_ObjectNeighborsInARow' (left): the next  nNumOfBlackNeighboingPixelsFrTheSameRowf = %d, nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d",
					nNumOfBlackNeighboingPixelsFrTheSameRowf, nRedCurf, nGreenCurf, nBlueCurf);

				fprintf(fout_lr, "\n iLenf = %d, nLenCurf = %d, nWidCurf = %d, nLenOfPixelSegmentOfOneRowf = %d", iLenf, nLenCurf, nWidCurf, nLenOfPixelSegmentOfOneRowf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			} // if (nRedCurf < nIntensityCloseToBlackMax && nGreenCurf < nIntensityCloseToBlackMax && ...

			  /*
			  if (nWidCurf == 9 && nLenCurf == 1)
			  {
			  #ifndef COMMENT_OUT_ALL_PRINTS
			  fprintf(fout_lr, "\n\n iLenf = %d, nLenCurf = %d, nWidCurf = %d, nLenOfPixelSegmentOfOneRowf = %d", iLenf, nLenCurf, nWidCurf, nLenOfPixelSegmentOfOneRowf);
			  fprintf(fout_lr, "\n nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d, nNumOfBlackNeighboingPixelsFrTheSameRowf = %d",
			  nRedCurf, nGreenCurf, nBlueCurf, nNumOfBlackNeighboingPixelsFrTheSameRowf);

			  fprintf(fout_lr, "\n nRedAverf = %d, nGreenAverf = %d, nBlueAverf = %d", nRedAverf, nGreenAverf, nBlueAverf);
			  #endif //#ifndef COMMENT_OUT_ALL_PRINTS

			  }//if (nWidCurf == 9 && nLenCurf == 1)
			  */

		} //for (iLenf = nLenMinf; iLenf <= nLenCurf; iLenf++)

		nRedAverf = nRedAverf / nLenOfPixelSegmentOfOneRowf;
		nGreenAverf = nGreenAverf / nLenOfPixelSegmentOfOneRowf;
		nBlueAverf = nBlueAverf / nLenOfPixelSegmentOfOneRowf;

		nAverIntensityOverNeighboingPixelsFrTheSameRowf = (nRedAverf + nGreenAverf + nBlueAverf) / 3;

		nDiffBetweenMaxAndMinf = nAverOfRedGreenBlueMaxf - nAverOfRedGreenBlueMinf;

	} // if (sColor_Imagef->nSideOfObjectLocation == -1)

	  ////////////////////////////////////////////////////////////////////////////////////////////
	  //object to the right
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{
		//still, start counting from the left
		nLenMaxf = nLenCurf + nLenOf_ObjectNeighboringPixelsFrTheSameRowf;

		if (nLenMaxf > sColor_Imagef->nLength - 1)
			nLenMaxf = sColor_Imagef->nLength - 1;

		nLenOfPixelSegmentOfOneRowf = nLenMaxf - nLenCurf + 1;

#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_lr, "\n 'CharacteristicsOf_ObjectNeighborsInARow' (right):  nLenCurf = %d, nLenMaxf = %d, nLenOfPixelSegmentOfOneRowf = %d", nLenCurf, nLenMaxf, nLenOfPixelSegmentOfOneRowf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		for (iLenf = nLenCurf; iLenf <= nLenMaxf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (nWidCurf*sColor_Imagef->nLength); // nLenMax);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			nAverOfRedGreenBluef = (nRedCurf + nGreenCurf + nBlueCurf) / 3;

			if (nAverOfRedGreenBluef < nAverOfRedGreenBlueMinf)
			{
				nAverOfRedGreenBlueMinf = nAverOfRedGreenBluef;
			}// if (nAverOfRedGreenBluef < nAverOfRedGreenBlueMinf)

			if (nAverOfRedGreenBluef > nAverOfRedGreenBlueMaxf)
			{
				nAverOfRedGreenBlueMaxf = nAverOfRedGreenBluef;
			}// if (nAverOfRedGreenBluef > nAverOfRedGreenBlueMaxf)

			nRedAverf += nRedCurf;
			nGreenAverf += nGreenCurf;
			nBlueAverf += nBlueCurf;

			if (nRedCurf < nIntensityCloseToBlackMax && nGreenCurf < nIntensityCloseToBlackMax &&
				nBlueCurf < nIntensityCloseToBlackMax)
			{
				nNumOfBlackNeighboingPixelsFrTheSameRowf += 1;
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'CharacteristicsOf_ObjectNeighborsInARow' (right): the next  nNumOfBlackNeighboingPixelsFrTheSameRowf = %d, nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d",
					nNumOfBlackNeighboingPixelsFrTheSameRowf, nRedCurf, nGreenCurf, nBlueCurf);

				fprintf(fout_lr, "\n iLenf = %d, nLenCurf = %d, nWidCurf = %d, nLenOfPixelSegmentOfOneRowf = %d", iLenf, nLenCurf, nWidCurf, nLenOfPixelSegmentOfOneRowf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			} // if (nRedCurf < nIntensityCloseToBlackMax && nGreenCurf < nIntensityCloseToBlackMax && ...

		} //for (iLenf = nLenCurf; iLenf <= nLenMaxf; iLenf++)

		nRedAverf = nRedAverf / nLenOfPixelSegmentOfOneRowf;
		nGreenAverf = nGreenAverf / nLenOfPixelSegmentOfOneRowf;
		nBlueAverf = nBlueAverf / nLenOfPixelSegmentOfOneRowf;

		nAverIntensityOverNeighboingPixelsFrTheSameRowf = (nRedAverf + nGreenAverf + nBlueAverf) / 3;

		nDiffBetweenMaxAndMinf = nAverOfRedGreenBlueMaxf - nAverOfRedGreenBlueMinf;
	} // if (sColor_Imagef->nSideOfObjectLocation == 1)

	  //scaling the number of black pixels in case of closeness to the edge
	if (nLenOfPixelSegmentOfOneRowf < nLenOf_ObjectNeighboringPixelsFrTheSameRowf)
	{
		fRatioOfLengthsf = (float)(nLenOf_ObjectNeighboringPixelsFrTheSameRowf) / (float)(nLenOfPixelSegmentOfOneRowf);

		nNumOfBlackNeighboingPixelsFrTheSameRowf = (int)((float)(nNumOfBlackNeighboingPixelsFrTheSameRowf)*fRatioOfLengthsf);
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'CharacteristicsOf_ObjectNeighborsInARow': the final nNumOfBlackNeighboingPixelsFrTheSameRowf = %d,  nLenCurf = %d, nWidCurf = %d", nNumOfBlackNeighboingPixelsFrTheSameRowf, nLenCurf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	} //if (nLenOfPixelSegmentOfOneRowf < nLenOf_ObjectNeighboringPixelsFrTheSameRowf)

	return SUCCESSFUL_RETURN;
} //int CharacteristicsOf_ObjectNeighborsInARow(...
  ////////////////////////////////////////////////////////////////////////////////////

int CharacteristicsOf_BackgroundNeighborsInARow(

	const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

	const int nWidCurf,
	const int nLenCurf,

	const int nLenOf_BackgroundNeighboringPixelsFrTheSameRowf,
	/////////////////////////////////////////////////////////////
	int &nNumOf_NonBlackNeighboingPixelsFrTheSameRowf,

	int &nAverIntensityOverNeighboingPixelsFrTheSameRowf,
	int &nDiffBetweenMaxAndMinf)
{
	int
		nIndexOfPixelCurf,

		//iWidf,
		iLenf,

		nLenMinf,
		nLenMaxf,

		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		nRedAverf = 0,
		nGreenAverf = 0,
		nBlueAverf = 0,

		nAverOfRedGreenBluef = 0,
		nAverOfRedGreenBlueMinf = nLarge,
		nAverOfRedGreenBlueMaxf = -nLarge,

		nLenOfPixelSegmentOfOneRowf;

	float
		fRatioOfLengthsf;

	if (nWidCurf < 0 || nWidCurf > sColor_Imagef->nWidth - 1 || nLenCurf <= 0 || nLenCurf >= sColor_Imagef->nLength - 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'CharacteristicsOf_BackgroundNeighborsInARow' 1: ...->nWidth - 1 = %d, nWidCurf = %d, ...->nLength - 1 = %d, nLenCurf = %d",
			sColor_Imagef->nWidth - 1, nWidCurf, sColor_Imagef->nLength - 1, nLenCurf);

		fprintf(fout_lr, "\n\n An error in 'CharacteristicsOf_BackgroundNeighborsInARow' 1: ...->nWidth - 1 = %d, nWidCurf = %d, ...->nLength - 1 = %d, nLenCurf = %d",
			sColor_Imagef->nWidth - 1, nWidCurf, sColor_Imagef->nLength - 1, nLenCurf);
		//printf("\n\n Please press any key to exit");	getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nWidCurf < 0 || nWidCurf > sColor_Imagef->nWidth - 1 || nLenCurf <= 0 || nLenCurf >= sColor_Imagef->nLength - 1)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n ////////////////////////////////////////////////////////////");
	fprintf(fout_lr, "\n 'CharacteristicsOf_BackgroundNeighborsInARow' 1: nWidCurf = %d, nLenCurf = %d, nLenOf_BackgroundNeighboringPixelsFrTheSameRowf = %d, sColor_Imagef->nSideOfObjectLocation = %d",
		nWidCurf, nLenCurf, nLenOf_BackgroundNeighboringPixelsFrTheSameRowf, sColor_Imagef->nSideOfObjectLocation);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	nNumOf_NonBlackNeighboingPixelsFrTheSameRowf = 0;
	nAverIntensityOverNeighboingPixelsFrTheSameRowf = 0;
	nDiffBetweenMaxAndMinf = -nLarge;

	////////////////////////////////////////////////////////////////////////////////////////////
	//object to the left
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{

		//nLenMaxf = nLenCurf + nLenOf_BackgroundNeighboringPixelsFrTheSameRowf;
		nLenMaxf = nLenCurf + nLenOf_BackgroundNeighboringPixelsFrTheSameRowf + 1;

		if (nLenMaxf > sColor_Imagef->nLength - 1)
			nLenMaxf = sColor_Imagef->nLength - 1;

		nLenOfPixelSegmentOfOneRowf = nLenMaxf - nLenCurf + 1;

#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_lr, "\n 'CharacteristicsOf_BackgroundNeighborsInARow' (left):  nLenCurf = %d, nLenMaxf = %d, nLenOfPixelSegmentOfOneRowf = %d", nLenCurf, nLenMaxf, nLenOfPixelSegmentOfOneRowf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		//for (iLenf = nLenCurf; iLenf <= nLenMaxf; iLenf++)
		for (iLenf = nLenCurf + 1; iLenf <= nLenMaxf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (nWidCurf*sColor_Imagef->nLength); // nLenMax);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			nAverOfRedGreenBluef = (nRedCurf + nGreenCurf + nBlueCurf) / 3;

			if (nAverOfRedGreenBluef < nAverOfRedGreenBlueMinf)
			{
				nAverOfRedGreenBlueMinf = nAverOfRedGreenBluef;
			}// if (nAverOfRedGreenBluef < nAverOfRedGreenBlueMinf)

			if (nAverOfRedGreenBluef > nAverOfRedGreenBlueMaxf)
			{
				nAverOfRedGreenBlueMaxf = nAverOfRedGreenBluef;
			}// if (nAverOfRedGreenBluef > nAverOfRedGreenBlueMaxf)

			nRedAverf += nRedCurf;
			nGreenAverf += nGreenCurf;
			nBlueAverf += nBlueCurf;

			//if (nRedCurf < nIntensityCloseToBlackMax && nGreenCurf < nIntensityCloseToBlackMax &&
			//nBlueCurf < nIntensityCloseToBlackMax)
			if (nRedCurf > nIntensityCloseToBlackMax || nGreenCurf > nIntensityCloseToBlackMax ||
				nBlueCurf > nIntensityCloseToBlackMax)
			{
				nNumOf_NonBlackNeighboingPixelsFrTheSameRowf += 1;
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'CharacteristicsOf_BackgroundNeighborsInARow' (left): the next  nNumOf_NonBlackNeighboingPixelsFrTheSameRowf = %d, nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d",
					nNumOf_NonBlackNeighboingPixelsFrTheSameRowf, nRedCurf, nGreenCurf, nBlueCurf);

				fprintf(fout_lr, "\n iLenf = %d, nLenCurf = %d, nWidCurf = %d, nLenOfPixelSegmentOfOneRowf = %d", iLenf, nLenCurf, nWidCurf, nLenOfPixelSegmentOfOneRowf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			} // if (nRedCurf > nIntensityCloseToBlackMax || nGreenCurf > nIntensityCloseToBlackMax || ...

		} //for (iLenf = nLenCurf + 1; iLenf <= nLenMaxf; iLenf++)

		nRedAverf = nRedAverf / nLenOfPixelSegmentOfOneRowf;
		nGreenAverf = nGreenAverf / nLenOfPixelSegmentOfOneRowf;
		nBlueAverf = nBlueAverf / nLenOfPixelSegmentOfOneRowf;

		nAverIntensityOverNeighboingPixelsFrTheSameRowf = (nRedAverf + nGreenAverf + nBlueAverf) / 3;

		nDiffBetweenMaxAndMinf = nAverOfRedGreenBlueMaxf - nAverOfRedGreenBlueMinf;
	} // if (sColor_Imagef->nSideOfObjectLocation == -1)

	  ///////////////////////////////////////////////////////////////
	  //object to the right   
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{
		//start counting from the left
		//nLenMinf = nLenCurf - nLenOf_BackgroundNeighboringPixelsFrTheSameRowf;
		nLenMinf = nLenCurf - nLenOf_BackgroundNeighboringPixelsFrTheSameRowf - 1;
		if (nLenMinf < 0)
			nLenMinf = 0;

		nLenOfPixelSegmentOfOneRowf = nLenCurf - nLenMinf + 1;


#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_lr, "\n 'CharacteristicsOf_BackgroundNeighborsInARow' (right):  nLenMinf = %d, nLenCurf = %d, nLenOfPixelSegmentOfOneRowf = %d", nLenMinf, nLenCurf, nLenOfPixelSegmentOfOneRowf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		//for (iLenf = nLenMinf; iLenf <= nLenCurf; iLenf++)
		for (iLenf = nLenMinf; iLenf < nLenCurf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (nWidCurf*sColor_Imagef->nLength); // nLenMax);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			nAverOfRedGreenBluef = (nRedCurf + nGreenCurf + nBlueCurf) / 3;

			if (nAverOfRedGreenBluef < nAverOfRedGreenBlueMinf)
			{
				nAverOfRedGreenBlueMinf = nAverOfRedGreenBluef;
			}// if (nAverOfRedGreenBluef < nAverOfRedGreenBlueMinf)

			if (nAverOfRedGreenBluef > nAverOfRedGreenBlueMaxf)
			{
				nAverOfRedGreenBlueMaxf = nAverOfRedGreenBluef;
			}// if (nAverOfRedGreenBluef > nAverOfRedGreenBlueMaxf)

			nRedAverf += nRedCurf;
			nGreenAverf += nGreenCurf;
			nBlueAverf += nBlueCurf;


			//	if (nRedCurf < nIntensityCloseToBlackMax && nGreenCurf < nIntensityCloseToBlackMax &&
			//	nBlueCurf < nIntensityCloseToBlackMax)
			if (nRedCurf > nIntensityCloseToBlackMax || nGreenCurf > nIntensityCloseToBlackMax ||
				nBlueCurf > nIntensityCloseToBlackMax)
			{
				nNumOf_NonBlackNeighboingPixelsFrTheSameRowf += 1;
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'CharacteristicsOf_BackgroundNeighborsInARow' (right): the next  nNumOf_NonBlackNeighboingPixelsFrTheSameRowf = %d, nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d",
					nNumOf_NonBlackNeighboingPixelsFrTheSameRowf, nRedCurf, nGreenCurf, nBlueCurf);

				fprintf(fout_lr, "\n iLenf = %d, nLenCurf = %d, nWidCurf = %d, nLenOfPixelSegmentOfOneRowf = %d", iLenf, nLenCurf, nWidCurf, nLenOfPixelSegmentOfOneRowf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			} // if (nRedCurf < nIntensityCloseToBlackMax && nGreenCurf < nIntensityCloseToBlackMax && ...

		} //for (iLenf = nLenMinf; iLenf < nLenCurf; iLenf++)

		nRedAverf = nRedAverf / nLenOfPixelSegmentOfOneRowf;
		nGreenAverf = nGreenAverf / nLenOfPixelSegmentOfOneRowf;
		nBlueAverf = nBlueAverf / nLenOfPixelSegmentOfOneRowf;

		nAverIntensityOverNeighboingPixelsFrTheSameRowf = (nRedAverf + nGreenAverf + nBlueAverf) / 3;

		nDiffBetweenMaxAndMinf = nAverOfRedGreenBlueMaxf - nAverOfRedGreenBlueMinf;

	} // if (sColor_Imagef->nSideOfObjectLocation == 1)

	  ////////////////////////////////////////////////////////////////////
	  //scaling the number of black pixels in case of closeness to the edge
	if (nLenOfPixelSegmentOfOneRowf < nLenOf_BackgroundNeighboringPixelsFrTheSameRowf)
	{
		fRatioOfLengthsf = (float)(nLenOf_BackgroundNeighboringPixelsFrTheSameRowf) / (float)(nLenOfPixelSegmentOfOneRowf);

		nNumOf_NonBlackNeighboingPixelsFrTheSameRowf = (int)((float)(nNumOf_NonBlackNeighboingPixelsFrTheSameRowf)*fRatioOfLengthsf);
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'CharacteristicsOf_BackgroundNeighborsInARow': the final nNumOf_NonBlackNeighboingPixelsFrTheSameRowf = %d,  nLenCurf = %d, nWidCurf = %d", nNumOf_NonBlackNeighboingPixelsFrTheSameRowf, nLenCurf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	} //if (nLenOfPixelSegmentOfOneRowf < nLenOf_BackgroundNeighboringPixelsFrTheSameRowf)

	return SUCCESSFUL_RETURN;
} //int CharacteristicsOf_BackgroundNeighborsInARow(...
  /////////////////////////////////////////////////////////////////////////////////////////

int Verifying_IfAPixelBelongsToANarrowBoundary(
	const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

	const int nWidCurf,
	const int nLenCurf,

	const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
	const int nLenOf_BackgroundNeighboringPixelsFrTheSameRowf)
{
	int CharacteristicsOf_ObjectNeighborsInARow(

		const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

		const int nWidCurf,
		const int nLenCurf,

		const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
		/////////////////////////////////////////////////////////////
		int &nNumOfBlackNeighboingPixelsFrTheSameRowf,
		int &nAverIntensityOverNeighboingPixelsFrTheSameRowf,
		int &nDiffBetweenMaxAndMinf);

	int CharacteristicsOf_BackgroundNeighborsInARow(

		const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

		const int nWidCurf,
		const int nLenCurf,

		const int nLenOf_BackgroundNeighboringPixelsFrTheSameRowf,
		/////////////////////////////////////////////////////////////
		int &nNumOf_NonBlackNeighboingPixelsFrTheSameRowf,

		int &nAverIntensityOverNeighboingPixelsFrTheSameRowf,
		int &nDiffBetweenMaxAndMinf);

	int
		nResf, //
		nNumOfBlackNeighboing_ObjectPixelsFrTheSameRowf,
		nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf,

		nNumOf_NonBlackNeighboing_BackgroundPixelsFrTheSameRowf,
		nAverIntensityOverNeighboing_BackgroundPixelsFrTheSameRowf,
		nDiffBetweenMaxAndMinf;

	nResf = CharacteristicsOf_ObjectNeighborsInARow(

		sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

		nWidCurf, //const int nWidCurf,
		nLenCurf, //const int nLenCurf,

		nLenOf_ObjectNeighboringPixelsFrTheSameRowf, //const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
													 /////////////////////////////////////////////////////////////
		nNumOfBlackNeighboing_ObjectPixelsFrTheSameRowf, //int &nNumOfBlackNeighboingPixelsFrTheSameRowf,
		nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf, //int &nAverIntensityOverNeighboingPixelsFrTheSameRowf,
		nDiffBetweenMaxAndMinf); // int &nDiffBetweenMaxAndMinf);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n UNSUCCESSFUL_RETURN in 'Verifying_IfAPixelBelongsToANarrowBoundary' 1");

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'Verifying_IfAPixelBelongsToANarrowBoundary':  nNumOfBlackNeighboing_ObjectPixelsFrTheSameRowf = %d,  nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf = %d, nLenCurf = %d, nWidCurf = %d",
		nNumOfBlackNeighboing_ObjectPixelsFrTheSameRowf, nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf, nLenCurf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nNumOfBlackNeighboing_ObjectPixelsFrTheSameRowf > nNumOfBlackNeighboing_ObjectPixels_ShortMax)
	{

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n No boundary pixel in 'Verifying_IfAPixelBelongsToANarrowBoundary':  nNumOfBlackNeighboing_ObjectPixelsFrTheSameRowf = %d > nNumOfBlackNeighboing_ObjectPixels_ShortMax = %d",
			nNumOfBlackNeighboing_ObjectPixelsFrTheSameRowf, nNumOfBlackNeighboing_ObjectPixels_ShortMax);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nNumOfBlackNeighboing_ObjectPixelsFrTheSameRowf > nNumOfBlackNeighboing_ObjectPixels_ShortMax)

	if (nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf < nAverIntensOfNeighboing_ObjectPixels_ShortMin)
	{

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n No boundary pixel in 'Verifying_IfAPixelBelongsToANarrowBoundary':  nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf = %d < nAverIntensOfNeighboing_ObjectPixels_ShortMin = %d",
			nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf, nAverIntensOfNeighboing_ObjectPixels_ShortMin);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf < nAverIntensOfNeighboing_ObjectPixels_ShortMin)

	  ////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//
	nResf = CharacteristicsOf_BackgroundNeighborsInARow(

		sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

		nWidCurf, //const int nWidCurf,
		nLenCurf, //const int nLenCurf,

		nLenOf_BackgroundNeighboringPixelsFrTheSameRowf, //const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
														 /////////////////////////////////////////////////////////////
		nNumOf_NonBlackNeighboing_BackgroundPixelsFrTheSameRowf, //int &nNumOf_NonBlackNeighboingPixelsFrTheSameRowf,

		nAverIntensityOverNeighboing_BackgroundPixelsFrTheSameRowf, //int &nAverIntensityOverNeighboingPixelsFrTheSameRowf,
		nDiffBetweenMaxAndMinf); // int &nDiffBetweenMaxAndMinf);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n UNSUCCESSFUL_RETURN in 'Verifying_IfAPixelBelongsToANarrowBoundary' 2");

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'Verifying_IfAPixelBelongsToANarrowBoundary':  nNumOf_NonBlackNeighboing_BackgroundPixelsFrTheSameRowf = %d,  nAverIntensityOverNeighboing_BackgroundPixelsFrTheSameRowf = %d, nLenCurf = %d, nWidCurf = %d",
		nNumOf_NonBlackNeighboing_BackgroundPixelsFrTheSameRowf, nAverIntensityOverNeighboing_BackgroundPixelsFrTheSameRowf, nLenCurf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nNumOf_NonBlackNeighboing_BackgroundPixelsFrTheSameRowf > nNumOf_NonBlackNeighboing_BackgroundPixels_ShortMax)
	{

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n No boundary pixel in 'Verifying_IfAPixelBelongsToANarrowBoundary':  nNumOf_NonBlackNeighboing_BackgroundPixelsFrTheSameRowf = %d > nNumOf_NonBlackNeighboing_BackgroundPixels_ShortMax = %d",
			nNumOf_NonBlackNeighboing_BackgroundPixelsFrTheSameRowf, nNumOf_NonBlackNeighboing_BackgroundPixels_ShortMax);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nNumOfBlackNeighboing_BackgroundPixelsFrTheSameRowf > nNumOfBlackNeighboing_BackgroundPixels_ShortMax)

	if (nAverIntensityOverNeighboing_BackgroundPixelsFrTheSameRowf > nAverIntensOfNeighboing_BackgroundPixels_ShortMax)
	{

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n No boundary pixel in 'Verifying_IfAPixelBelongsToANarrowBoundary':  nAverIntensityOverNeighboing_BackgroundPixelsFrTheSameRowf = %d > nAverIntensOfNeighboing_BackgroundPixels_ShortMax = %d",
			nAverIntensityOverNeighboing_BackgroundPixelsFrTheSameRowf, nAverIntensOfNeighboing_BackgroundPixels_ShortMax);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nAverIntensityOverNeighboing_BackgroundPixelsFrTheSameRowf > nAverIntensOfNeighboing_BackgroundPixels_ShortMax)


	return SUCCESSFUL_RETURN;
}//int Verifying_IfAPixelBelongsToANarrowBoundary(...

 ///////////////////////////////////////////////////////////////////////////////////////////
int Erasing_Labels(

	COLOR_IMAGE *sColor_Imagef) //[nImageSizeMax]
{
	int
		nIndexOfPixelCurf,

		iWidf,
		iLenf,

		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		nLenOfObjectBoundaryCurf;

	//object to the left   
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{
		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			//start counting from the right

			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];
			if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)
			{
				if (nLenOfObjectBoundaryCurf == -1)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n No boundary/erasing in 'Erasing_Labels' 1: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
						nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					continue;
				} //if (nLenOfObjectBoundaryCurf == -1)

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'Erasing_Labels' 1: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
				printf("\n\n An error in 'Erasing_Labels' 1: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}// if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)

			for (iLenf = sColor_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryCurf; iLenf--)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				if (nRedCurf != sColor_Imagef->nIntensityOfBackground_Red)
				{
					sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = sColor_Imagef->nIntensityOfBackground_Red;
				} // if (nRedCurf != sColor_Imagef->nIntensityOfBackground_Red)

				if (nGreenCurf != sColor_Imagef->nIntensityOfBackground_Green)
				{
					sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = sColor_Imagef->nIntensityOfBackground_Green;
				} // if (nGreenCurf != sColor_Imagef->nIntensityOfBackground_Green)

				if (nBlueCurf != sColor_Imagef->nIntensityOfBackground_Blue)
				{
					sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = sColor_Imagef->nIntensityOfBackground_Blue;
				} // if (nBlueCurf != sColor_Imagef->nIntensityOfBackground_Blue)

			} //for (iLenf = sColor_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryCurf; iLenf--)

		} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
	} // if (sColor_Imagef->nSideOfObjectLocation == -1)

	  //object to the right
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{
		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			//start counting from the left

			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];
			if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)
			{
				if (nLenOfObjectBoundaryCurf == -1)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n No boundary/erasing in 'Erasing_Labels' 2: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
						nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					continue;
				} //if (nLenOfObjectBoundaryCurf == -1)

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'Erasing_Labels' 2: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
				printf("\n\n An error in 'Erasing_Labels' 2: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}// if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)

			for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				if (nRedCurf != sColor_Imagef->nIntensityOfBackground_Red)
				{
					sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = sColor_Imagef->nIntensityOfBackground_Red;
				} // if (nRedCurf != sColor_Imagef->nIntensityOfBackground_Red)

				if (nGreenCurf != sColor_Imagef->nIntensityOfBackground_Green)
				{
					sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = sColor_Imagef->nIntensityOfBackground_Green;
				} // if (nGreenCurf != sColor_Imagef->nIntensityOfBackground_Green)

				if (nBlueCurf != sColor_Imagef->nIntensityOfBackground_Blue)
				{
					sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = sColor_Imagef->nIntensityOfBackground_Blue;
				} // if (nBlueCurf != sColor_Imagef->nIntensityOfBackground_Blue)

			} //for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)

		} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
	} // if (sColor_Imagef->nSideOfObjectLocation == 1)

	return SUCCESSFUL_RETURN;
} //int Erasing_Labels(...

  //////////////////////////////////////////////////////////////////
int Detecting_BackgroundPixels(

	COLOR_IMAGE *sColor_Imagef) //[nImageSizeMax]
{
	int
		nIndexOfPixelCurf,

		nIndexOfPixelNeighborToRightf,

		nIndexOfPixelNeighborToRightBelowf,
		nIndexOfPixelNeighborToBelowf,
		nIndexOfPixelNeighborToLeftBelowf,

		nIndexOfPixelNeighborToRightAbovef,
		nIndexOfPixelNeighborToAbovef,
		nIndexOfPixelNeighborToLeftAbovef,

		iWidf,
		iLenf,

		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		nNumOfBlackPixelsInTheLastRowMinf = (int)(fLowestPercentageOfBlackPixelsInTheLastRow*sColor_Imagef->nLength),
		nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf = (int)(fLargestPercentageOfTheLastRowsWithInsufficientNumberOfBlackPixels*sColor_Imagef->nWidth),

		nIsAPixel_BackgroundOnef,

		//nBottomOfBlackPixelsf = -1,
		//nTopOfBlackPixelsf = -1,

		nNumOfBlackPixelsInALineTotf,

		nWidFor1stRowWithBlackPixelsf,

		nLenOfObjectBoundaryCurf;

	nWidFor1stRowWithBlackPixelsf = sColor_Imagef->nWidth - 1;

	//object to the left   
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//from Up to Down, the object is to the left 
		for (iWidf = sColor_Imagef->nWidth - 1; iWidf >= 0; iWidf--)
		{
			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];

			if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'Detecting_BackgroundPixels' Up to Down: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				fprintf(fout_lr, "\n\n An error in 'Detecting_BackgroundPixels' Up to Down: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				//printf("\n\n Please press any key to exit"); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}// if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)

			 //start counting from the right for the left object
			nNumOfBlackPixelsInALineTotf = 0;

			for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				if (iWidf == nWidFor1stRowWithBlackPixelsf) //sColor_Imagef->nWidth - 1)
				{
					if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
						nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
					{
						nNumOfBlackPixelsInALineTotf += 1;
						sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background
						continue;
					} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...

				}//if (iWidf == nWidFor1stRowWithBlackPixelsf)
				else // if (iWidf < sColor_Imagef->nWidth - 1)
				{
					if (iLenf == sColor_Imagef->nLength - 1)
					{
						// No neighbor to the left or left below

						/////////////////////////////////////////////////////
						nIndexOfPixelNeighborToBelowf = iLenf + ((iWidf + 1)*sColor_Imagef->nLength); // a neighbor to the exactly below
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToBelowf];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...

							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

						  /////////////////////////////////////////////////////

						nIndexOfPixelNeighborToLeftBelowf = iLenf - 1 + ((iWidf + 1)*sColor_Imagef->nLength); // a neighbor to the left below
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToLeftBelowf];

						if (nIsAPixel_BackgroundOnef == 1)
						{
							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

					} //if (iLenf == sColor_Imagef->nLength - 1)
					  //////////////////////////////////////////////////////////

					if (iLenf < sColor_Imagef->nLength - 1)
					{
						nIndexOfPixelNeighborToRightf = iLenf + 1 + (iWidf*sColor_Imagef->nLength); // a neighbor to the right

						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToRightf];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)
						  /////////////////////////////////////////////////////

						nIndexOfPixelNeighborToRightBelowf = iLenf + 1 + ((iWidf + 1)*sColor_Imagef->nLength); // a neighbor to the right below
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToRightBelowf];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

						  /////////////////////////////////////////////////////

						nIndexOfPixelNeighborToBelowf = iLenf + ((iWidf + 1)*sColor_Imagef->nLength); // a neighbor to the exactly below
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToBelowf];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

						  /////////////////////////////////////////////////////
						if (iLenf > 0)
						{
							nIndexOfPixelNeighborToLeftBelowf = iLenf - 1 + ((iWidf + 1)*sColor_Imagef->nLength); // a neighbor to the left below
							nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToLeftBelowf];

							if (nIsAPixel_BackgroundOnef == 1)
							{
								if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
									nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
								{
									nNumOfBlackPixelsInALineTotf += 1;
									sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

								} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
								continue;
							} //if (nIsAPixel_BackgroundOnef == 1)
						} // if (iLenf > 0)

					} //if (iLenf < sColor_Imagef->nLength - 1)

				} // else // if (iWidf < sColor_Imagef->nWidth - 1)

			} //for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)

			  //nNumOfBlackPixelsInTheLastRowMinf
			if (iWidf == nWidFor1stRowWithBlackPixelsf) //sColor_Imagef->nWidth - 1)
			{
				if (nNumOfBlackPixelsInALineTotf < nNumOfBlackPixelsInTheLastRowMinf)
				{
					nWidFor1stRowWithBlackPixelsf = nWidFor1stRowWithBlackPixelsf - 1;

					if (sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf > nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						printf("\n\n An error in 'Detecting_BackgroundPixels' from Up to Down: sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf = %d > nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf = %d",
							sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf, nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf);

						printf("\n nNumOfBlackPixelsInALineTotf = %d, nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
							nNumOfBlackPixelsInALineTotf, nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

						fprintf(fout_lr, "\n\n An error in 'Detecting_BackgroundPixels' from Up to Down: sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf = %d > nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf = %d",
							sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf, nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf);

						fprintf(fout_lr, "\n nNumOfBlackPixelsInALineTotf = %d, nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
							nNumOfBlackPixelsInALineTotf, nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

						//printf("\n\n Please press any key to exit");	getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						return UNSUCCESSFUL_RETURN;
					} //if (sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf > nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf)

					continue; // for (iWidf = sColor_Imagef->nWidth - 1; iWidf >= 0; iWidf--)
				} //if (nNumOfBlackPixelsInALineTotf < nNumOfBlackPixelsInTheLastRowMinf) 

			}//if (iWidf == nWidFor1stRowWithBlackPixelsf)

			if (nNumOfBlackPixelsInALineTotf == 0)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'Detecting_BackgroundPixels' from Up to Down: nNumOfBlackPixelsInALineTotf == 0, nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				fprintf(fout_lr, "\n\n An error in 'Detecting_BackgroundPixels' from Up to Down: nNumOfBlackPixelsInALineTotf == 0, nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				//printf("\n\n Please press any key to exit"); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS			

				return UNSUCCESSFUL_RETURN;
			} // if (nNumOfBlackPixelsInALineTotf == 0)

		} //for (iWidf = sColor_Imagef->nWidth - 1; iWidf >= 0; iWidf--)
		  //The end of Up to Down, the object is to the left

		  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  //from Down to Up, the object is to the left 
		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];

			//start counting from the right for the left object
			nNumOfBlackPixelsInALineTotf = 0;

			//for (iLenf = sColor_Imagef->nLength - 1; iLenf >= nLenOfObjectBoundaryCurf; iLenf--)
			for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] == 1)
				{
					nNumOfBlackPixelsInALineTotf += 1;
					continue;
				}// if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] == 1)

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				if (iWidf == 0)
				{
					if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
						nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
					{
						nNumOfBlackPixelsInALineTotf += 1;
						sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background
						continue;
					} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
				}//if (iWidf == 0)

				else // if (iWidf > 0)
				{

					if (iLenf == sColor_Imagef->nLength - 1)
					{
						// No neighbor to the right or right Above
						/////////////////////////////////////////////////////
						nIndexOfPixelNeighborToAbovef = iLenf + ((iWidf - 1)*sColor_Imagef->nLength); // a neighbor to the exactly Above
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToAbovef];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

						  /////////////////////////////////////////////////////

						nIndexOfPixelNeighborToLeftAbovef = iLenf - 1 + ((iWidf - 1)*sColor_Imagef->nLength); // a neighbor to the left Above
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToLeftAbovef];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

					} //if (iLenf == sColor_Imagef->nLength - 1)
					  //////////////////////////////////////////////////////////

					if (iLenf < sColor_Imagef->nLength - 1)
					{
						nIndexOfPixelNeighborToRightf = iLenf + 1 + (iWidf*sColor_Imagef->nLength); // a neighbor to the right

						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToRightf];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)
						  /////////////////////////////////////////////////////

						nIndexOfPixelNeighborToRightAbovef = iLenf + 1 + ((iWidf - 1)*sColor_Imagef->nLength); // a neighbor to the right Above
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToRightAbovef];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

						  /////////////////////////////////////////////////////

						nIndexOfPixelNeighborToAbovef = iLenf + ((iWidf - 1)*sColor_Imagef->nLength); // a neighbor to the exactly Above
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToAbovef];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

						  /////////////////////////////////////////////////////
						if (iLenf > 0)
						{
							nIndexOfPixelNeighborToLeftAbovef = iLenf - 1 + ((iWidf - 1)*sColor_Imagef->nLength); // a neighbor to the left Above
							nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToLeftAbovef];

							if (nIsAPixel_BackgroundOnef == 1)
							{
								if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
									nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
								{
									nNumOfBlackPixelsInALineTotf += 1;
									sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

								} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
								continue;
							} //if (nIsAPixel_BackgroundOnef == 1)
						} // if (iLenf > 0)

					} //if (iLenf < sColor_Imagef->nLength - 1)

				} // else // // if (iWidf > 0)

			} //for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)

		} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)

		  //The end of Down to Up, the object is to the left

	} // if (sColor_Imagef->nSideOfObjectLocation == -1)
	  //////////////////////////////////////////////////////////////////////////////////////////////////////////

	  //object to the right
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//from Up to Down, the object is to the right 
		for (iWidf = sColor_Imagef->nWidth - 1; iWidf >= 0; iWidf--)
		{
			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];

			if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'Detecting_BackgroundPixels' Up to Down: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				fprintf(fout_lr, "\n\n An error in 'Detecting_BackgroundPixels' Up to Down: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
				//printf("\n\n Please press any key to exit");	getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}// if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)

			 //start counting from the right for the left object
			nNumOfBlackPixelsInALineTotf = 0;

			for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				if (iWidf == nWidFor1stRowWithBlackPixelsf) //sColor_Imagef->nWidth - 1)
				{
					if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
						nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
					{
						nNumOfBlackPixelsInALineTotf += 1;
						sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background
						continue;
					} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...

				}//if (iWidf == nWidFor1stRowWithBlackPixelsf)
				else // if (iWidf < sColor_Imagef->nWidth - 1)
				{
					//if (iLenf == sColor_Imagef->nLength - 1)
					if (iLenf == 0)
					{
						// No neighbor to the left or left below
						/////////////////////////////////////////////////////
						nIndexOfPixelNeighborToBelowf = iLenf + ((iWidf + 1)*sColor_Imagef->nLength); // a neighbor to the exactly below
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToBelowf];

						if (nIsAPixel_BackgroundOnef == 1)
						{
							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

						  /////////////////////////////////////////////////////

						nIndexOfPixelNeighborToLeftBelowf = iLenf + 1 + ((iWidf + 1)*sColor_Imagef->nLength); // a neighbor to the right below
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToLeftBelowf];

						if (nIsAPixel_BackgroundOnef == 1)
						{
							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

					} //if (iLenf == 0)

					  //////////////////////////////////////////////////////////
					  //if (iLenf < sColor_Imagef->nLength - 1)
					if (iLenf > 0)
					{
						nIndexOfPixelNeighborToRightf = iLenf - 1 + (iWidf*sColor_Imagef->nLength); // a neighbor to the left (not to the right)

						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToRightf];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)
						  /////////////////////////////////////////////////////

						if (iLenf < sColor_Imagef->nLength - 1)
						{
							nIndexOfPixelNeighborToRightBelowf = iLenf + 1 + ((iWidf + 1)*sColor_Imagef->nLength); // a neighbor to the right below
							nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToRightBelowf];

							if (nIsAPixel_BackgroundOnef == 1)
							{

								if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
									nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
								{
									nNumOfBlackPixelsInALineTotf += 1;
									sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

								} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
								continue;
							} //if (nIsAPixel_BackgroundOnef == 1)

						} // iLenf < sColor_Imagef->nLength - 1
						  /////////////////////////////////////////////////////

						nIndexOfPixelNeighborToBelowf = iLenf + ((iWidf + 1)*sColor_Imagef->nLength); // a neighbor to the exactly below
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToBelowf];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

						  /////////////////////////////////////////////////////
						nIndexOfPixelNeighborToLeftBelowf = iLenf - 1 + ((iWidf + 1)*sColor_Imagef->nLength); // a neighbor to the left below
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToLeftBelowf];

						if (nIsAPixel_BackgroundOnef == 1)
						{
							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

					} //if (iLenf > 0)

				} // else // if (iWidf < sColor_Imagef->nWidth - 1)

			} //for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)

			  //nNumOfBlackPixelsInTheLastRowMinf
			if (iWidf == nWidFor1stRowWithBlackPixelsf) //sColor_Imagef->nWidth - 1)
			{
				if (nNumOfBlackPixelsInALineTotf < nNumOfBlackPixelsInTheLastRowMinf)
				{
					nWidFor1stRowWithBlackPixelsf = nWidFor1stRowWithBlackPixelsf - 1;

					if (sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf > nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						printf("\n\n An error in 'Detecting_BackgroundPixels' from Up to Down 2: sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf = %d > nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf = %d",
							sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf, nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf);

						printf("\n nNumOfBlackPixelsInALineTotf = %d, nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
							nNumOfBlackPixelsInALineTotf, nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

						fprintf(fout_lr, "\n\n An error in 'Detecting_BackgroundPixels' from Up to Down 2: sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf = %d > nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf = %d",
							sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf, nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf);

						fprintf(fout_lr, "\n nNumOfBlackPixelsInALineTotf = %d, nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
							nNumOfBlackPixelsInALineTotf, nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

						//printf("\n\n Please press any key to exit");	getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						return UNSUCCESSFUL_RETURN;
					} //if (sColor_Imagef->nWidth - nWidFor1stRowWithBlackPixelsf > nNumOfTheLastRowsWithInsufficientNumberOfBlackPixelsMaxf)

					continue; // for (iWidf = sColor_Imagef->nWidth - 1; iWidf >= 0; iWidf--)
				} //if (nNumOfBlackPixelsInALineTotf < nNumOfBlackPixelsInTheLastRowMinf) 

			}//if (iWidf == nWidFor1stRowWithBlackPixelsf)

			if (nNumOfBlackPixelsInALineTotf == 0)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'Detecting_BackgroundPixels' from Up to Down 2: nNumOfBlackPixelsInALineTotf == 0, nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				fprintf(fout_lr, "\n\n An error in 'Detecting_BackgroundPixels' from Up to Down 2: nNumOfBlackPixelsInALineTotf == 0, nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				//printf("\n\n Please press any key to exit"); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			} // if (nNumOfBlackPixelsInALineTotf == 0)

		} //for (iWidf = sColor_Imagef->nWidth - 1; iWidf >= 0; iWidf--)
		  //The end of Up to Down, the object is to the right

		  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  //from Down to Up, the object is to the right 
		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];

			//start counting from the right for the left object
			nNumOfBlackPixelsInALineTotf = 0;

			for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] == 1)
				{
					nNumOfBlackPixelsInALineTotf += 1;
					continue;
				}// if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] == 1)

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				if (iWidf == 0)
				{
					if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
						nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
					{
						nNumOfBlackPixelsInALineTotf += 1;
						sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background
						continue;
					} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
				}//if (iWidf == 0)

				else // if (iWidf > 0)
				{

					if (iLenf == 0)
					{
						// No neighbor to the left or left Above
						/////////////////////////////////////////////////////
						nIndexOfPixelNeighborToAbovef = iLenf + ((iWidf - 1)*sColor_Imagef->nLength); // a neighbor to the exactly Above
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToAbovef];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

						  /////////////////////////////////////////////////////

						nIndexOfPixelNeighborToLeftAbovef = iLenf + 1 + ((iWidf - 1)*sColor_Imagef->nLength); // a neighbor to the right Above
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToLeftAbovef];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

					} //if (iLenf == 0)

					  //////////////////////////////////////////////////////////
					if (iLenf > 0)
					{
						nIndexOfPixelNeighborToRightf = iLenf - 1 + (iWidf*sColor_Imagef->nLength); // a neighbor to the left (not to the right)

						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToRightf];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)
						  /////////////////////////////////////////////////////

						if (iLenf < sColor_Imagef->nLength - 1)
						{
							nIndexOfPixelNeighborToRightAbovef = iLenf + 1 + ((iWidf - 1)*sColor_Imagef->nLength); // a neighbor to the right Above
							nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToRightAbovef];

							if (nIsAPixel_BackgroundOnef == 1)
							{

								if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
									nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
								{
									nNumOfBlackPixelsInALineTotf += 1;
									sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

								} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
								continue;
							} //if (nIsAPixel_BackgroundOnef == 1)
						} // if (iLenf < sColor_Imagef->nLength - 1)

						  /////////////////////////////////////////////////////

						nIndexOfPixelNeighborToAbovef = iLenf + ((iWidf - 1)*sColor_Imagef->nLength); // a neighbor to the exactly Above
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToAbovef];

						if (nIsAPixel_BackgroundOnef == 1)
						{

							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

						  /////////////////////////////////////////////////////
						nIndexOfPixelNeighborToLeftAbovef = iLenf - 1 + ((iWidf - 1)*sColor_Imagef->nLength); // a neighbor to the left Above
						nIsAPixel_BackgroundOnef = sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelNeighborToLeftAbovef];

						if (nIsAPixel_BackgroundOnef == 1)
						{
							if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
								nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
							{
								nNumOfBlackPixelsInALineTotf += 1;
								sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 1; // the pixel belongs to the background

							} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...
							continue;
						} //if (nIsAPixel_BackgroundOnef == 1)

					} //if (iLenf > 0)

				} // else // if (iWidf < sColor_Imagef->nWidth - 1)

			} //for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)

		} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)

		  //The end of Down to Up, the object is to the right

	} // if (sColor_Imagef->nSideOfObjectLocation == 1)

	return SUCCESSFUL_RETURN;
} //int Detecting_BackgroundPixels(...

  ///////////////////////////////////////////////////////////////////////////////////
int Smoothing_ObjectBoundaries(

	MASK_IMAGE *sMask_Imagef)
{
	//	void Initializing_nLenOfDentPixelsArrf(
	//	int nLenOfDentPixelsArrf[]);

	int Finding_BottomOfDentBoundary(
		const int nLenOfObjectBoundaryOfDentInitf,
		const int nWidOfObjectBoundaryOfDentInitf,

		MASK_IMAGE *sMask_Imagef,

		int &nWidOfBottomOfDentBoundaryf,
		int &nLenOfBottomOfDentBoundaryf); //

	int AdjustingBoundariesAndFillingOutPixelsOfADent(
		const int nLenOfObjectBoundaryOfDentPrevf,
		const int nWidOfObjectBoundaryOfDentPrevf,

		const int nWidOfBottomOfDentBoundaryf,
		const int nLenOfBottomOfDentBoundaryf,
		MASK_IMAGE *sMask_Imagef);

	int
		nResf,
		nIndexOfPixelCurf,

		iWidf,

		iLenf,

		nIntensityOfMaskImageCurf,

		nNumOfDentsTotf = 0,
		nNumOfDentsUnfilledTotf = 0,

		nNumOfDentsWithNoBottomFoundf = 0,

		nLenOfObjectBoundaryOfDentInitf,
		nWidOfObjectBoundaryOfDentInitf,

		nWidOfBottomOfDentBoundaryf,
		nLenOfBottomOfDentBoundaryf,

		nWidOfObjectBoundaryOfDentPrevf,
		nLenOfObjectBoundaryOfDentPrevf,

		nLenOfObjectBoundaryPrevf,

		nLenOfObjectBoundaryCurf,
		nDiffOfLensPrevAndCurf;
	////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'Smoothing_ObjectBoundaries':");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//finding the left and right boundaries of black object pixels 
	for (iWidf = nWidSubimageToPrintMin; iWidf < nWidSubimageToPrintMax; iWidf++)
	{
		nLenOfObjectBoundaryCurf = sMask_Imagef->nLenObjectBoundary_Arr[iWidf];
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n iWidf = %d, init nLenOfObjectBoundaryCurf = %d\n", iWidf, nLenOfObjectBoundaryCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		for (iLenf = 0; iLenf < sMask_Imagef->nLength; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*sMask_Imagef->nLength); // nLenMax);

			nIntensityOfMaskImageCurf = sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCurf];

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "%d:%d,", iLenf, nIntensityOfMaskImageCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		} //for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)

	}//for (iWidf = nWidSubimageToPrintMin; iWidf < nWidSubimageToPrintMax; iWidf++)

	 //object to the left   
	if (sMask_Imagef->nSideOfObjectLocation == -1)
	{
		nLenOfObjectBoundaryPrevf = -1;

		nLenOfObjectBoundaryOfDentInitf = -1; // no dent yet
		nWidOfObjectBoundaryOfDentInitf = -1;

		nWidOfBottomOfDentBoundaryf = -1;

		nWidOfObjectBoundaryOfDentPrevf = 0;

		//finding the left and right boundaries of black object pixels 
		for (iWidf = 0; iWidf < sMask_Imagef->nWidth; iWidf++)
		{

			if (iWidf < nWidOfBottomOfDentBoundaryf)
			{
				nWidOfObjectBoundaryOfDentPrevf = iWidf;
				continue;
			}//if (iWidf < nWidOfBottomOfDentBoundaryf)

			nLenOfObjectBoundaryCurf = sMask_Imagef->nLenObjectBoundary_Arr[iWidf];
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n 'Smoothing_ObjectBoundaries' (the left location): the next iWidf = %d, init nLenOfObjectBoundaryCurf = %d, nLenOfObjectBoundaryPrevf = %d",
				iWidf, nLenOfObjectBoundaryCurf, nLenOfObjectBoundaryPrevf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sMask_Imagef->nLength - 1)
			{
				if (nLenOfObjectBoundaryCurf == -1)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n No boundary in 'Smoothing_ObjectBoundaries' 1: nLenOfObjectBoundaryCurf = %d, sMask_Imagef->nLength - 1 = %d, iWidf = %d",
						nLenOfObjectBoundaryCurf, sMask_Imagef->nLength - 1, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					continue;
				} //if (nLenOfObjectBoundaryCurf == -1)

#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'Smoothing_ObjectBoundaries' 1: nLenOfObjectBoundaryCurf = %d, sMask_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sMask_Imagef->nLength - 1, iWidf);

				fprintf(fout_lr, "\n\n An error in 'Smoothing_ObjectBoundaries' 1: nLenOfObjectBoundaryCurf = %d, sMask_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sMask_Imagef->nLength - 1, iWidf);

				//printf("\n\n Please press any key to exit");	getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			}// if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sMask_Imagef->nLength - 1)

			if (nLenOfObjectBoundaryPrevf == -1)
			{
				nLenOfObjectBoundaryPrevf = nLenOfObjectBoundaryCurf;

				continue; //for (iWidf = 0; iWidf < sMask_Imagef->nWidth; iWidf++)
			} //if (nLenOfObjectBoundaryPrevf == -1)

			nDiffOfLensPrevAndCurf = nLenOfObjectBoundaryPrevf - nLenOfObjectBoundaryCurf;

			if (nDiffOfLensPrevAndCurf > nDiffOfLensCurAndPrevForADentMin) // a dent has been found
			{
				nNumOfDentsTotf += 1;
				nLenOfObjectBoundaryOfDentInitf = nLenOfObjectBoundaryCurf;
				nWidOfObjectBoundaryOfDentInitf = iWidf;

				nWidOfObjectBoundaryOfDentPrevf = iWidf - 1;
				nLenOfObjectBoundaryOfDentPrevf = nLenOfObjectBoundaryPrevf;

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n A next dent 1: nNumOfDentsTotf = %d, iWidf = %d, nDiffOfLensPrevAndCurf = %d", nNumOfDentsTotf, iWidf, nDiffOfLensPrevAndCurf);

				fprintf(fout_lr, "\n nWidOfObjectBoundaryOfDentInitf = %d, nLenOfObjectBoundaryOfDentInitf = %d, nLenOfObjectBoundaryOfDentPrevf = %d",
					nWidOfObjectBoundaryOfDentInitf, nLenOfObjectBoundaryOfDentInitf, nLenOfObjectBoundaryOfDentPrevf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				/////////////////////////////////////////////////////////////////////////////////
				//finding the bottom edge of the dent

				nResf = Finding_BottomOfDentBoundary(
					nLenOfObjectBoundaryOfDentInitf, //const int nLenOfObjectBoundaryOfDentInitf,
					nWidOfObjectBoundaryOfDentInitf, //const int nWidOfObjectBoundaryOfDentInitf,

					sMask_Imagef, //COLOR_IMAGE *sMask_Imagef,

					nWidOfBottomOfDentBoundaryf, //int &nWidOfBottomOfDentBoundaryf,
					nLenOfBottomOfDentBoundaryf); // int &nLenOfBottomOfDentBoundaryf); //
												  
//////////////////////////////////////////////
				if (nResf == SUCCESSFUL_RETURN)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n The bottom of the dent 1: nWidOfBottomOfDentBoundaryf = %d, nLenOfBottomOfDentBoundaryf = %d",
						nWidOfBottomOfDentBoundaryf, nLenOfBottomOfDentBoundaryf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				}// if (nResf == SUCCESSFUL_RETURN)
				else if (nResf == -2)
				{
					nNumOfDentsWithNoBottomFoundf += 1;
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n The bottom of the dent has not been found 1: nWidOfBottomOfDentBoundaryf = %d, nLenOfBottomOfDentBoundaryf = %d, nNumOfDentsWithNoBottomFoundf = %d",
						nWidOfBottomOfDentBoundaryf, nLenOfBottomOfDentBoundaryf, nNumOfDentsWithNoBottomFoundf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					nLenOfObjectBoundaryPrevf = nLenOfObjectBoundaryCurf;
					continue;
				}// else if (nResf == -2)
				else if (nResf == UNSUCCESSFUL_RETURN)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An UNSUCCESSFUL_RETURN from 'Finding_BottomOfDentBoundary' 1: nWidOfBottomOfDentBoundaryf = %d, nLenOfBottomOfDentBoundaryf = %d, nNumOfDentsWithNoBottomFoundf = %d",
						nWidOfBottomOfDentBoundaryf, nLenOfBottomOfDentBoundaryf, nNumOfDentsWithNoBottomFoundf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN;
				}// if (nResf == UNSUCCESSFUL_RETURN)

				nResf = AdjustingBoundariesAndFillingOutPixelsOfADent(

					nLenOfObjectBoundaryOfDentPrevf, //const int nLenOfObjectBoundaryOfDentPrevf,
					nWidOfObjectBoundaryOfDentPrevf, //const int nWidOfObjectBoundaryOfDentPrevf,	,

					nWidOfBottomOfDentBoundaryf, //const int nWidOfBottomOfDentBoundaryf,
					nLenOfBottomOfDentBoundaryf, //const int nLenOfBottomOfDentBoundaryf,
					sMask_Imagef); // COLOR_IMAGE *sMask_Imagef) //

				if (nResf == -2)
				{
					nNumOfDentsUnfilledTotf += 1;
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n A next nNumOfDentsUnfilledTotf dent 1: nNumOfDentsUnfilledTotf = %d, nNumOfDentsTotf = %d",
						nNumOfDentsUnfilledTotf, nNumOfDentsTotf);

					fprintf(fout_lr, "\n nWidOfObjectBoundaryOfDentInitf = %d, nLenOfObjectBoundaryOfDentInitf = %d, nWidOfBottomOfDentBoundaryf = %d, nLenOfBottomOfDentBoundaryf = %d",
						nWidOfObjectBoundaryOfDentInitf, nLenOfObjectBoundaryOfDentInitf, nWidOfBottomOfDentBoundaryf, nLenOfBottomOfDentBoundaryf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				}//if (nResf == -2)
				else if (nResf == UNSUCCESSFUL_RETURN)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n An UNSUCCESSFUL_RETURN from 'AdjustingBoundariesAndFillingOutPixelsOfADent' 1: nWidOfBottomOfDentBoundaryf = %d, nLenOfBottomOfDentBoundaryf = %d, nNumOfDentsWithNoBottomFoundf = %d",
							nWidOfBottomOfDentBoundaryf, nLenOfBottomOfDentBoundaryf, nNumOfDentsWithNoBottomFoundf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN;
				} // else if (nResf == UNSUCCESSFUL_RETURN)

				nLenOfObjectBoundaryPrevf = nLenOfObjectBoundaryCurf;

				continue; //for (iWidf = 0; iWidf <
			} // if (nDiffOfLensPrevAndCurf > nDiffOfLensPrevAndCurforADentMin)

			nLenOfObjectBoundaryPrevf = nLenOfObjectBoundaryCurf;
		} //for (iWidf = 0; iWidf < sMask_Imagef->nWidth; iWidf++)

	} // if (sMask_Imagef->nSideOfObjectLocation == -1)

	  ///////////////////////////////////////////////////////////////////////////////
	  //object to the right
	if (sMask_Imagef->nSideOfObjectLocation == 1)
	{
		nLenOfObjectBoundaryPrevf = -1;

		nLenOfObjectBoundaryOfDentInitf = -1; // no dent yet
		nWidOfObjectBoundaryOfDentInitf = -1;

		nWidOfBottomOfDentBoundaryf = -1;

		nWidOfObjectBoundaryOfDentPrevf = 0;

		//finding the left and right boundaries of black object pixels 
		for (iWidf = 0; iWidf < sMask_Imagef->nWidth; iWidf++)
		{
			if (iWidf < nWidOfBottomOfDentBoundaryf)
			{
				nWidOfObjectBoundaryOfDentPrevf = iWidf;
				continue;
			}//if (iWidf < nWidOfBottomOfDentBoundaryf)

			nLenOfObjectBoundaryCurf = sMask_Imagef->nLenObjectBoundary_Arr[iWidf];

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n 'Smoothing_ObjectBoundaries' (the right location): the next iWidf = %d, init nLenOfObjectBoundaryCurf = %d, nLenOfObjectBoundaryPrevf = %d",
				iWidf, nLenOfObjectBoundaryCurf, nLenOfObjectBoundaryPrevf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sMask_Imagef->nLength - 1)
			{
				if (nLenOfObjectBoundaryCurf == -1)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n No boundary in 'Smoothing_ObjectBoundaries' 2: nLenOfObjectBoundaryCurf = %d, sMask_Imagef->nLength - 1 = %d, iWidf = %d",
						nLenOfObjectBoundaryCurf, sMask_Imagef->nLength - 1, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					continue;
				} //if (nLenOfObjectBoundaryCurf == -1)

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'Smoothing_ObjectBoundaries' 2: nLenOfObjectBoundaryCurf = %d, sMask_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sMask_Imagef->nLength - 1, iWidf);
				printf("\n\n An error in 'Smoothing_ObjectBoundaries' 1: nLenOfObjectBoundaryCurf = %d, sMask_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sMask_Imagef->nLength - 1, iWidf);

				//printf("\n\n Please press any key to exit");	getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}// if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sMask_Imagef->nLength - 1)

			if (nLenOfObjectBoundaryPrevf == -1)
			{
				nLenOfObjectBoundaryPrevf = nLenOfObjectBoundaryCurf;
				continue; //for (iWidf = 0; iWidf < sMask_Imagef->nWidth; iWidf++)
			} //if (nLenOfObjectBoundaryPrevf == -1)

			nDiffOfLensPrevAndCurf = nLenOfObjectBoundaryCurf - nLenOfObjectBoundaryPrevf;

			if (nDiffOfLensPrevAndCurf > nDiffOfLensCurAndPrevForADentMin) // a dent has been found
			{
				nNumOfDentsTotf += 1;

				nLenOfObjectBoundaryOfDentInitf = nLenOfObjectBoundaryCurf;
				nWidOfObjectBoundaryOfDentInitf = iWidf;

				nWidOfObjectBoundaryOfDentPrevf = iWidf - 1;
				nLenOfObjectBoundaryOfDentPrevf = nLenOfObjectBoundaryPrevf;

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n A next dent 2: nNumOfDentsTotf = %d, iWidf = %d, nDiffOfLensPrevAndCurf = %d", nNumOfDentsTotf, iWidf, nDiffOfLensPrevAndCurf);

				fprintf(fout_lr, "\n nWidOfObjectBoundaryOfDentInitf = %d, nLenOfObjectBoundaryOfDentInitf = %d, nLenOfObjectBoundaryOfDentPrevf = %d",
					nWidOfObjectBoundaryOfDentInitf, nLenOfObjectBoundaryOfDentInitf, nLenOfObjectBoundaryOfDentPrevf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				/////////////////////////////////////////////////////////////////////////////////
				//finding the bottom edge of the dent

				nResf = Finding_BottomOfDentBoundary(
					nLenOfObjectBoundaryOfDentInitf, //const int nLenOfObjectBoundaryOfDentInitf,
					nWidOfObjectBoundaryOfDentInitf, //const int nWidOfObjectBoundaryOfDentInitf,

					sMask_Imagef, //COLOR_IMAGE *sMask_Imagef,

					nWidOfBottomOfDentBoundaryf, //int &nWidOfBottomOfDentBoundaryf,
					nLenOfBottomOfDentBoundaryf); // int &nLenOfBottomOfDentBoundaryf); //

												  //////////////////////////////////////////////

				if (nResf == SUCCESSFUL_RETURN)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n The bottom of the dent 2: nWidOfBottomOfDentBoundaryf = %d, nLenOfBottomOfDentBoundaryf = %d",
						nWidOfBottomOfDentBoundaryf, nLenOfBottomOfDentBoundaryf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				}// if (nResf == SUCCESSFUL_RETURN)
				if (nResf == -2)
				{
					nNumOfDentsWithNoBottomFoundf += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n The bottom of the dent has not been found 2: nWidOfBottomOfDentBoundaryf = %d, nLenOfBottomOfDentBoundaryf = %d, nNumOfDentsWithNoBottomFoundf = %d",
						nWidOfBottomOfDentBoundaryf, nLenOfBottomOfDentBoundaryf, nNumOfDentsWithNoBottomFoundf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					nLenOfObjectBoundaryPrevf = nLenOfObjectBoundaryCurf;
					continue;
				}// if (nResf == -2)
				else if (nResf == UNSUCCESSFUL_RETURN)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An UNSUCCESSFUL_RETURN from 'Finding_BottomOfDentBoundary' 2: nWidOfBottomOfDentBoundaryf = %d, nLenOfBottomOfDentBoundaryf = %d, nNumOfDentsWithNoBottomFoundf = %d",
						nWidOfBottomOfDentBoundaryf, nLenOfBottomOfDentBoundaryf, nNumOfDentsWithNoBottomFoundf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					return UNSUCCESSFUL_RETURN;
				}// if (nResf == UNSUCCESSFUL_RETURN)

				nResf = AdjustingBoundariesAndFillingOutPixelsOfADent(
					nLenOfObjectBoundaryOfDentPrevf, //const int nLenOfObjectBoundaryOfDentPrevf,
					nWidOfObjectBoundaryOfDentPrevf, //const int nWidOfObjectBoundaryOfDentPrevf,

					nWidOfBottomOfDentBoundaryf, //const int nWidOfBottomOfDentBoundaryf,
					nLenOfBottomOfDentBoundaryf, //const int nLenOfBottomOfDentBoundaryf,
					sMask_Imagef); // COLOR_IMAGE *sMask_Imagef) //

				if (nResf == -2)
				{
					nNumOfDentsUnfilledTotf += 1;
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n A next nNumOfDentsUnfilledTotf dent 2: nNumOfDentsUnfilledTotf = %d, nNumOfDentsTotf = %d",
						nNumOfDentsUnfilledTotf, nNumOfDentsTotf);

					fprintf(fout_lr, "\n nWidOfObjectBoundaryOfDentInitf = %d, nLenOfObjectBoundaryOfDentInitf = %d, nWidOfBottomOfDentBoundaryf = %d, nLenOfBottomOfDentBoundaryf = %d",
						nWidOfObjectBoundaryOfDentInitf, nLenOfObjectBoundaryOfDentInitf, nWidOfBottomOfDentBoundaryf, nLenOfBottomOfDentBoundaryf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				}//if (nResf == -2)
				else if (nResf == UNSUCCESSFUL_RETURN)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An UNSUCCESSFUL_RETURN from 'AdjustingBoundariesAndFillingOutPixelsOfADent' 2: nWidOfBottomOfDentBoundaryf = %d, nLenOfBottomOfDentBoundaryf = %d, nNumOfDentsWithNoBottomFoundf = %d",
						nWidOfBottomOfDentBoundaryf, nLenOfBottomOfDentBoundaryf, nNumOfDentsWithNoBottomFoundf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					return UNSUCCESSFUL_RETURN;
				} // else if (nResf == UNSUCCESSFUL_RETURN)

				nLenOfObjectBoundaryPrevf = nLenOfObjectBoundaryCurf;
				continue;
			} // if (nDiffOfLensPrevAndCurf > nDiffOfLensPrevAndCurforADentMin)

			nLenOfObjectBoundaryPrevf = nLenOfObjectBoundaryCurf;
		} //for (iWidf = 0; iWidf < sMask_Imagef->nWidth; iWidf++)

	} // if (sMask_Imagef->nSideOfObjectLocation == 1)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iWidf = nWidSubimageToPrintMin; iWidf < nWidSubimageToPrintMax; iWidf++)
	{
		nLenOfObjectBoundaryCurf = sMask_Imagef->nLenObjectBoundary_Arr[iWidf];

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n After smoothing: iWidf = %d, fin nLenOfObjectBoundaryCurf = %d\n", iWidf, nLenOfObjectBoundaryCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	}//if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n The total: nNumOfDentsUnfilledTotf = %d, nNumOfDentsWithNoBottomFoundf = %d, nNumOfDentsTotf = %d", nNumOfDentsUnfilledTotf, nNumOfDentsWithNoBottomFoundf, nNumOfDentsTotf);
	fprintf(fout_lr, "\n\n The total: nNumOfDentsUnfilledTotf = %d, nNumOfDentsWithNoBottomFoundf = %d, nNumOfDentsTotf = %d", nNumOfDentsUnfilledTotf, nNumOfDentsWithNoBottomFoundf, nNumOfDentsTotf);

	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	return SUCCESSFUL_RETURN;
} //int Smoothing_ObjectBoundaries(...

  ///////////////////////////////////////////////////////////////////////////

int Finding_BottomOfDentBoundary(
	const int nLenOfObjectBoundaryOfDentInitf,
	const int nWidOfObjectBoundaryOfDentInitf,

	MASK_IMAGE *sMask_Imagef,

	int &nWidOfBottomOfDentBoundaryf,
	int &nLenOfBottomOfDentBoundaryf) //
{
	int
		//nResf = -1,
		nWidBottomMaxf,

		nLenOfObjectBoundaryPrevf,
		nLenOfObjectBoundaryCurf,
		nDiffOfLensCurAndPrevf,

		iWidf;

	if (nWidOfObjectBoundaryOfDentInitf < 0 || nWidOfObjectBoundaryOfDentInitf > sMask_Imagef->nWidth - 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Finding_BottomOfDentBoundary': nWidOfObjectBoundaryOfDentInitf = %d,  sMask_Imagef->nWidth - 1 = %d",
			nWidOfObjectBoundaryOfDentInitf, sMask_Imagef->nWidth - 1);

		fprintf(fout_lr, "\n\n An error in 'Finding_BottomOfDentBoundary': nWidOfObjectBoundaryOfDentInitf = %d,  sMask_Imagef->nWidth - 1 = %d",
			nWidOfObjectBoundaryOfDentInitf, sMask_Imagef->nWidth - 1);

		//printf("\n\n Please press any key to exit");	getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS		

		return UNSUCCESSFUL_RETURN;
	} // if (nWidOfObjectBoundaryOfDentInitf < 0 || nWidOfObjectBoundaryOfDentInitf > sMask_Imagef->nWidth - 1)


	nWidBottomMaxf = nWidOfObjectBoundaryOfDentInitf + nWidthOfBoundaryDentMax;

	if (nWidBottomMaxf >= sMask_Imagef->nWidth - 1)
	{
		nWidBottomMaxf = sMask_Imagef->nWidth - 1;
	} // if (nWidBottomMaxf >= sMask_Imagef->nWidth - 1)

	nLenOfObjectBoundaryPrevf = nLenOfObjectBoundaryOfDentInitf;
	for (iWidf = nWidOfObjectBoundaryOfDentInitf + 1; iWidf <= nWidBottomMaxf; iWidf++)
	{
		nLenOfObjectBoundaryCurf = sMask_Imagef->nLenObjectBoundary_Arr[iWidf];

		//object to the left
		if (sMask_Imagef->nSideOfObjectLocation == -1)
		{
			nDiffOfLensCurAndPrevf = nLenOfObjectBoundaryCurf - nLenOfObjectBoundaryPrevf;

			//new
			if (nLenOfObjectBoundaryCurf - nLenOfObjectBoundaryOfDentInitf > nDiffOfLensCurAndPrevf)
				nDiffOfLensCurAndPrevf = nLenOfObjectBoundaryCurf - nLenOfObjectBoundaryOfDentInitf;

		} //if (sMask_Imagef->nSideOfObjectLocation == -1)

		  //object to the right
		if (sMask_Imagef->nSideOfObjectLocation == 1)
		{
			nDiffOfLensCurAndPrevf = nLenOfObjectBoundaryPrevf - nLenOfObjectBoundaryCurf; //vice versa

			if (nLenOfObjectBoundaryOfDentInitf - nLenOfObjectBoundaryCurf > nDiffOfLensCurAndPrevf)
				nDiffOfLensCurAndPrevf = nLenOfObjectBoundaryOfDentInitf - nLenOfObjectBoundaryCurf;

		} // if (sMask_Imagef->nSideOfObjectLocation == 1)

		if (nDiffOfLensCurAndPrevf > nDiffOfLensCurAndPrevForADentMin)
		{
			nWidOfBottomOfDentBoundaryf = iWidf;
			nLenOfBottomOfDentBoundaryf = nLenOfObjectBoundaryCurf;
			return SUCCESSFUL_RETURN;
		}//if (nDiffOfLensCurAndPrevf > nDiffOfLensCurAndPrevForADentMin)

		if (iWidf == nWidBottomMaxf)
		{
			nWidOfBottomOfDentBoundaryf = iWidf;
			nLenOfBottomOfDentBoundaryf = nLenOfObjectBoundaryCurf;
			return (-2);
		} // if (iWidf == nWidBottomMaxf)

		nLenOfObjectBoundaryPrevf = nLenOfObjectBoundaryCurf;
	}// for (iWidf = nWidOfObjectBoundaryOfDentInitf; iWidf <= nWidBottomMaxf; iWidf++)

	return UNSUCCESSFUL_RETURN;
} //int Finding_BottomOfDentBoundary(...)
  ////////////////////////////////////////////////////////////////////////

int AdjustingBoundariesAndFillingOutPixelsOfADent(
	const int nLenOfObjectBoundaryOfDentPrevf,
	const int nWidOfObjectBoundaryOfDentPrevf,

	const int nWidOfBottomOfDentBoundaryf,
	const int nLenOfBottomOfDentBoundaryf,

	MASK_IMAGE *sMask_Imagef) //
{
	int
		//nResf = -1,
		nNumOfPixelsReplacedTotf = 0,

		nNumOfPixelsReplacedInARowf,
		nIndexOfPixelCurf,

		nLenOfObjectBoundaryCurf,

		iLenf,
		iWidf;

	float
		fSlopef;

	//the same column
	if (nLenOfBottomOfDentBoundaryf == nLenOfObjectBoundaryOfDentPrevf)
	{
		if (nWidOfObjectBoundaryOfDentPrevf >= nWidOfBottomOfDentBoundaryf)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'AdjustingBoundariesAndFillingOutPixelsOfADent': nWidOfObjectBoundaryOfDentPrevf = %d >= nWidOfBottomOfDentBoundaryf = %d,  sMask_Imagef->nWidth - 1 = %d",
				nWidOfObjectBoundaryOfDentPrevf, nWidOfBottomOfDentBoundaryf, sMask_Imagef->nWidth - 1);
			fprintf(fout_lr, "\n\n An error in 'AdjustingBoundariesAndFillingOutPixelsOfADent': nWidOfObjectBoundaryOfDentPrevf = %d >= nWidOfBottomOfDentBoundaryf = %d,  sMask_Imagef->nWidth - 1 = %d",
				nWidOfObjectBoundaryOfDentPrevf, nWidOfBottomOfDentBoundaryf, sMask_Imagef->nWidth - 1);

		//	printf("\n\n Please press any key to exit");	getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS			

			return UNSUCCESSFUL_RETURN;
		}//if (nWidOfObjectBoundaryOfDentPrevf >= nWidOfBottomOfDentBoundaryf)

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'AdjustingBoundariesAndFillingOutPixelsOfADent': nLenOfBottomOfDentBoundaryf = %d == nLenOfObjectBoundaryOfDentPrevf, nWidOfObjectBoundaryOfDentPrevf = %d, nWidOfBottomOfDentBoundaryf = %d,  sMask_Imagef->nWidth - 1 = %d",
			nLenOfBottomOfDentBoundaryf, nWidOfObjectBoundaryOfDentPrevf, nWidOfBottomOfDentBoundaryf, sMask_Imagef->nWidth - 1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		for (iWidf = nWidOfObjectBoundaryOfDentPrevf; iWidf <= nWidOfBottomOfDentBoundaryf; iWidf++)
		{
			nNumOfPixelsReplacedInARowf = 0;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n iWidf = %d, sMask_Imagef->nLenObjectBoundary_Arr[%d] = %d, the same nLenOfBottomOfDentBoundaryf = %d, sMask_Imagef->nLength = %d",
				iWidf, iWidf, sMask_Imagef->nLenObjectBoundary_Arr[iWidf], nLenOfBottomOfDentBoundaryf, sMask_Imagef->nLength);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			if (sMask_Imagef->nSideOfObjectLocation == -1) //object is to the left
			{
				for (iLenf = sMask_Imagef->nLenObjectBoundary_Arr[iWidf]; iLenf <= nLenOfBottomOfDentBoundaryf; iLenf++)
				{
					nNumOfPixelsReplacedTotf += 1;
					nNumOfPixelsReplacedInARowf += 1;

					nIndexOfPixelCurf = iLenf + (iWidf*sMask_Imagef->nLength); // nLenMax);

					sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCurf] = 1; // nRedForDentFillingOut;

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n The color replacement in the dent (left object): iLenf = %d, nNumOfPixelsReplacedTotf = %d, nNumOfPixelsReplacedInARowf = %d, sMask_Imagef->nMaskImage_Arr[%d] = %d",
						iLenf, nNumOfPixelsReplacedTotf, nIndexOfPixelCurf, nNumOfPixelsReplacedInARowf, sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCurf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				} //for (iLenf = sMask_Imagef->nLenObjectBoundary_Arr[iWidf]; iLenf <= nLenOfBottomOfDentBoundaryf; iLenf++)

			} //if (sMask_Imagef->nSideOfObjectLocation == -1)//object is to the left

			else if (sMask_Imagef->nSideOfObjectLocation == 1) //object is to the right
			{
				for (iLenf = nLenOfBottomOfDentBoundaryf; iLenf <= sMask_Imagef->nLenObjectBoundary_Arr[iWidf]; iLenf++)
				{
					nNumOfPixelsReplacedTotf += 1;
					nNumOfPixelsReplacedInARowf += 1;

					nIndexOfPixelCurf = iLenf + (iWidf*sMask_Imagef->nLength); // nLenMax);

					sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCurf] = 1; // nRedForDentFillingOut;

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n The color replacement in the dent (right object): iLenf = %d, nNumOfPixelsReplacedTotf = %d, nNumOfPixelsReplacedInARowf = %d, sMask_Imagef->nRed_Arr[%d] = %d",
						iLenf, nNumOfPixelsReplacedTotf, nNumOfPixelsReplacedInARowf, nIndexOfPixelCurf, sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCurf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				} //for (iLenf = nLenOfBottomOfDentBoundaryf; iLenf <= sMask_Imagef->nLenObjectBoundary_Arr[iWidf]; iLenf++)

			} //else if (sMask_Imagef->nSideOfObjectLocation == 1)//object is to the right

			sMask_Imagef->nLenObjectBoundary_Arr[iWidf] = nLenOfBottomOfDentBoundaryf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n A new sMask_Imagef->nLenObjectBoundary_Arr[%d] = %d, nNumOfPixelsReplacedInARowf = %d, nNumOfPixelsReplacedTotf = %d",
				iWidf, sMask_Imagef->nLenObjectBoundary_Arr[iWidf], nNumOfPixelsReplacedInARowf, nNumOfPixelsReplacedTotf);
			if (nNumOfPixelsReplacedInARowf == 0)
			{
				fprintf(fout_lr, "\n\n A warning in iWidf = %d, nNumOfPixelsReplacedInARowf == 0", iWidf);
			}// if (nNumOfPixelsReplacedInARowf == 0)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		} //for (iWidf = nWidOfObjectBoundaryOfDentPrevf; iWidf <= nWidOfBottomOfDentBoundaryf; iWidf++)
	}//if (nLenOfBottomOfDentBoundaryf == nLenOfObjectBoundaryOfDentPrevf)

	else //if (nLenOfBottomOfDentBoundaryf != nLenOfObjectBoundaryOfDentPrevf)
	{
		fSlopef = (float)(nWidOfBottomOfDentBoundaryf - nWidOfObjectBoundaryOfDentPrevf) / (float)(nLenOfBottomOfDentBoundaryf - nLenOfObjectBoundaryOfDentPrevf);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'AdjustingBoundariesAndFillingOutPixelsOfADent': nLenOfBottomOfDentBoundaryf = %d, nLenOfObjectBoundaryOfDentPrevf = %d,  sMask_Imagef->nWidth - 1 = %d",
			nLenOfBottomOfDentBoundaryf, nLenOfObjectBoundaryOfDentPrevf, sMask_Imagef->nWidth - 1);

		fprintf(fout_lr, "\n fSlopef = %E, nWidOfObjectBoundaryOfDentPrevf = %d, nWidOfBottomOfDentBoundaryf = %d, sMask_Imagef->nWidth - 1 = %d",
			fSlopef, nWidOfObjectBoundaryOfDentPrevf, nWidOfBottomOfDentBoundaryf, sMask_Imagef->nWidth - 1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		for (iWidf = nWidOfObjectBoundaryOfDentPrevf + 1; iWidf <= nWidOfBottomOfDentBoundaryf; iWidf++)
		{
			nNumOfPixelsReplacedInARowf = 0;

			nLenOfObjectBoundaryCurf = nLenOfObjectBoundaryOfDentPrevf + (int)((float)(iWidf - nWidOfObjectBoundaryOfDentPrevf) / fSlopef);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n iWidf = %d, prev sMask_Imagef->nLenObjectBoundary_Arr[%d] = %d, the new nLenOfObjectBoundaryCurf = %d, sMask_Imagef->nLength = %d",
				iWidf, iWidf, sMask_Imagef->nLenObjectBoundary_Arr[iWidf], nLenOfObjectBoundaryCurf, sMask_Imagef->nLength);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			if (sMask_Imagef->nSideOfObjectLocation == -1) //object is to the left
			{

#ifndef COMMENT_OUT_ALL_PRINTS
				if (nLenOfObjectBoundaryCurf < sMask_Imagef->nLenObjectBoundary_Arr[iWidf])
				{
					fprintf(fout_lr, "\n\n A warning in the pixel replacement, iWidf = %d, nLenOfObjectBoundaryCurf = %d < sMask_Imagef->nLenObjectBoundary_Arr[iWidf] = %d",
						iWidf, nLenOfObjectBoundaryCurf, sMask_Imagef->nLenObjectBoundary_Arr[iWidf]);
				}// if (nLenOfObjectBoundaryCurf < sMask_Imagef->nLenObjectBoundary_Arr[iWidf])
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				for (iLenf = sMask_Imagef->nLenObjectBoundary_Arr[iWidf]; iLenf <= nLenOfObjectBoundaryCurf; iLenf++)
				{
					nNumOfPixelsReplacedTotf += 1;
					nNumOfPixelsReplacedInARowf += 1;

					nIndexOfPixelCurf = iLenf + (iWidf*sMask_Imagef->nLength); // nLenMax);

					sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCurf] = 1; // nRedForDentFillingOut;

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n The color replacement in the dent: iLenf = %d, nNumOfPixelsReplacedTotf = %d, nNumOfPixelsReplacedInARowf = %d, sMask_Imagef->nMaskImage_Arr[%d] = %d",
						iLenf, nNumOfPixelsReplacedTotf, nNumOfPixelsReplacedInARowf, nIndexOfPixelCurf, sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCurf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				} //for (iLenf = sMask_Imagef->nLenObjectBoundary_Arr[iWidf]; iLenf <= nLenOfObjectBoundaryCurf; iLenf++)
			} //if (sMask_Imagef->nSideOfObjectLocation == -1) //object is to the left

			else if (sMask_Imagef->nSideOfObjectLocation == 1) //object is to the right
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				if (nLenOfObjectBoundaryCurf > sMask_Imagef->nLenObjectBoundary_Arr[iWidf])
				{
					fprintf(fout_lr, "\n\n A warning in the pixel replacement, iWidf = %d, nLenOfObjectBoundaryCurf = %d > sMask_Imagef->nLenObjectBoundary_Arr[iWidf] = %d",
						iWidf, nLenOfObjectBoundaryCurf, sMask_Imagef->nLenObjectBoundary_Arr[iWidf]);
				}// if (nLenOfObjectBoundaryCurf > sMask_Imagef->nLenObjectBoundary_Arr[iWidf])
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				for (iLenf = nLenOfObjectBoundaryCurf; iLenf <= sMask_Imagef->nLenObjectBoundary_Arr[iWidf]; iLenf++)
				{
					nNumOfPixelsReplacedTotf += 1;
					nNumOfPixelsReplacedInARowf += 1;

					nIndexOfPixelCurf = iLenf + (iWidf*sMask_Imagef->nLength); // nLenMax);

					sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCurf] = 1; // nRedForDentFillingOut;

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n The color replacement in the dent: iLenf = %d, nNumOfPixelsReplacedTotf = %d, nNumOfPixelsReplacedInARowf = %d, sMask_Imagef->nMaskImage_Arr[%d] = %d",
						iLenf, nNumOfPixelsReplacedTotf, nNumOfPixelsReplacedInARowf, nIndexOfPixelCurf, sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCurf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				} //for (iLenf = nLenOfObjectBoundaryCurf; iLenf <= sMask_Imagef->nLenObjectBoundary_Arr[iWidf]; iLenf++)

			} //elseif (sMask_Imagef->nSideOfObjectLocation == 1)//object is to the right

			sMask_Imagef->nLenObjectBoundary_Arr[iWidf] = nLenOfObjectBoundaryCurf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n A new sMask_Imagef->nLenObjectBoundary_Arr[%d] = %d, nNumOfPixelsReplacedInARowf = %d, nNumOfPixelsReplacedTotf = %d",
				iWidf, sMask_Imagef->nLenObjectBoundary_Arr[iWidf], nNumOfPixelsReplacedInARowf, nNumOfPixelsReplacedTotf);

			if (nNumOfPixelsReplacedInARowf == 0)
			{
				fprintf(fout_lr, "\n\n A warning in iWidf = %d, nNumOfPixelsReplacedInARowf == 0", iWidf);
			}// if (nNumOfPixelsReplacedInARowf == 0)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		}//for (iWidf = nWidOfObjectBoundaryOfDentPrevf + 1; iWidf <= nWidOfBottomOfDentBoundaryf; iWidf++)

	}//else //if (nLenOfBottomOfDentBoundaryf != nLenOfObjectBoundaryOfDentPrevf)

	if (nNumOfPixelsReplacedTotf == 0)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n A warning in 'AdjustingBoundariesAndFillingOutPixelsOfADent': no dent pixels have been replaced");
		fprintf(fout_lr, "\n\n A warning in 'AdjustingBoundariesAndFillingOutPixelsOfADent': no dent pixels have been replaced");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return (-2);
	}//if (nNumOfPixelsReplacedTotf == 0)

	return SUCCESSFUL_RETURN;
} //int AdjustingBoundariesAndFillingOutPixelsOfADent(...)

  /////////////////////////////////////////////////////////////////////
int FillingOut_BlackObjectPixels(

	COLOR_IMAGE *sColor_Imagef) //[nImageSizeMax]
{
	int
		nIndexOfPixelCurf,

		iWidf,
		iLenf,

		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		nBottomOfBlackPixelsf = -1,
		nTopOfBlackPixelsf = -1,

		nNumOfBlackPixelsInALineTotf,
		nLenOfObjectBoundaryCurf;

	int *nLeftBoundaryOfBlackPixelsArrf = new int[sColor_Imagef->nWidth];

	int *nRightBoundaryOfBlackPixelsArrf = new int[sColor_Imagef->nWidth];

	if (nLeftBoundaryOfBlackPixelsArrf == nullptr || nRightBoundaryOfBlackPixelsArrf == nullptr)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'FillingOut_BlackObjectPixels': nLeftBoundaryOfBlackPixelsArrf == nullptr || nRightBoundaryOfBlackPixelsArrf == nullptr");
		fprintf(fout_lr, "\n\n An error in 'FillingOut_BlackObjectPixels': nLeftBoundaryOfBlackPixelsArrf == nullptr || nRightBoundaryOfBlackPixelsArrf == nullptr");

		//printf("\n\n Please press any key to exit"); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS		

		delete[] nLeftBoundaryOfBlackPixelsArrf;
		delete[] nRightBoundaryOfBlackPixelsArrf;
		return UNSUCCESSFUL_RETURN;
	} // if (nLeftBoundaryOfBlackPixelsArrf == nullptr || nRightBoundaryOfBlackPixelsArrf == nullptr)

	  //object to the left   
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{

		//finding the left and right boundaries of black object pixels 
		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			nLeftBoundaryOfBlackPixelsArrf[iWidf] = -1;
			nRightBoundaryOfBlackPixelsArrf[iWidf] = -1;

			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];
			if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)
			{
				if (nLenOfObjectBoundaryCurf == -1)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n No boundary/erasing in 'FillingOut_BlackObjectPixels' 1: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
						nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					continue;
				} //if (nLenOfObjectBoundaryCurf == -1)

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'FillingOut_BlackObjectPixels' 1: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
				printf("\n\n An error in 'FillingOut_BlackObjectPixels' 1: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				//printf("\n\n Please press any key to exit"); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] nLeftBoundaryOfBlackPixelsArrf;
				delete[] nRightBoundaryOfBlackPixelsArrf;
				return UNSUCCESSFUL_RETURN;
			}// if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)

			 //start counting from the left
			nNumOfBlackPixelsInALineTotf = 0;
			for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)
				//for (iLenf = sColor_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryCurf; iLenf--)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				//if (nRedCurf != sColor_Imagef->nIntensityOfBackground_Red)
				if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
					nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
					//if (nRedCurf <= nIntensityCloseToBlackMax && nGreenCurf <= nIntensityCloseToBlackMax &&
					//nBlueCurf <= nIntensityCloseToBlackMax)
				{

					if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] == 0)
					{
						sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 2; //was replaced
						sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nIntensityToReplaceBlackObjectPixels;
						sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nIntensityToReplaceBlackObjectPixels;
						sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nIntensityToReplaceBlackObjectPixels;

						nNumOfBlackPixelsInALineTotf += 1;

						if (nBottomOfBlackPixelsf == -1)
						{
							nBottomOfBlackPixelsf = iWidf;
						} //if (nBottomOfBlackPixelsf == -1)

						if (nBottomOfBlackPixelsf != -1 && nTopOfBlackPixelsf != iWidf)
						{
							nTopOfBlackPixelsf = iWidf;
						} //if (nBottomOfBlackPixelsf != -1 && ..)

						  //sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = sColor_Imagef->nIntensityOfBackground_Red;
						if (nLeftBoundaryOfBlackPixelsArrf[iWidf] == -1)
						{
							nLeftBoundaryOfBlackPixelsArrf[iWidf] = iLenf;
						} //if (nLeftBoundaryOfBlackPixelsArrf[iWidf] == -1)

						if (nLeftBoundaryOfBlackPixelsArrf[iWidf] != -1)
						{
							nRightBoundaryOfBlackPixelsArrf[iWidf] = iLenf;
						} //if (nLeftBoundaryOfBlackPixelsArrf[iWidf] != -1)

					} // if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] == 0)

				} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...

			} //for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)

		} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
	} // if (sColor_Imagef->nSideOfObjectLocation == -1)

	  //object to the right
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{
		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			//start counting from the right

			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];
			if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)
			{
				if (nLenOfObjectBoundaryCurf == -1)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n No boundary/erasing in 'FillingOut_BlackObjectPixels' 2: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
						nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					continue;
				} //if (nLenOfObjectBoundaryCurf == -1)

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'FillingOut_BlackObjectPixels' 2: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
				printf("\n\n An error in 'FillingOut_BlackObjectPixels' 2: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				//printf("\n\n Please press any key to exit");	getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				delete[] nLeftBoundaryOfBlackPixelsArrf;
				delete[] nRightBoundaryOfBlackPixelsArrf;
				return UNSUCCESSFUL_RETURN;
			}// if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)

			nLeftBoundaryOfBlackPixelsArrf[iWidf] = -1;
			nRightBoundaryOfBlackPixelsArrf[iWidf] = -1;

			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];
			if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)
			{
				if (nLenOfObjectBoundaryCurf == -1)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n No boundary/erasing in 'FillingOut_BlackObjectPixels' 1: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
						nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					continue;
				} //if (nLenOfObjectBoundaryCurf == -1)

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'FillingOut_BlackObjectPixels' 1: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
				printf("\n\n An error in 'FillingOut_BlackObjectPixels' 1: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				//printf("\n\n Please press any key to exit"); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				delete[] nLeftBoundaryOfBlackPixelsArrf;
				delete[] nRightBoundaryOfBlackPixelsArrf;
				return UNSUCCESSFUL_RETURN;
			}// if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)

			 //start counting from the left
			nNumOfBlackPixelsInALineTotf = 0;
			for (iLenf = sColor_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryCurf; iLenf--)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				//if (nRedCurf != sColor_Imagef->nIntensityOfBackground_Red)
				//if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green &&
				//nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
				if (nRedCurf <= nIntensityCloseToBlackMax && nGreenCurf <= nIntensityCloseToBlackMax &&
					nBlueCurf <= nIntensityCloseToBlackMax)
				{

					if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] == 0)
					{
						sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 2; //was replaced
						sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nIntensityToReplaceBlackObjectPixels;
						sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nIntensityToReplaceBlackObjectPixels;
						sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nIntensityToReplaceBlackObjectPixels;

						nNumOfBlackPixelsInALineTotf += 1;

						if (nBottomOfBlackPixelsf == -1)
						{
							nBottomOfBlackPixelsf = iWidf;
						} //if (nBottomOfBlackPixelsf == -1)

						if (nBottomOfBlackPixelsf != -1 && nTopOfBlackPixelsf != iWidf)
						{
							nTopOfBlackPixelsf = iWidf;
						} //if (nBottomOfBlackPixelsf != -1 && ..)

						  //sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = sColor_Imagef->nIntensityOfBackground_Red;
						if (nLeftBoundaryOfBlackPixelsArrf[iWidf] == -1)
						{
							nLeftBoundaryOfBlackPixelsArrf[iWidf] = iLenf;
						} //if (nLeftBoundaryOfBlackPixelsArrf[iWidf] == -1)

						if (nLeftBoundaryOfBlackPixelsArrf[iWidf] != -1)
						{
							nRightBoundaryOfBlackPixelsArrf[iWidf] = iLenf;
						} //if (nLeftBoundaryOfBlackPixelsArrf[iWidf] != -1)

					} // if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] == 0)

				} // if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && ...

			} //for (iLenf = sColor_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryCurf; iLenf--)

		} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
	} // if (sColor_Imagef->nSideOfObjectLocation == 1)

	delete[] nLeftBoundaryOfBlackPixelsArrf;
	delete[] nRightBoundaryOfBlackPixelsArrf;

	return SUCCESSFUL_RETURN;
} //int FillingOut_BlackObjectPixels(...
  ///////////////////////////////////////////////////////////////////

int Initializing_Color_To_CurSize(
	const int nImageWidthf,
	const int nImageLengthf,

	COLOR_IMAGE *sColor_Imagef) //[]
{
	int
		nIndexOfPixelCurf,

		nImageSizeCurf = nImageWidthf*nImageLengthf,
		iWidf,
		iLenf;

	sColor_Imagef->nSideOfObjectLocation = 0; // neither left nor right
	sColor_Imagef->nIntensityOfBackground_Red = -1;
	sColor_Imagef->nIntensityOfBackground_Green = -1;
	sColor_Imagef->nIntensityOfBackground_Blue = -1;

	sColor_Imagef->nWidth = nImageWidthf;
	sColor_Imagef->nLength = nImageLengthf;

	//sGrayscale_Imagef->nPixel_ValidOrNotArr = new int[nImageSizeCurf];

	sColor_Imagef->nLenObjectBoundary_Arr = new int[nImageWidthf];

	sColor_Imagef->nRed_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nGreen_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nBlue_Arr = new int[nImageSizeCurf];

	sColor_Imagef->nIsAPixelBackground_Arr = new int[nImageSizeCurf];

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = -1;

		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			//sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1; //valid for no restrictions
			sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = -1;

			sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 0; //all pixels belong to the object

		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	return SUCCESSFUL_RETURN;
} //int Initializing_Color_To_CurSize(...

  ///////////////////////////////////////////////////////////////////////////////////////////////
int Initializing_MaskImage(
	const int nImageWidthf,
	const int nImageLengthf,

	const COLOR_IMAGE *sColor_Imagef,
	MASK_IMAGE *sMask_Imagef)
{
	int
		nIndexOfPixelCurf,
		iWidf,
		iLenf,

		iPaddf,
		nRedf,
		nGreenf,
		nBluef,

		nLenObjectBoundaryColorCurf,

		nImageSizeCurf = nImageWidthf*nImageLengthf;

	sMask_Imagef->nSideOfObjectLocation = sColor_Imagef->nSideOfObjectLocation; // neither left nor right

	sMask_Imagef->nWidth = nImageWidthf;
	sMask_Imagef->nLength = nImageLengthf;

	/*
	if (( (sMask_Imagef->nWidth - 1) / nWidScaleForPadding)*nWidScaleForPadding == sMask_Imagef->nWidth - 1) //
	//if ((sMask_Imagef->nWidth / nWidScaleForPadding)*nWidScaleForPadding == sMask_Imagef->nWidth) // //there is no gap in width between the last padding point and the bottom edge
	{
	sMask_Imagef->nIs_WidthDivisibleBynScaleForPadding = 1;
	}// if ((sMask_Imagef->nWidth / nWidScaleForPadding)*nWidScaleForPadding == sMask_Imagef->nWidth - 1)
	else
	{
	sMask_Imagef->nIs_WidthDivisibleBynScaleForPadding = 0; // //there is a gap in width between the last padding point and the bottom edge
	}// else

	if (sMask_Imagef->nIs_WidthDivisibleBynScaleForPadding == 1)
	{
	//sMask_Imagef->nNumOfPaddedLengths = (sMask_Imagef->nWidth / nWidScaleForPadding);
	sMask_Imagef->nNumOfPaddedLengths = ( (sMask_Imagef->nWidth - 1) / nWidScaleForPadding) + 1;
	}//if (sMask_Imagef->nIs_WidthDivisibleBynWidScaleForPadding == 1)
	else
	{
	sMask_Imagef->nNumOfPaddedLengths = (sMask_Imagef->nWidth / nWidScaleForPadding) + 1;
	}//else
	*/
	sMask_Imagef->nNumOfPaddedLengths = ((sMask_Imagef->nWidth - 1) / nWidScaleForPadding) + 1;

	sMask_Imagef->nScaledPaddedWid_Arr = new int[sMask_Imagef->nNumOfPaddedLengths];
	sMask_Imagef->nScaledPaddedLen_Arr = new int[sMask_Imagef->nNumOfPaddedLengths];


	for (iPaddf = 0; iPaddf < sMask_Imagef->nNumOfPaddedLengths; iPaddf++)
	{
		sMask_Imagef->nScaledPaddedWid_Arr[iPaddf] = -1;
		sMask_Imagef->nScaledPaddedLen_Arr[iPaddf] = -1;
	} //for (iPaddf = 0; iPaddf < sMask_Imagef->nNumOfPaddedLengths; iPaddf++)
	  ////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n ///////////////////////////////////////////////////////////////////");
	fprintf(fout_lr, "\n 'Initializing_MaskImage':\n");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	sMask_Imagef->nLenObjectBoundary_Arr = new int[nImageWidthf];
	sMask_Imagef->nMaskImage_Arr = new int[nImageSizeCurf];

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		nLenObjectBoundaryColorCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];

		sMask_Imagef->nLenObjectBoundary_Arr[iWidf] = nLenObjectBoundaryColorCurf;	//-1;

																					//if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax)
		{

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n 'Initializing_MaskImage': sMask_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sMask_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		}//if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax)

		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			//		sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCurf] = -1;
			nRedf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

			nGreenf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			nBluef = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			if (nRedf > sColor_Imagef->nIntensityOfBackground_Red || nGreenf > sColor_Imagef->nIntensityOfBackground_Green ||
				nBluef > sColor_Imagef->nIntensityOfBackground_Blue)
			{
				sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCurf] = 1; // nIntensityStatMax; // nRed;

			} //if (nRed > sColor_Image.nIntensityOfBackground_Red || nGreen > sColor_Image.nIntensityOfBackground_Green ||
			else
			{
				sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCurf] = 0; //background
			}//else

		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)

		if (sMask_Imagef->nLenObjectBoundary_Arr[iWidf] == -1 && sMask_Imagef->nSideOfObjectLocation == -1) // the boundary has not been found yet
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'Initializing_MaskImage': the mask boundary has not been found for iWidf = %d, nLenObjectBoundaryColorCurf = %d",
				iWidf, nLenObjectBoundaryColorCurf);
			fprintf(fout_lr, "\n\n An error in 'Initializing_MaskImage': the mask boundary has not been found for iWidf = %d, nLenObjectBoundaryColorCurf = %d",
				iWidf, nLenObjectBoundaryColorCurf);

			//printf("\n\n Please press any key to exit"); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		} //if (sMask_Imagef->nLenObjectBoundary_Arr[iWidf] == -1 && sMask_Imagef->nSideOfObjectLocation == -1) // the boundary has not been found yet
	} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	return SUCCESSFUL_RETURN;
}//int Initializing_MaskImage(...

int PaddedLengthsForMask(
	MASK_IMAGE *sMask_Imagef)
{
	int
		nWidCurf,

		iPaddf;

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n sMask_Imagef->nNumOfPaddedLengths = %d, sMask_Imagef->nWidth = %d", sMask_Imagef->nNumOfPaddedLengths, sMask_Imagef->nWidth);
	fprintf(fout_lr, "\n\n sMask_Imagef->nNumOfPaddedLengths = %d, sMask_Imagef->nWidth = %d", sMask_Imagef->nNumOfPaddedLengths, sMask_Imagef->nWidth);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iPaddf = 0; iPaddf < sMask_Imagef->nNumOfPaddedLengths; iPaddf++)
	{
		nWidCurf = iPaddf*nWidScaleForPadding;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n 'PaddedLengthsForMask': iPaddf = %d, nWidCurf = %d", iPaddf, nWidCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		if (nWidCurf >= sMask_Imagef->nWidth)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n An error in 'PaddedLengthsForMask': nWidCurf = %d >= sMask_Imagef->nWidth = %d",
				nWidCurf, sMask_Imagef->nWidth);

			fprintf(fout_lr, "\n\n An error in 'PaddedLengthsForMask': nWidCurf = %d >= sMask_Imagef->nWidth = %d",
				nWidCurf, sMask_Imagef->nWidth);

			//printf("\n\n Please press any key to exit"); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
		}// if (nWidCurf >= sMask_Imagef->nWidth)

		sMask_Imagef->nScaledPaddedWid_Arr[iPaddf] = nWidCurf;

		if (sMask_Imagef->nSideOfObjectLocation == -1) // left
		{
			sMask_Imagef->nScaledPaddedLen_Arr[iPaddf] = sMask_Imagef->nLenObjectBoundary_Arr[nWidCurf] + nLenForPadding;

			if (sMask_Imagef->nScaledPaddedLen_Arr[iPaddf] >= sMask_Imagef->nLength)
				sMask_Imagef->nScaledPaddedLen_Arr[iPaddf] = sMask_Imagef->nLength - 1;

		} // if (sMask_Imagef->nSideOfObjectLocation == -1)
		else if (sMask_Imagef->nSideOfObjectLocation == 1) // right
		{
			sMask_Imagef->nScaledPaddedLen_Arr[iPaddf] = sMask_Imagef->nLenObjectBoundary_Arr[nWidCurf] - nLenForPadding;

			if (sMask_Imagef->nScaledPaddedLen_Arr[iPaddf] < 0)
				sMask_Imagef->nScaledPaddedLen_Arr[iPaddf] = 0;

		}//else if (sMask_Imagef->nSideOfObjectLocation == 1) // right

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n sMask_Imagef->nScaledPaddedWid_Arr[%d] = %d, sMask_Imagef->nScaledPaddedLen_Arr[%d]  = %d",
			iPaddf, sMask_Imagef->nScaledPaddedWid_Arr[iPaddf], iPaddf, sMask_Imagef->nScaledPaddedLen_Arr[iPaddf]);

		fprintf(fout_lr, "\n\n sMask_Imagef->nScaledPaddedWid_Arr[%d] = %d, sMask_Imagef->nScaledPaddedLen_Arr[%d]  = %d",
			iPaddf, sMask_Imagef->nScaledPaddedWid_Arr[iPaddf], iPaddf, sMask_Imagef->nScaledPaddedLen_Arr[iPaddf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	} //for (iPaddf = 0; iPaddf < sMask_Imagef->nNumOfPaddedLengths; iPaddf++)

	return SUCCESSFUL_RETURN;
}//int PaddedLengthsForMask(...
 ///////////////////////////////////////////////////////////////////////////////////

void Coeffficients_Of_B_Spline(
	const float fTf, //  0 <= fTf <= 1
	COEFS_OF_B_SPLINE_BETWEEN_2_POINTS *sCoef_Of_B_Spline_Between_2_Pointsf)
{
	float
		fSquaref = fTf*fTf,
		fCubef = fTf*fTf*fTf,
		f_1stTermf,
		f_2ndTermf,
		f_3rdTermf,
		f_4thTermf;

	f_1stTermf = (float)(-(fCubef)+(3.0*fSquaref) - 3.0*fTf + 1.0);

	f_2ndTermf = (float)((3.0*fCubef) - (6.0*fSquaref) + 4.0);

	f_3rdTermf = (float)(-(3.0*fCubef) + (3.0*fSquaref) + 3.0*fTf + 1.0);

	f_4thTermf = (float)(fCubef);

	sCoef_Of_B_Spline_Between_2_Pointsf->fCoefLenArr[0] = f_1stTermf;

	sCoef_Of_B_Spline_Between_2_Pointsf->fCoefLenArr[1] = f_2ndTermf;

	sCoef_Of_B_Spline_Between_2_Pointsf->fCoefLenArr[2] = f_3rdTermf;

	sCoef_Of_B_Spline_Between_2_Pointsf->fCoefLenArr[3] = f_4thTermf;
	/////////////////////////////////////////////////////////////////////////////////
	sCoef_Of_B_Spline_Between_2_Pointsf->fCoefWidArr[0] = f_1stTermf;

	sCoef_Of_B_Spline_Between_2_Pointsf->fCoefWidArr[1] = f_2ndTermf;

	sCoef_Of_B_Spline_Between_2_Pointsf->fCoefWidArr[2] = f_3rdTermf;

	sCoef_Of_B_Spline_Between_2_Pointsf->fCoefWidArr[3] = f_4thTermf;

	//return SUCCESSFUL_RETURN;
} // void Coeffficients_Of_B_Spline(

  ///////////////////////////////////////////////////
void X_Y_Coordins_Approximated_By_B_Spline(

	const float fTf, //0 <= fTf <= 1
	const FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_Between_2_Pointsf,

	COEFS_OF_B_SPLINE_BETWEEN_2_POINTS *sCoef_Of_B_Spline_Between_2_Pointsf,

	float &fX_Approximf,
	float &fY_Approximf)
{
	void Coeffficients_Of_B_Spline(
		const float fTf,
		COEFS_OF_B_SPLINE_BETWEEN_2_POINTS *sCoef_Of_B_Spline_Between_2_Pointsf);

	int
		iPointf;
		//nResf;

	Coeffficients_Of_B_Spline(
		fTf, //const float fTf,
		sCoef_Of_B_Spline_Between_2_Pointsf); // COEFS_OF_B_SPLINE_BETWEEN_2_POINTS *sCoef_Of_B_Spline_Between_2_Pointsf);

	fX_Approximf = 0.0;
	fY_Approximf = 0.0;

	for (iPointf = 0; iPointf < 4; iPointf++)
	{
		fX_Approximf += (float)(s4_X_Y_Coordins_For_Spline_Between_2_Pointsf->nX_Arr[iPointf]) * sCoef_Of_B_Spline_Between_2_Pointsf->fCoefLenArr[iPointf];

		fY_Approximf += (float)(s4_X_Y_Coordins_For_Spline_Between_2_Pointsf->nY_Arr[iPointf]) * sCoef_Of_B_Spline_Between_2_Pointsf->fCoefWidArr[iPointf];

	} // for (iPointf = 0; iPointf < 4; iPointf++)

	fX_Approximf = (float)(fX_Approximf / 6.0);
	fY_Approximf = (float)(fY_Approximf / 6.0);

//	return SUCCESSFUL_RETURN;
} //void X_Y_Coordins_Approximated_By_B_Spline(...

  //////////////////////////////////////////////////////////////////////
int Shading_APixel_By_B_Spline(

	const int nItIs_1st_Intervalf,
	const float fTf, // 0 <= fTf <= 1
	const  FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_Between_2_Pointsf,

	COEFS_OF_B_SPLINE_BETWEEN_2_POINTS *sCoef_Of_B_Spline_Between_2_Pointsf,

	MASK_IMAGE *sMask_Imagef)
{
	void X_Y_Coordins_Approximated_By_B_Spline(

		const float fTf,
		const  FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_Between_2_Pointsf,

		COEFS_OF_B_SPLINE_BETWEEN_2_POINTS *sCoef_Of_B_Spline_Between_2_Pointsf,

		float &fX_Approximf,
		float &fY_Approximf);

	int
		nLenOfImagef = sMask_Imagef->nLength, // X coor
		nWidOfImagef = sMask_Imagef->nWidth, // Y coor

		nIndexOfPixelCurf, // = nLenf + (nWidf*nLenOfImagef); , 

		nLenf,
		nWidf,

		iLenf;
	//	nResf;

	float
		fX_Approximf,
		fY_Approximf;

	X_Y_Coordins_Approximated_By_B_Spline(

		fTf, //const float fTf,
		s4_X_Y_Coordins_For_Spline_Between_2_Pointsf, //const  FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_Between_2_Pointsf,

		sCoef_Of_B_Spline_Between_2_Pointsf, //COEFS_OF_B_SPLINE_BETWEEN_2_POINTS *sCoef_Of_B_Spline_Between_2_Pointsf,

		fX_Approximf, //float &fX_Approximf,
		fY_Approximf); // float &fY_Approximf);

	nLenf = (int)(fX_Approximf);

	if (nItIs_1st_Intervalf == 0)
	{
		nWidf = (int)(fY_Approximf);
	} //if (nItIs_1st_Intervalf == 0)
	else if (nItIs_1st_Intervalf == 1)
	{
		nWidf = (int)(fY_Approximf)-nWidScaleForPadding;

#ifndef COMMENT_OUT_ALL_PRINTS
		if (nWidf < 0)
		{
			printf("\n\n A warning in 'Shading_APixel_By_B_Spline' for 1st width interval:  nWidf = %d < 0, fTf = %E", nWidf, fTf);
			fprintf(fout_lr, "\n\n A warning in 'Shading_APixel_By_B_Spline' for 1st width interval:  nWidf = %d < 0, fTf = %E", nWidf, fTf);
		} //if (nWidf < 0)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	} // else if (nItIs_1st_Intervalf == 1)

#ifndef COMMENT_OUT_ALL_PRINTS
	if (nPrintInsideShadingBoundaryOfObject_By_Padding_Points_And_B_Spline_Glob == 1)
	{
		if (nWidf < sMask_Imagef->nWidth)
		{
			fprintf(fout_lr, "\n 'Shading_APixel_By_B_Spline':  no adjusting for nWidf = %d, nLenf = %d, fTf = %E, sMask_Imagef->nLenObjectBoundary_Arr[nWidf] = %d",
				nWidf, nLenf, fTf, sMask_Imagef->nLenObjectBoundary_Arr[nWidf]);

		} //if (nWidf < sMask_Imagef->nWidth)

	}//if (nPrintInsideShadingBoundaryOfObject_By_Padding_Points_And_B_Spline_Glob == 1)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nLenf < 0)
		nLenf = 0;

	if (nLenf > nLenOfImagef - 1)
		nLenf = nLenOfImagef - 1;

	if (nWidf < 0)
		nWidf = 0;

	if (nWidf > nWidOfImagef - 1)
	{
		nWidf = nWidOfImagef - 1;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n Exit in 'Shading_APixel_By_B_Spline':  nWidf = %d > nWidOfImagef - 1, nLenf = %d, fTf = %E, sMask_Imagef->nLenObjectBoundary_Arr[nWidf] = %d", nWidf, nLenf, fTf, sMask_Imagef->nLenObjectBoundary_Arr[nWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return 2;
	} //if (nWidf > nWidOfImagef - 1)

	if (sMask_Imagef->nSideOfObjectLocation == -1) //left
	{
		if (nLenf < sMask_Imagef->nLenObjectBoundary_Arr[nWidf])
		{
			//shading white pixels to black outside the boundary
			for (iLenf = nLenf + 1; iLenf <= sMask_Imagef->nLenObjectBoundary_Arr[nWidf]; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (nWidf*nLenOfImagef);

				sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCurf] = 0;

#ifndef COMMENT_OUT_ALL_PRINTS
				if (nPrintInsideShadingBoundaryOfObject_By_Padding_Points_And_B_Spline_Glob == 1)
				{
					if (nWidf < sMask_Imagef->nWidth)
					{
						fprintf(fout_lr, "\n 'Shading_APixel_By_B_Spline':  shading white pixel to black nWidf = %d, nLenf = %d, sMask_Imagef->nLenObjectBoundary_Arr[nWidf] = %d",
							nWidf, nLenf, sMask_Imagef->nLenObjectBoundary_Arr[nWidf]);

					} //if (nWidf < sMask_Imagef->nWidth)

				}//if (nPrintInsideShadingBoundaryOfObject_By_Padding_Points_And_B_Spline_Glob == 1)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			}// for (iLenf = nLenf + 1; iLenf <= sMask_Imagef->nLenObjectBoundary_Arr[nWidf]; iLenf++)

		} //if (nLenf < sMask_Imagef->nLenObjectBoundary_Arr[nWidf])

		sMask_Imagef->nLenObjectBoundary_Arr[nWidf] = nLenf;

	} //if (sMask_Imagef->nSideOfObjectLocation == -1) //left

	if (sMask_Imagef->nSideOfObjectLocation == 1) //right
	{
		if (nLenf > sMask_Imagef->nLenObjectBoundary_Arr[nWidf])
		{
			//shading white pixels to black outside the boundary

			for (iLenf = sMask_Imagef->nLenObjectBoundary_Arr[nWidf] + 1; iLenf <= nLenf; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (nWidf*nLenOfImagef);

				sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCurf] = 0;
			}// for (iLenf = sMask_Imagef->nLenObjectBoundary_Arr[nWidf] + 1; iLenf <= nLenf; iLenf++)

		}// if (nLenf > sMask_Imagef->nLenObjectBoundary_Arr[nWidf])

		sMask_Imagef->nLenObjectBoundary_Arr[nWidf] = nLenf;

	} //if (sMask_Imagef->nSideOfObjectLocation == 1) //right

	nIndexOfPixelCurf = nLenf + (nWidf*nLenOfImagef);

	sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCurf] = 1;

	return SUCCESSFUL_RETURN;
} // int Shading_APixel_By_B_Spline(...
  ////////////////////////////////////////////////////////////////////////////////////////////////

  //Shading contour between points [1] and [2] ([0] and [4] serve for the support)
void Shading_Contour_Between_2_Points_By_B_Spline(

	const int nItIs_1st_Intervalf,
	const  FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_Between_2_Pointsf,

	COEFS_OF_B_SPLINE_BETWEEN_2_POINTS *sCoef_Of_B_Spline_Between_2_Pointsf,

	MASK_IMAGE *sMask_Imagef)
{
	int Shading_APixel_By_B_Spline(

		const int nItIs_1st_Intervalf,
		const float fTf,
		const  FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_Between_2_Pointsf,

		COEFS_OF_B_SPLINE_BETWEEN_2_POINTS *sCoef_Of_B_Spline_Between_2_Pointsf,

		MASK_IMAGE *sMask_Imagef);

	int
		iTf,
		nResf;

	float
		fTf,
		fStepIn_B_Splinesf = (float)(1.0 / (float)(nNumOf_T_PointsMax));

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n A new 'Shading_Contour_Between_2_Points_By_B_Spline'\n");

	fprintf(fout_lr, "\n\n ...Between_2_Pointsf->nY_Arr[0] = %d, Between_2_Pointsf->nX_Arr[0] = %d, Between_2_Pointsf->nY_Arr[1] = %d, Between_2_Pointsf->nX_Arr[1] = %d",
		s4_X_Y_Coordins_For_Spline_Between_2_Pointsf->nY_Arr[0], s4_X_Y_Coordins_For_Spline_Between_2_Pointsf->nX_Arr[0],
		s4_X_Y_Coordins_For_Spline_Between_2_Pointsf->nY_Arr[1], s4_X_Y_Coordins_For_Spline_Between_2_Pointsf->nX_Arr[1]);

	fprintf(fout_lr, "\n\n ...Between_2_Pointsf->nY_Arr[2] = %d, Between_2_Pointsf->nX_Arr[2] = %d, Between_2_Pointsf->nY_Arr[3] = %d, Between_2_Pointsf->nX_Arr[3] = %d\n",
		s4_X_Y_Coordins_For_Spline_Between_2_Pointsf->nY_Arr[2], s4_X_Y_Coordins_For_Spline_Between_2_Pointsf->nX_Arr[2],
		s4_X_Y_Coordins_For_Spline_Between_2_Pointsf->nY_Arr[3], s4_X_Y_Coordins_For_Spline_Between_2_Pointsf->nX_Arr[3]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iTf = 0; iTf <= nNumOf_T_PointsMax; iTf++)
	{
		fTf = iTf*fStepIn_B_Splinesf;

		nResf = Shading_APixel_By_B_Spline(

			nItIs_1st_Intervalf, //const int nItIs_1st_Intervalf,
			fTf, //const float fTf,
			s4_X_Y_Coordins_For_Spline_Between_2_Pointsf, //const  FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_Between_2_Pointsf,

			sCoef_Of_B_Spline_Between_2_Pointsf, //COEFS_OF_B_SPLINE_BETWEEN_2_POINTS *sCoef_Of_B_Spline_Between_2_Pointsf,

			sMask_Imagef); // MASK_IMAGE *sMask_Imagef)

		if (nResf == 2) //exceeding the width of image
			break;
	}//for (iTf = 0; iTf <= nNumOf_T_PointsMax; iTf++)

	//return SUCCESSFUL_RETURN;
} // void Shading_Contour_Between_2_Points_By_B_Spline(...

  //////////////////////////////////////////////////////////////////

int ShadingBoundaryOfObject_By_Padding_Points_And_B_Spline(
	MASK_IMAGE *sMask_Imagef)
{
	void Coordinates_For_Padding_Interval(
		const int nPadf,
		const MASK_IMAGE *sMask_Imagef,

		FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_Between_2_Pointsf);

	void Coordinates_For_1st_Padding_Interval(
		const MASK_IMAGE *sMask_Imagef,

		FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_For_1st_2_Pointsf);

	int Coordinates_For_Interval_Between_2LastPaddingPoints(
		const MASK_IMAGE *sMask_Imagef,

		FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf);

	void Coordinates_For_Interval_Between_LastPaddingPointAndBoundary(
		const MASK_IMAGE *sMask_Imagef,

		FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_For_LastPaddingPointsAndBoundaryf);

	void Shading_Contour_Between_2_Points_By_B_Spline(

		const int nItIs_1st_Intervalf,
		const  FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_Between_2_Pointsf,

		COEFS_OF_B_SPLINE_BETWEEN_2_POINTS *sCoef_Of_B_Spline_Between_2_Pointsf,

		MASK_IMAGE *sMask_Imagef);

#ifndef USE_SPLINES_FOR_BOUNDARY_INTERVALS
	int AdjustObjectBoundariesBy_AStraightLine(
		const TWO_X_Y_COORDINS_FOR_LINE_BETWEEN_2_POINTS *s2_X_Y_Coordins_For_Line,

		MASK_IMAGE *sMask_Imagef);
#endif //#ifndef USE_SPLINES_FOR_BOUNDARY_INTERVALS

	////////////////////////////////////////////////////////////////

	int
		iPaddf,

		iWidf,
		nLenOfObjectBoundaryCurf,
		nItIs_1st_Intervalf, // 1 - Yes, 0 - No
		nResf;

	FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS s4_X_Y_Coordins_For_Spline_Between_2_Pointsf;

#ifdef USE_SPLINES_FOR_BOUNDARY_INTERVALS
	FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS s4_X_Y_Coordins_For_Spline_For_1st_2_Pointsf;
	FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf;
	FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS s4_X_Y_Coordins_For_Spline_For_LastPaddingPointsAndBoundaryf;
#endif //#ifdef USE_SPLINES_FOR_BOUNDARY_INTERVALS

	COEFS_OF_B_SPLINE_BETWEEN_2_POINTS sCoef_Of_B_Spline_Between_2_Pointsf; // calculated in 'X_Y_Coordins_Approximated_By_B_Spline'

#ifndef USE_SPLINES_FOR_BOUNDARY_INTERVALS
	TWO_X_Y_COORDINS_FOR_LINE_BETWEEN_2_POINTS s2_X_Y_Coordins_For_Line;
#endif //#ifndef USE_SPLINES_FOR_BOUNDARY_INTERVALS

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'ShadingBoundaryOfObject_By_Padding_Points_And_B_Spline': sMask_Imagef->nIs_WidthDivisibleBynWidScaleForPadding = %d\n", sMask_Imagef->nIs_WidthDivisibleBynScaleForPadding);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iWidf = nWidSubimageToPrintMin; iWidf < nWidSubimageToPrintMax; iWidf++)
	{
		nLenOfObjectBoundaryCurf = sMask_Imagef->nLenObjectBoundary_Arr[iWidf];

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n 'ShadingBoundaryOfObject_By_Padding_Points_And_B_Spline' (before): iWidf = %d, nLenOfObjectBoundaryCurf = %d", iWidf, nLenOfObjectBoundaryCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	}//if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax

	nItIs_1st_Intervalf = 0; // not 1st interval
	for (iPaddf = 2; iPaddf < sMask_Imagef->nNumOfPaddedLengths - 1; iPaddf++)
	{
		Coordinates_For_Padding_Interval(
			iPaddf, //const int nPadf,
			sMask_Imagef, //const MASK_IMAGE *sMask_Imagef,

			&s4_X_Y_Coordins_For_Spline_Between_2_Pointsf); // FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_Between_2_Pointsf);

		Shading_Contour_Between_2_Points_By_B_Spline(

			nItIs_1st_Intervalf, //const nItIs_1st_Intervalf,
			&s4_X_Y_Coordins_For_Spline_Between_2_Pointsf, //const  FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_Between_2_Pointsf,

			&sCoef_Of_B_Spline_Between_2_Pointsf, //COEFS_OF_B_SPLINE_BETWEEN_2_POINTS *sCoef_Of_B_Spline_Between_2_Pointsf,

			sMask_Imagef); // MASK_IMAGE *sMask_Imagef);

	} //for (iPaddf = 2; iPaddf < sMask_Imagef->nNumOfPaddedLengths - 1; iPaddf++)

#ifdef USE_SPLINES_FOR_BOUNDARY_INTERVALS

	  //connecting top of the image (iPaddf == 0) with iPaddf == 1 by a spline - 1st interval
	Coordinates_For_1st_Padding_Interval(
		sMask_Imagef, //const MASK_IMAGE *sMask_Imagef,

		&s4_X_Y_Coordins_For_Spline_For_1st_2_Pointsf); // FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_For_1st_2_Pointsf);

	nItIs_1st_Intervalf = 1; //  1st interval
	Shading_Contour_Between_2_Points_By_B_Spline(

		nItIs_1st_Intervalf, //const nItIs_1st_Intervalf,

		&s4_X_Y_Coordins_For_Spline_For_1st_2_Pointsf, //const  FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_Between_2_Pointsf,

		&sCoef_Of_B_Spline_Between_2_Pointsf, //COEFS_OF_B_SPLINE_BETWEEN_2_POINTS *sCoef_Of_B_Spline_Between_2_Pointsf,

		sMask_Imagef); // MASK_IMAGE *sMask_Imagef);
					   ///////////////////////////////////////////////////////////////////////

					   //the last 2 padding points
	nItIs_1st_Intervalf = 0; // not 1st interval

	nResf = Coordinates_For_Interval_Between_2LastPaddingPoints(
		sMask_Imagef, //const MASK_IMAGE *sMask_Imagef,

		&s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf); // FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n Zero in 'ShadingBoundaryOfObject_By_Padding_Points_And_B_Spline'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

	nPrintInsideShadingBoundaryOfObject_By_Padding_Points_And_B_Spline_Glob = 1;

	Shading_Contour_Between_2_Points_By_B_Spline(

		nItIs_1st_Intervalf, //const nItIs_1st_Intervalf,

		&s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf, //const  FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_Between_2_Pointsf,

		&sCoef_Of_B_Spline_Between_2_Pointsf, //COEFS_OF_B_SPLINE_BETWEEN_2_POINTS *sCoef_Of_B_Spline_Between_2_Pointsf,

		sMask_Imagef); // MASK_IMAGE *sMask_Imagef);

					   /////////////////////////////////////////////////////////////////////
					   //the last padding point and the boundary
	if (sMask_Imagef->nIs_WidthDivisibleBynScaleForPadding != 1) // there is a gap between the last padding point and the bottom edge 
	{
		Coordinates_For_Interval_Between_LastPaddingPointAndBoundary(
			sMask_Imagef, //const MASK_IMAGE *sMask_Imagef,

			&s4_X_Y_Coordins_For_Spline_For_LastPaddingPointsAndBoundaryf); // FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_For_LastPaddingPointsAndBoundaryf);

		Shading_Contour_Between_2_Points_By_B_Spline(

			nItIs_1st_Intervalf, //const nItIs_1st_Intervalf,

			&s4_X_Y_Coordins_For_Spline_For_LastPaddingPointsAndBoundaryf, //const  FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_Between_2_Pointsf,

			&sCoef_Of_B_Spline_Between_2_Pointsf, //COEFS_OF_B_SPLINE_BETWEEN_2_POINTS *sCoef_Of_B_Spline_Between_2_Pointsf,

			sMask_Imagef); // MASK_IMAGE *sMask_Imagef);

		nPrintInsideShadingBoundaryOfObject_By_Padding_Points_And_B_Spline_Glob = 0;
	} //if (sMask_Imagef->nIs_WidthDivisibleBynScaleForPadding != 1)

#endif //#ifdef USE_SPLINES_FOR_BOUNDARY_INTERVALS


	  //////////////////
#ifndef USE_SPLINES_FOR_BOUNDARY_INTERVALS
	  //connecting top of the image (iPaddf == 0) with iPaddf == 1
	  //fprintf(fout_lr, "\n\n 'ShadingBoundaryOfObject_By_Padding_Points_And_B_Spline':

	s2_X_Y_Coordins_For_Line.nX_Arr[0] = sMask_Imagef->nScaledPaddedLen_Arr[0];
	s2_X_Y_Coordins_For_Line.nY_Arr[0] = sMask_Imagef->nScaledPaddedWid_Arr[0];

	s2_X_Y_Coordins_For_Line.nX_Arr[1] = sMask_Imagef->nScaledPaddedLen_Arr[1];
	s2_X_Y_Coordins_For_Line.nY_Arr[1] = sMask_Imagef->nScaledPaddedWid_Arr[1];

	nResf = AdjustObjectBoundariesBy_AStraightLine(

		&s2_X_Y_Coordins_For_Line, //const TWO_X_Y_COORDINS_FOR_LINE_BETWEEN_2_POINTS *s2_X_Y_Coordins_For_Line,
		sMask_Imagef); // MASK_IMAGE *sMask_Imagef);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n Zero in 'ShadingBoundaryOfObject_By_Padding_Points_And_B_Spline' 1");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

	  //////////////////
	  //connecting the last two padding points: iPaddf == sMask_Imagef->nNumOfPaddedLengths - 2 with iPaddf == sMask_Imagef->nNumOfPaddedLengths - 1
	nPadCur1f = sMask_Imagef->nNumOfPaddedLengths - 2;
	nPadCur2f = sMask_Imagef->nNumOfPaddedLengths - 1;

	s2_X_Y_Coordins_For_Line.nX_Arr[0] = sMask_Imagef->nScaledPaddedLen_Arr[nPadCur1f];
	s2_X_Y_Coordins_For_Line.nY_Arr[0] = sMask_Imagef->nScaledPaddedWid_Arr[nPadCur1f];

	s2_X_Y_Coordins_For_Line.nX_Arr[1] = sMask_Imagef->nScaledPaddedLen_Arr[nPadCur2f];
	s2_X_Y_Coordins_For_Line.nY_Arr[1] = sMask_Imagef->nScaledPaddedWid_Arr[nPadCur2f];

	nResf = AdjustObjectBoundariesBy_AStraightLine(

		&s2_X_Y_Coordins_For_Line, //const TWO_X_Y_COORDINS_FOR_LINE_BETWEEN_2_POINTS *s2_X_Y_Coordins_For_Line,
		sMask_Imagef); // MASK_IMAGE *sMask_Imagef);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n Zero in 'ShadingBoundaryOfObject_By_Padding_Points_And_B_Spline' 2");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)
	  /////////////////////////////////
	  //there is a gap between the last padding point and the bottom of the image

	if (sMask_Imagef->nIs_WidthDivisibleBynWidScaleForPadding != 1)
	{
		nWidf = sMask_Imagef->nWidth - 1;

		nPadCur1f = sMask_Imagef->nNumOfPaddedLengths - 1;

		s2_X_Y_Coordins_For_Line.nX_Arr[0] = sMask_Imagef->nScaledPaddedLen_Arr[nPadCur1f];
		s2_X_Y_Coordins_For_Line.nY_Arr[0] = sMask_Imagef->nScaledPaddedWid_Arr[nPadCur1f];

		//adding the padding to the bottom
		if (sMask_Imagef->nSideOfObjectLocation == -1) //left
		{
			s2_X_Y_Coordins_For_Line.nX_Arr[1] = sMask_Imagef->nLenObjectBoundary_Arr[nWidf] + nLenForPadding;

			if (s2_X_Y_Coordins_For_Line.nX_Arr[1] >= sMask_Imagef->nLength - 1)
				s2_X_Y_Coordins_For_Line.nX_Arr[1] = sMask_Imagef->nLength - 1;

		} //if (sMask_Imagef->nSideOfObjectLocation == -1) //left
		else // right
		{
			s2_X_Y_Coordins_For_Line.nX_Arr[1] = sMask_Imagef->nLenObjectBoundary_Arr[nWidf] - nLenForPadding;

			if (s2_X_Y_Coordins_For_Line.nX_Arr[1] < 0)
				s2_X_Y_Coordins_For_Line.nX_Arr[1] = 0;

		}//else // right

		s2_X_Y_Coordins_For_Line.nY_Arr[1] = nWidf;

		nResf = AdjustObjectBoundariesBy_AStraightLine(

			&s2_X_Y_Coordins_For_Line, //const TWO_X_Y_COORDINS_FOR_LINE_BETWEEN_2_POINTS *s2_X_Y_Coordins_For_Line,
			sMask_Imagef); // MASK_IMAGE *sMask_Imagef);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n Zero in 'ShadingBoundaryOfObject_By_Padding_Points_And_B_Spline' 3");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return UNSUCCESSFUL_RETURN;
	} //if (nResf == UNSUCCESSFUL_RETURN)

	}//if (sMask_Imagef->nIs_WidthDivisibleBynWidScaleForPadding != 1)
#endif //#ifndef USE_SPLINES_FOR_BOUNDARY_INTERVALS	

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n ShadingBoundaryOfObject_By_Padding_Points_And_B_Spline (after):\n");
	for (iWidf = nWidSubimageToPrintMin; iWidf < nWidSubimageToPrintMax; iWidf++)
	{
		nLenOfObjectBoundaryCurf = sMask_Imagef->nLenObjectBoundary_Arr[iWidf];
		fprintf(fout_lr, "\n After 'ShadingBoundaryOfObject_By_Padding_Points_And_B_Spline': iWidf = %d, nLenOfObjectBoundaryCurf = %d", iWidf, nLenOfObjectBoundaryCurf);

	}//if (iWidf >= nWidSubimageToPrintMin && iWidf <= nWidSubimageToPrintMax
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	return SUCCESSFUL_RETURN;
} // int ShadingBoundaryOfObject_By_Padding_Points_And_B_Spline(...

void Coordinates_For_Padding_Interval(
	const int nPadf,
	const MASK_IMAGE *sMask_Imagef,

	FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_Between_2_Pointsf)

{
	int
		iPointf;

	for (iPointf = 0; iPointf < 4; iPointf++)
	{
		s4_X_Y_Coordins_For_Spline_Between_2_Pointsf->nY_Arr[iPointf] = sMask_Imagef->nScaledPaddedWid_Arr[nPadf - 2 + iPointf];
		s4_X_Y_Coordins_For_Spline_Between_2_Pointsf->nX_Arr[iPointf] = sMask_Imagef->nScaledPaddedLen_Arr[nPadf - 2 + iPointf];

	} // for (iPointf = 0; iPointf < 4; iPointf++)

	//return SUCCESSFUL_RETURN;
} //void Coordinates_For_Padding_Interval(...

  //////////////////////////////////////////////////
void Coordinates_For_1st_Padding_Interval(
	const MASK_IMAGE *sMask_Imagef,

	FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_For_1st_2_Pointsf)

{
	s4_X_Y_Coordins_For_Spline_For_1st_2_Pointsf->nY_Arr[0] = sMask_Imagef->nScaledPaddedWid_Arr[0]; // 0
	s4_X_Y_Coordins_For_Spline_For_1st_2_Pointsf->nX_Arr[0] = sMask_Imagef->nScaledPaddedLen_Arr[0]; // the same length

	s4_X_Y_Coordins_For_Spline_For_1st_2_Pointsf->nY_Arr[1] = sMask_Imagef->nScaledPaddedWid_Arr[0] + nWidScaleForPadding;
	s4_X_Y_Coordins_For_Spline_For_1st_2_Pointsf->nX_Arr[1] = sMask_Imagef->nScaledPaddedLen_Arr[0]; // the same length

	s4_X_Y_Coordins_For_Spline_For_1st_2_Pointsf->nY_Arr[2] = sMask_Imagef->nScaledPaddedWid_Arr[1] + nWidScaleForPadding;
	s4_X_Y_Coordins_For_Spline_For_1st_2_Pointsf->nX_Arr[2] = sMask_Imagef->nScaledPaddedLen_Arr[1];

	s4_X_Y_Coordins_For_Spline_For_1st_2_Pointsf->nY_Arr[3] = sMask_Imagef->nScaledPaddedWid_Arr[2] + nWidScaleForPadding;
	s4_X_Y_Coordins_For_Spline_For_1st_2_Pointsf->nX_Arr[3] = sMask_Imagef->nScaledPaddedLen_Arr[2];

	//return SUCCESSFUL_RETURN;
} //void Coordinates_For_1st_Padding_Interval(...
  ///////////////////////////////////////////////////////

int Coordinates_For_Interval_Between_2LastPaddingPoints(
	const MASK_IMAGE *sMask_Imagef,

	FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf)

{
	int
		nWidOfImagef = sMask_Imagef->nWidth;

	if (sMask_Imagef->nNumOfPaddedLengths < 3)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Coordinates_For_Interval_Between_2LastPaddingPoints': sMask_Imagef->nNumOfPaddedLengths = %d < 3",
			sMask_Imagef->nNumOfPaddedLengths);
		printf("\n Either image width is too small or 'nWidScaleForPadding' is too large");

		fprintf(fout_lr, "\n\n An error in 'Coordinates_For_Interval_Between_2LastPaddingPoints': sMask_Imagef->nNumOfPaddedLengths = %d < 3",
			sMask_Imagef->nNumOfPaddedLengths);
		fprintf(fout_lr, "\n Either image width is too small or 'nWidScaleForPadding' is too large");

		//printf("\n\n Please press any key to exit");	getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}// if (sMask_Imagef->nNumOfPaddedLengths < 3)

	if (sMask_Imagef->nIs_WidthDivisibleBynScaleForPadding == 1) //there is no gap in width between the last padding point and the bottom edge 
	{

		s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nY_Arr[0] = sMask_Imagef->nScaledPaddedWid_Arr[sMask_Imagef->nNumOfPaddedLengths - 3]; // 0
		s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nX_Arr[0] = sMask_Imagef->nScaledPaddedLen_Arr[sMask_Imagef->nNumOfPaddedLengths - 3]; // the same length

		s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nY_Arr[1] = sMask_Imagef->nScaledPaddedWid_Arr[sMask_Imagef->nNumOfPaddedLengths - 2];
		s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nX_Arr[1] = sMask_Imagef->nScaledPaddedLen_Arr[sMask_Imagef->nNumOfPaddedLengths - 2]; // the same length

		s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nY_Arr[2] = sMask_Imagef->nScaledPaddedWid_Arr[sMask_Imagef->nNumOfPaddedLengths - 1]; // the last point is on the bottom boundary
		s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nX_Arr[2] = sMask_Imagef->nScaledPaddedLen_Arr[sMask_Imagef->nNumOfPaddedLengths - 1];

		//the next point is beyond the image boundary -- an artificial point is created
		s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nY_Arr[3] = sMask_Imagef->nScaledPaddedWid_Arr[sMask_Imagef->nNumOfPaddedLengths - 1] + nWidScaleForPadding;

		s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nX_Arr[3] = sMask_Imagef->nScaledPaddedLen_Arr[sMask_Imagef->nNumOfPaddedLengths - 1]; // the same length

	} //if (sMask_Imagef->nIs_WidthDivisibleBynWidScaleForPadding == 1) //there is no gap in width between the last padding point and the bottom edge 
	else if (sMask_Imagef->nIs_WidthDivisibleBynScaleForPadding != 1) //there is a gap in width between the last padding point and the bottom edge 
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'Coordinates_For_Interval_Between_2LastPaddingPoints'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nY_Arr[0] = sMask_Imagef->nScaledPaddedWid_Arr[sMask_Imagef->nNumOfPaddedLengths - 3]; // 
		s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nX_Arr[0] = sMask_Imagef->nScaledPaddedLen_Arr[sMask_Imagef->nNumOfPaddedLengths - 3];

		s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nY_Arr[1] = sMask_Imagef->nScaledPaddedWid_Arr[sMask_Imagef->nNumOfPaddedLengths - 2];
		s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nX_Arr[1] = sMask_Imagef->nScaledPaddedLen_Arr[sMask_Imagef->nNumOfPaddedLengths - 2];

		s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nY_Arr[2] = sMask_Imagef->nScaledPaddedWid_Arr[sMask_Imagef->nNumOfPaddedLengths - 1];
		s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nX_Arr[2] = sMask_Imagef->nScaledPaddedLen_Arr[sMask_Imagef->nNumOfPaddedLengths - 1];

		//the next point is on the image boundary
		s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nY_Arr[3] = sMask_Imagef->nScaledPaddedWid_Arr[sMask_Imagef->nNumOfPaddedLengths - 1] + nWidScaleForPadding;
		s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nX_Arr[3] = sMask_Imagef->nLenObjectBoundary_Arr[nWidOfImagef - 1] + nLenForPadding;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n ...Last_2_Padding_Pointsf->nY_Arr[0] = %d, Last_2_Padding_Pointsf->nX_Arr[0] = %d, Last_2_Padding_Pointsf->nY_Arr[1] = %d, Last_2_Padding_Pointsf->nX_Arr[1] = %d",
			s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nY_Arr[0], s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nX_Arr[0],
			s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nY_Arr[1], s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nX_Arr[1]);

		fprintf(fout_lr, "\n\n ...Last_2_Padding_Pointsf->nY_Arr[2] = %d, Last_2_Padding_Pointsf->nX_Arr[2] = %d, Last_2_Padding_Pointsf->nY_Arr[3] = %d, Last_2_Padding_Pointsf->nX_Arr[3] = %d",
			s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nY_Arr[2], s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nX_Arr[2],
			s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nY_Arr[3], s4_X_Y_Coordins_For_Spline_For_Last_2_Padding_Pointsf->nX_Arr[3]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	} // else if (sMask_Imagef->nIs_WidthDivisibleBynScaleForPadding != 1) //there is a gap in width between the last padding point and the bottom edge 

	return SUCCESSFUL_RETURN;
} //int Coordinates_For_Interval_Between_2LastPaddingPoints(...
  /////////////////////////////////////////////////////////////////////

void Coordinates_For_Interval_Between_LastPaddingPointAndBoundary(
	const MASK_IMAGE *sMask_Imagef,

	FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS *s4_X_Y_Coordins_For_Spline_For_LastPaddingPointsAndBoundaryf)

{
	int
		nWidOfImagef = sMask_Imagef->nWidth;

	s4_X_Y_Coordins_For_Spline_For_LastPaddingPointsAndBoundaryf->nY_Arr[0] = sMask_Imagef->nScaledPaddedWid_Arr[sMask_Imagef->nNumOfPaddedLengths - 2]; // 
	s4_X_Y_Coordins_For_Spline_For_LastPaddingPointsAndBoundaryf->nX_Arr[0] = sMask_Imagef->nScaledPaddedLen_Arr[sMask_Imagef->nNumOfPaddedLengths - 2];

	s4_X_Y_Coordins_For_Spline_For_LastPaddingPointsAndBoundaryf->nY_Arr[1] = sMask_Imagef->nScaledPaddedWid_Arr[sMask_Imagef->nNumOfPaddedLengths - 1];
	s4_X_Y_Coordins_For_Spline_For_LastPaddingPointsAndBoundaryf->nX_Arr[1] = sMask_Imagef->nScaledPaddedLen_Arr[sMask_Imagef->nNumOfPaddedLengths - 1];

	//the next point is on the image boundary
	s4_X_Y_Coordins_For_Spline_For_LastPaddingPointsAndBoundaryf->nY_Arr[2] = sMask_Imagef->nScaledPaddedWid_Arr[sMask_Imagef->nNumOfPaddedLengths - 1] + nWidScaleForPadding;
	s4_X_Y_Coordins_For_Spline_For_LastPaddingPointsAndBoundaryf->nX_Arr[2] = sMask_Imagef->nLenObjectBoundary_Arr[nWidOfImagef - 1] + nLenForPadding;

	//the last point is beyond the image boundary -- an artificial point is created

	s4_X_Y_Coordins_For_Spline_For_LastPaddingPointsAndBoundaryf->nY_Arr[3] = sMask_Imagef->nScaledPaddedWid_Arr[sMask_Imagef->nNumOfPaddedLengths - 1] + (2 * nWidScaleForPadding);
	s4_X_Y_Coordins_For_Spline_For_LastPaddingPointsAndBoundaryf->nX_Arr[3] = sMask_Imagef->nLenObjectBoundary_Arr[nWidOfImagef - 1] + nLenForPadding; //the same length

	//return SUCCESSFUL_RETURN;
} //void Coordinates_For_Interval_Between_LastPaddingPointAndBoundary(...

  ///////////////////////////////////////////////////////////////
int AdjustObjectBoundariesBy_AStraightLine(
	const TWO_X_Y_COORDINS_FOR_LINE_BETWEEN_2_POINTS *s2_X_Y_Coordins_For_Line,

	MASK_IMAGE *sMask_Imagef)
{
	int
		iWidf,
		nLenOfObjectBoundaryCurf;

	float
		fSlopef;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'AdjustObjectBoundariesBy_AStraightLine': ");
	fprintf(fout_lr, "\n s2_X_Y_Coordins_For_Line->nY_Arr[0] = %d, ->nX_Arr[0] = %d, ->nY_Arr[1] = %d, ->nX_Arr[1] = %d",
		s2_X_Y_Coordins_For_Line->nY_Arr[0], s2_X_Y_Coordins_For_Line->nX_Arr[0], s2_X_Y_Coordins_For_Line->nY_Arr[1], s2_X_Y_Coordins_For_Line->nX_Arr[1]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (s2_X_Y_Coordins_For_Line->nY_Arr[0] >= s2_X_Y_Coordins_For_Line->nY_Arr[1])
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'AdjustObjectBoundariesBy_AStraightLine': s2_X_Y_Coordins_For_Line->nY_Arr[0] = %d >= s2_X_Y_Coordins_For_Line->nY_Arr[1] = %d, sMask_Imagef->nWidth = %d",
			s2_X_Y_Coordins_For_Line->nY_Arr[0], s2_X_Y_Coordins_For_Line->nY_Arr[1], sMask_Imagef->nWidth);

		fprintf(fout_lr, "\n\n An error in 'AdjustObjectBoundariesBy_AStraightLine': s2_X_Y_Coordins_For_Line->nY_Arr[0] = %d >= s2_X_Y_Coordins_For_Line->nY_Arr[1] = %d, sMask_Imagef->nWidth = %d",
			s2_X_Y_Coordins_For_Line->nY_Arr[0], s2_X_Y_Coordins_For_Line->nY_Arr[1], sMask_Imagef->nWidth);

		//printf("\n\n Please press any key to exit"); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (s2_X_Y_Coordins_For_Line->nY_Arr[0] >= s2_X_Y_Coordins_For_Line->nY_Arr[1])

	  //the same column
	if (s2_X_Y_Coordins_For_Line->nX_Arr[0] == s2_X_Y_Coordins_For_Line->nX_Arr[1])
	{
		for (iWidf = 0; iWidf <= sMask_Imagef->nScaledPaddedWid_Arr[1]; iWidf++)
		{
			sMask_Imagef->nLenObjectBoundary_Arr[iWidf] = s2_X_Y_Coordins_For_Line->nX_Arr[0]; //sMask_Imagef->nScaledPaddedLen_Arr[0];

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 1: a new sMask_Imagef->nLenObjectBoundary_Arr[%d] = %d, in 'AdjustObjectBoundariesBy_AStraightLine'",
				iWidf, sMask_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		}//for (iWidf = 0; iWidf <= sMask_Imagef->nScaledPaddedWid_Arr[1]; iWidf++)

	}//if (s2_X_Y_Coordins_For_Line->nX_Arr[0] == s2_X_Y_Coordins_For_Line->nX_Arr[1])

	else //if (s2_X_Y_Coordins_For_Line->nX_Arr[0] != s2_X_Y_Coordins_For_Line->nX_Arr[1])
	{
		fSlopef = (float)(s2_X_Y_Coordins_For_Line->nY_Arr[0] - s2_X_Y_Coordins_For_Line->nY_Arr[1]) / (float)(s2_X_Y_Coordins_For_Line->nX_Arr[0] - s2_X_Y_Coordins_For_Line->nX_Arr[1]);

		for (iWidf = s2_X_Y_Coordins_For_Line->nY_Arr[0]; iWidf <= s2_X_Y_Coordins_For_Line->nY_Arr[1]; iWidf++)
		{
			nLenOfObjectBoundaryCurf = s2_X_Y_Coordins_For_Line->nX_Arr[0] + (int)((float)(iWidf - s2_X_Y_Coordins_For_Line->nY_Arr[0]) / fSlopef);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'AdjustObjectBoundariesBy_AStraightLine': iWidf = %d, prev sMask_Imagef->nLenObjectBoundary_Arr[%d] = %d, the new nLenOfObjectBoundaryCurf = %d, sMask_Imagef->nLength = %d",
				iWidf, iWidf, sMask_Imagef->nLenObjectBoundary_Arr[iWidf], nLenOfObjectBoundaryCurf, sMask_Imagef->nLength);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			sMask_Imagef->nLenObjectBoundary_Arr[iWidf] = nLenOfObjectBoundaryCurf;

		}//for (iWidf = s2_X_Y_Coordins_For_Line->nY_Arr[0]; iWidf <= s2_X_Y_Coordins_For_Line->nY_Arr[1]; iWidf++)

	}//else //if (s2_X_Y_Coordins_For_Line->nX_Arr[0] != s2_X_Y_Coordins_For_Line->nX_Arr[1])

	return SUCCESSFUL_RETURN;
} //int AdjustObjectBoundariesBy_AStraightLine(...

  ///////////////////////////////////////////////////

#ifndef USE_MEDIAN_FILTER
void FillingOut_ObjectPaddedAreaByWhitePixels(
	MASK_IMAGE *sMask_Imagef)
{
	int
		nIndexOfPixelCurf,
		nLenOfObjectBoundaryCurf,
		iWidf,
		iLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n FillingOut_ObjectPaddedAreaByWhitePixels:\n");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iWidf = 0; iWidf < sMask_Imagef->nWidth; iWidf++)
	{
		nLenOfObjectBoundaryCurf = sMask_Imagef->nLenObjectBoundary_Arr[iWidf];

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n The boundary: iWidf = %d, nLenOfObjectBoundaryCurf = %d", iWidf, nLenOfObjectBoundaryCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		if (sMask_Imagef->nSideOfObjectLocation == -1) //left
		{
			for (iLenf = 0; iLenf <= nLenOfObjectBoundaryCurf; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sMask_Imagef->nLength);

				sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCurf] = 1; // nIntensityStatMax; // nRed;

			} //for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)

		} //if (sMask_Imagef->nSideOfObjectLocation == -1)
		else //right
		{
			for (iLenf = sMask_Imagef->nLength - 1; iLenf >= nLenOfObjectBoundaryCurf; iLenf--)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sMask_Imagef->nLength);

				sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCurf] = 1; // nIntensityStatMax; // nRed;

			} //for (iLenf = sMask_Imagef->nLength - 1; iLenf >= nLenOfObjectBoundaryCurf; iLenf--)

		}//else //right

	} // for (iWidf = 0; iWidf <  sMask_Imagef->nWidth; iWidf++)

	//return SUCCESSFUL_RETURN;
} //void FillingOut_ObjectPaddedAreaByWhitePixels(

#endif //#ifndef USE_MEDIAN_FILTER

  ////////////////////////////////////////////////////////////
void MedianFiterTo_APixel_InAMaskImage(
	const int nWidOfPixelf,
	const int nLenOfPixelf,

	int &nPixelShadedToWhiteOrBlackf,
	MASK_IMAGE *sMask_Imagef)
{
	int
		nIndexOfPixelCurf,

		nIndexOfPixelCentref, // = nLenOfPixelf + (nWidOfPixelf*sMask_Imagef->nLength,

		nWidOfWindowInitf,
		nWidOfWindowFinf,

		nLenOfWindowLeftf,
		nLenOfWindowRightf,

		nSumOfOnesForOutputOneMin_Curf,

		nSumOfOnesCurf = 0,
		nClassOfPixelInitf, //0 or 1
							//nAreaf, // = (2 * nRadiusOfMedianFilter + 1)*(2 * nRadiusOfMedianFilter + 1), //init

		iWidf,
		iLenf;

	nPixelShadedToWhiteOrBlackf = 0; //no change initially
	nIndexOfPixelCentref = nLenOfPixelf + (nWidOfPixelf*sMask_Imagef->nLength);

	nClassOfPixelInitf = sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCentref];

	nWidOfWindowInitf = nWidOfPixelf - nRadiusOfMedianFilter;
	if (nWidOfWindowInitf < 0)
	{
		nWidOfWindowInitf = 0;
	} //if (nWidOfWindowInitf < 0)

	nWidOfWindowFinf = nWidOfPixelf + nRadiusOfMedianFilter;
	if (nWidOfWindowFinf > sMask_Imagef->nWidth - 1)
	{
		nWidOfWindowFinf = sMask_Imagef->nWidth - 1;
	} //if (nWidOfWindowFinf > sMask_Imagef->nWidth - 1)

	nLenOfWindowLeftf = nLenOfPixelf - nRadiusOfMedianFilter;
	if (nLenOfWindowLeftf < 0)
	{
		nLenOfWindowLeftf = 0;
	} //if (nLenOfWindowLeftf < 0)

	nLenOfWindowRightf = nLenOfPixelf + nRadiusOfMedianFilter;
	if (nLenOfWindowRightf > sMask_Imagef->nLength - 1)
	{
		nLenOfWindowRightf = sMask_Imagef->nLength - 1;
	}// if (nLenOfWindowRightf > sMask_Imagef->nLength - 1)

	nSumOfOnesForOutputOneMin_Curf = ((nWidOfWindowFinf - nWidOfWindowInitf + 1)*(nLenOfWindowRightf - nLenOfWindowLeftf + 1) / 2) + 1;

	for (iWidf = nWidOfWindowInitf; iWidf <= nWidOfWindowFinf; iWidf++)
	{
		for (iLenf = nLenOfWindowLeftf; iLenf <= nLenOfWindowRightf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*sMask_Imagef->nLength);

			if (sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCurf] == 1)
			{
				nSumOfOnesCurf += 1;
				if (nSumOfOnesCurf >= nSumOfOnesForOutputOneMin_Curf)
				{
					if (nClassOfPixelInitf == 0)
					{
						nPixelShadedToWhiteOrBlackf = 1; //changing from black to white
					} //if (nClassOfPixelInitf == 0)

					sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCentref] = 1;
					//return SUCCESSFUL_RETURN;

					goto Mark_EndOfMedianFiterTo_APixel_InAMaskImage;
				}//if (nSumOfOnesCurf >= nSumOfOnesForOutputOneMin_Curf)

			} //if (sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCurf] == 1)

		} //for (iLenf = nLenOfWindowLeftf; iLenf <= nLenOfWindowRightf; iLenf++)

	} // for (iWidf = nWidOfWindowInitf; iWidf <= nWidOfWindowFinf; iWidf++)

	if (nClassOfPixelInitf == 1)
	{
		nPixelShadedToWhiteOrBlackf = -1; //changing to from white to black
	} //if (nClassOfPixelInitf == 1)

	sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCentref] = 0;

Mark_EndOfMedianFiterTo_APixel_InAMaskImage: iWidf = 0;
	//return SUCCESSFUL_RETURN;

}//void MedianFiterTo_APixel_InAMaskImage(...

 /////////////////////////////////////////////////////////////////////
void MedianFiterTo_BoundaryArea(

	int &nNumOfChangedPixelsCloseToBoundaryTotf,
	MASK_IMAGE *sMask_Imagef)
{
	void MedianFiterTo_APixel_InAMaskImage(
		const int nWidOfPixelf,
		const int nLenOfPixelf,

		int &nPixelShadedToWhiteOrBlackf,
		MASK_IMAGE *sMask_Imagef);

	int
		 //nResf,
		nLenOfObjectBoundaryCurf,

		nLen_ToLeftOfBoundaryCurf,
		nLen_ToRightOfBoundaryCurf,

		nPixelShadedToWhiteOrBlackf,

		nNumOfPixelsTurnedToWhitef = 0,
		nNumOfPixelsTurnedToBlackf = 0,

		iWidf,
		iLenf;

	nNumOfChangedPixelsCloseToBoundaryTotf = 0;

	for (iWidf = 0; iWidf < sMask_Imagef->nWidth; iWidf++)
	{
		nLenOfObjectBoundaryCurf = sMask_Imagef->nLenObjectBoundary_Arr[iWidf];

		if (sMask_Imagef->nSideOfObjectLocation == -1) //left
		{
			nLen_ToLeftOfBoundaryCurf = nLenOfObjectBoundaryCurf - (nCoeff_DeviationFromBoundaryForMedianFilter_Into*nRadiusOfMedianFilter);
			if (nLen_ToLeftOfBoundaryCurf < 0)
			{
				nLen_ToLeftOfBoundaryCurf = 0;
			} //if (nLen_ToLeftOfBoundaryCurf < 0)

			nLen_ToRightOfBoundaryCurf = nLenOfObjectBoundaryCurf + (nCoeff_DeviationFromBoundaryForMedianFilter_Out*nRadiusOfMedianFilter);

			if (nLen_ToRightOfBoundaryCurf > sMask_Imagef->nLength - 1)
			{
				nLen_ToRightOfBoundaryCurf = sMask_Imagef->nLength - 1;
			}// if (nLen_ToRightOfBoundaryCurf > sMask_Imagef->nLength - 1)

			//if (iWidf == 273)
			//{
			//fprintf(fout_lr, "\n\n iWidf == 273, iIterGlob = %d, nLen_ToLeftOfBoundaryCurf = %d, nLen_ToRightOfBoundaryCurf = %d",
			//iIterGlob, nLen_ToLeftOfBoundaryCurf, nLen_ToRightOfBoundaryCurf);
			//}//if (iWidf == 273)

		} //if (sMask_Imagef->nSideOfObjectLocation == -1) //left
		else if (sMask_Imagef->nSideOfObjectLocation == 1) //right
		{
			nLen_ToLeftOfBoundaryCurf = nLenOfObjectBoundaryCurf - (nCoeff_DeviationFromBoundaryForMedianFilter_Out*nRadiusOfMedianFilter);
			if (nLen_ToLeftOfBoundaryCurf < 0)
			{
				nLen_ToLeftOfBoundaryCurf = 0;
			} //if (nLen_ToLeftOfBoundaryCurf < 0)

			nLen_ToRightOfBoundaryCurf = nLenOfObjectBoundaryCurf + (nCoeff_DeviationFromBoundaryForMedianFilter_Into*nRadiusOfMedianFilter);
			if (nLen_ToRightOfBoundaryCurf > sMask_Imagef->nLength - 1)
			{
				nLen_ToRightOfBoundaryCurf = sMask_Imagef->nLength - 1;
			}// if (nLen_ToRightOfBoundaryCurf > sMask_Imagef->nLength - 1)
		} //else if (sMask_Imagef->nSideOfObjectLocation == 1) //right

		for (iLenf = nLen_ToLeftOfBoundaryCurf; iLenf <= nLen_ToRightOfBoundaryCurf; iLenf++)
		{
			MedianFiterTo_APixel_InAMaskImage(
				iWidf, //const int nWidOfPixelf,
				iLenf, //const int nLenOfPixelf,

				nPixelShadedToWhiteOrBlackf, //int &nPixelShadedToWhiteOrBlackf,

				sMask_Imagef); // MASK_IMAGE *sMask_Imagef);

			if (nPixelShadedToWhiteOrBlackf == 1)
			{
				nNumOfPixelsTurnedToWhitef += 1;

				if (iLenf > sMask_Imagef->nLenObjectBoundary_Arr[iWidf] && sMask_Imagef->nSideOfObjectLocation == -1) //left
				{
					sMask_Imagef->nLenObjectBoundary_Arr[iWidf] = iLenf;
				} //if (iLenf > sMask_Imagef->nLenObjectBoundary_Arr[iWidf] && sMask_Imagef->nSideOfObjectLocation == -1) //left

				if (iLenf < sMask_Imagef->nLenObjectBoundary_Arr[iWidf] && sMask_Imagef->nSideOfObjectLocation == 1) //right
				{
					sMask_Imagef->nLenObjectBoundary_Arr[iWidf] = iLenf;
				} //if (iLenf < sMask_Imagef->nLenObjectBoundary_Arr[iWidf] && sMask_Imagef->nSideOfObjectLocation == 1) //right

			} //if (nPixelShadedToWhiteOrBlackf == 1)
			else if (nPixelShadedToWhiteOrBlackf == -1)
			{
				nNumOfPixelsTurnedToBlackf += 1;
			}//else if (nPixelShadedToWhiteOrBlackf == -1)
			 /*
			 if (iWidf == 273)
			 {
			 fprintf(fout_lr, "\n\n iLenf = %d, nPixelShadedToWhiteOrBlackf = %d, nNumOfPixelsTurnedToWhitef = %d, nNumOfPixelsTurnedToBlackf = %d",
			 iLenf, nPixelShadedToWhiteOrBlackf, nNumOfPixelsTurnedToWhitef, nNumOfPixelsTurnedToBlackf);
			 }//if (iWidf == 273)
			 */
		} //for (iLenf = nLen_ToLeftOfBoundaryCurf; iLenf <= nLen_ToRightOfBoundaryCurf; iLenf++)

	} // for (iWidf = 0; iWidf <  sMask_Imagef->nWidth; iWidf++)

	nNumOfChangedPixelsCloseToBoundaryTotf = nNumOfPixelsTurnedToWhitef + nNumOfPixelsTurnedToBlackf;

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n The end in 'MedianFiterTo_BoundaryArea': nNumOfPixelsTurnedToWhitef = %d, nNumOfPixelsTurnedToBlackf = %d", nNumOfPixelsTurnedToWhitef, nNumOfPixelsTurnedToBlackf);
	fprintf(fout_lr, "\n\n The end in 'MedianFiterTo_BoundaryArea': nNumOfPixelsTurnedToWhitef = %d, nNumOfPixelsTurnedToBlackf = %d", nNumOfPixelsTurnedToWhitef, nNumOfPixelsTurnedToBlackf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//return SUCCESSFUL_RETURN;
}//void MedianFiterTo_BoundaryArea(...

 ///////////////////////////////////////////////////////////////////////////////
int MedianFiterTo_BlackAreaBehindBoundary(

	int &nWidToStartForBlackAreaBehindBoundaryf,

	int &nWid_BlackPixelsBehindBoundaryMinf,
	int &nWid_BlackPixelsBehindBoundaryMaxf,

	int &nNumOfChangedPixelsBehindBoundaryTotf,

	MASK_IMAGE *sMask_Imagef)
{
	int RangeOfWidthsWithBlackPixelsBehindBoundary(

		int &nWidToStartForBlackAreaBehindBoundaryf,

		int &nWid_BlackPixelsBehindBoundaryMinf,
		int &nWid_BlackPixelsBehindBoundaryMaxf,

		MASK_IMAGE *sMask_Imagef);

	void MedianFiterTo_APixel_InAMaskImage(
		const int nWidOfPixelf,
		const int nLenOfPixelf,

		int &nPixelShadedToWhiteOrBlackf,
		MASK_IMAGE *sMask_Imagef);

	int
		nResf,
		nLenOfObjectBoundaryCurf,

		nLen_ToLeftOfBoundaryCurf,
		nLen_ToRightOfBoundaryCurf,

		nPixelShadedToWhiteOrBlackf,

		nNumOfPixelsTurnedToWhitef = 0,
		nNumOfPixelsTurnedToBlackf = 0,

		iWidf,
		iLenf;

	nNumOfChangedPixelsBehindBoundaryTotf = 0;
	nWid_BlackPixelsBehindBoundaryMinf = -1;
	nWid_BlackPixelsBehindBoundaryMaxf = -1;

	nResf = RangeOfWidthsWithBlackPixelsBehindBoundary(

		nWidToStartForBlackAreaBehindBoundaryf, //int &nWidToStartForBlackAreaBehindBoundaryf,
		nWid_BlackPixelsBehindBoundaryMinf, //int &nWid_BlackPixelsBehindBoundaryMinf,
		nWid_BlackPixelsBehindBoundaryMaxf, //int &nWid_BlackPixelsBehindBoundaryMaxf,

		sMask_Imagef); // MASK_IMAGE *sMask_Imagef);

	if (nResf == -2) //nWid_BlackPixelsBehindBoundaryMinf == -1)
	{

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'MedianFiterTo_BlackAreaBehindBoundary': no black pixels behind the boundary have been found");
		fprintf(fout_lr, "\n\n 'MedianFiterTo_BlackAreaBehindBoundary': no black pixels behind the boundary have been found");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return (-2);
	} // if (nResf == -2)
	else
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'MedianFiterTo_BlackAreaBehindBoundary': iIterGlob = %d, nWid_BlackPixelsBehindBoundaryMinf = %d, nWid_BlackPixelsBehindBoundaryMaxf = %d",
			iIterGlob, nWid_BlackPixelsBehindBoundaryMinf, nWid_BlackPixelsBehindBoundaryMaxf);
		fprintf(fout_lr, "\n\n 'MedianFiterTo_BlackAreaBehindBoundary': iIterGlob = %d, nWid_BlackPixelsBehindBoundaryMinf = %d, nWid_BlackPixelsBehindBoundaryMaxf = %d",
			iIterGlob, nWid_BlackPixelsBehindBoundaryMinf, nWid_BlackPixelsBehindBoundaryMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	}//else
	 ///////////////////////////////////////////////
	nNumOfChangedPixelsBehindBoundaryTotf = 0;

	for (iWidf = nWid_BlackPixelsBehindBoundaryMinf; iWidf <= nWid_BlackPixelsBehindBoundaryMaxf; iWidf++)
	{
		nLenOfObjectBoundaryCurf = sMask_Imagef->nLenObjectBoundary_Arr[iWidf];

		if (sMask_Imagef->nSideOfObjectLocation == -1) //left
		{
			nLen_ToLeftOfBoundaryCurf = 0;
			nLen_ToRightOfBoundaryCurf = nLenOfObjectBoundaryCurf - 1;
		}// if (sMask_Imagef->nSideOfObjectLocation == -1) //left
		else if (sMask_Imagef->nSideOfObjectLocation == 1) //right
		{
			nLen_ToLeftOfBoundaryCurf = nLenOfObjectBoundaryCurf + 1;
			nLen_ToRightOfBoundaryCurf = sMask_Imagef->nLength - 1;
		} //else if (sMask_Imagef->nSideOfObjectLocation == 1) //right

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'MedianFiterTo_BlackAreaBehindBoundary': iWidf = %d, nLenOfObjectBoundaryCurf = %d, iIterGlob = %d", iWidf, nLenOfObjectBoundaryCurf, iIterGlob);
		fprintf(fout_lr, "\n\n iIterGlob = %d, nWid_BlackPixelsBehindBoundaryMinf = %d, nWid_BlackPixelsBehindBoundaryMaxf = %d",
			iIterGlob, nWid_BlackPixelsBehindBoundaryMinf, nWid_BlackPixelsBehindBoundaryMaxf);
		fprintf(fout_lr, "\n nLen_ToLeftOfBoundaryCurf = %d, nLen_ToRightOfBoundaryCurf = %d", nLen_ToLeftOfBoundaryCurf, nLen_ToRightOfBoundaryCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		for (iLenf = nLen_ToLeftOfBoundaryCurf; iLenf <= nLen_ToRightOfBoundaryCurf; iLenf++)
		{
			MedianFiterTo_APixel_InAMaskImage(
				iWidf, //const int nWidOfPixelf,
				iLenf, //const int nLenOfPixelf,

				nPixelShadedToWhiteOrBlackf, //int &nPixelShadedToWhiteOrBlackf,

				sMask_Imagef); // MASK_IMAGE *sMask_Imagef);

			if (nPixelShadedToWhiteOrBlackf == 1)
			{
				nNumOfPixelsTurnedToWhitef += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n White: nNumOfPixelsTurnedToWhitef = %d, iWidf = %d, iLenf = %d, iIterGlob = %d", nNumOfPixelsTurnedToWhitef, iWidf, iLenf, iIterGlob);
				fprintf(fout_lr, "\n nLen_ToLeftOfBoundaryCurf = %d, nLen_ToRightOfBoundaryCurf = %d", nLen_ToLeftOfBoundaryCurf, nLen_ToRightOfBoundaryCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			} //if (nPixelShadedToWhiteOrBlackf == 1)
			else if (nPixelShadedToWhiteOrBlackf == -1)
			{
				nNumOfPixelsTurnedToBlackf += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n Black: nNumOfPixelsTurnedToBlackf = %d, iWidf = %d, iLenf = %d, iIterGlob = %d", nNumOfPixelsTurnedToWhitef, iWidf, iLenf, iIterGlob);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			}//else if (nPixelShadedToWhiteOrBlackf == -1)

		} //for (iLenf = nLen_ToLeftOfBoundaryCurf; iLenf <= nLen_ToRightOfBoundaryCurf; iLenf++)

	} // for (iWidf = nWid_BlackPixelsBehindBoundaryMinf; iWidf <= nWid_BlackPixelsBehindBoundaryMaxf; iWidf++)

	nNumOfChangedPixelsBehindBoundaryTotf = nNumOfPixelsTurnedToWhitef + nNumOfPixelsTurnedToBlackf;

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n The end in 'MedianFiterTo_BlackAreaBehindBoundary': nNumOfPixelsTurnedToWhitef = %d, nNumOfPixelsTurnedToBlackf = %d", nNumOfPixelsTurnedToWhitef, nNumOfPixelsTurnedToBlackf);
	fprintf(fout_lr, "\n\n The end in 'MedianFiterTo_BlackAreaBehindBoundary': nNumOfPixelsTurnedToWhitef = %d, nNumOfPixelsTurnedToBlackf = %d", nNumOfPixelsTurnedToWhitef, nNumOfPixelsTurnedToBlackf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	return SUCCESSFUL_RETURN;
}//int MedianFiterTo_BlackAreaBehindBoundary(...

 ////////////////////////////////////////////////////////////////////
int RangeOfWidthsWithBlackPixelsBehindBoundary(

	int &nWidToStartForBlackAreaBehindBoundaryf,

	int &nWid_BlackPixelsBehindBoundaryMinf,
	int &nWid_BlackPixelsBehindBoundaryMaxf,

	MASK_IMAGE *sMask_Imagef)
{
	int
		nLenOfObjectBoundaryCurf,

		nIndexOfPixelCurf,

		nLineHasBlackPixelsBehindBoundaryf,

		nNumOfInitLinesWithNoBlackPixelssBehindBoundaryf = 0,

		nLineWithBlackPixelsBehindBoundary_AlreadyFoundf = 0, //not initially

		iLenInitf,
		iLenFinf,
		iWidf,
		iLenf;

	nWid_BlackPixelsBehindBoundaryMinf = -1; //not set up yet
	nWid_BlackPixelsBehindBoundaryMaxf = -1; // sMask_Imagef->nWidth;

	for (iWidf = nWidToStartForBlackAreaBehindBoundaryf; iWidf < sMask_Imagef->nWidth; iWidf++)
	{
		nLenOfObjectBoundaryCurf = sMask_Imagef->nLenObjectBoundary_Arr[iWidf];

		nLineHasBlackPixelsBehindBoundaryf = 0; //no black pixels behind 

												//fprintf(fout_lr, "\n The boundary: iWidf = %d, nLenOfObjectBoundaryCurf = %d", iWidf, nLenOfObjectBoundaryCurf);
		if (sMask_Imagef->nSideOfObjectLocation == -1) //left
		{
			iLenFinf = nLenOfObjectBoundaryCurf - (nRadiusOfMedianFilter*nCoeff_DeviationFromBoundaryForMedianFilter_Into);
			for (iLenf = 0; iLenf < iLenFinf; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sMask_Imagef->nLength);

				if (sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCurf] == 0)
				{
					nLineHasBlackPixelsBehindBoundaryf = 1; //there are black pixels behind 

					nLineWithBlackPixelsBehindBoundary_AlreadyFoundf = 1; //found

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n 'RangeOfWidthsWithBlackPixelsBehindBoundary' (left): black pixel at iWidf = %d, iLenf = %d, nLenOfObjectBoundaryCurf = %d", iWidf, iLenf, nLenOfObjectBoundaryCurf);
					fprintf(fout_lr, "\n iIterGlob = %d", iIterGlob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					if (nWid_BlackPixelsBehindBoundaryMinf == -1)
					{
						nWid_BlackPixelsBehindBoundaryMinf = iWidf;
					} //if (nWid_BlackPixelsBehindBoundaryMinf == -1)

					if (nWid_BlackPixelsBehindBoundaryMinf != -1 && iWidf > nWid_BlackPixelsBehindBoundaryMaxf)
					{
						nWid_BlackPixelsBehindBoundaryMaxf = iWidf;

					} //if (nWid_BlackPixelsBehindBoundaryMinf != -1 && iWidf > nWid_BlackPixelsBehindBoundaryMaxf)

					break;  // for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)
				} //if (sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCurf] == 0)

			} //for (iLenf = 0; iLenf < iLenFinf; iLenf++)

			if ((nLineHasBlackPixelsBehindBoundaryf == 0 || iWidf == sMask_Imagef->nWidth - 1) && nWid_BlackPixelsBehindBoundaryMinf != -1 && nWid_BlackPixelsBehindBoundaryMaxf != -1)
			{
				nWidToStartForBlackAreaBehindBoundaryf = nWidToStartForBlackAreaBehindBoundaryf + nNumOfInitLinesWithNoBlackPixelssBehindBoundaryf;

				return SUCCESSFUL_RETURN;
			}// if ( (nLineHasBlackPixelsBehindBoundaryf == 0 || iWidf ==  sMask_Imagef->nWidth - 1) && nWid_BlackPixelsBehindBoundaryMinf != -1 && nWid_BlackPixelsBehindBoundaryMaxf != -1)

		} //if (sMask_Imagef->nSideOfObjectLocation == -1)
		else if (sMask_Imagef->nSideOfObjectLocation == 1) // right
		{
			//fprintf(fout_lr, "\n The boundary: iWidf = %d, nLenOfObjectBoundaryCurf = %d", iWidf, nLenOfObjectBoundaryCurf);
			iLenInitf = nLenOfObjectBoundaryCurf + (nRadiusOfMedianFilter*nCoeff_DeviationFromBoundaryForMedianFilter_Into);
			for (iLenf = iLenInitf; iLenf < sMask_Imagef->nLength - 1; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sMask_Imagef->nLength);

				if (sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCurf] == 0)
				{
					nLineHasBlackPixelsBehindBoundaryf = 1; //there are black pixels behind 

					nLineWithBlackPixelsBehindBoundary_AlreadyFoundf = 1;

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n 'RangeOfWidthsWithBlackPixelsBehindBoundary' (right): black pixel at iWidf = %d, iLenf = %d, nLenOfObjectBoundaryCurf = %d", iWidf, iLenf, nLenOfObjectBoundaryCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					if (nWid_BlackPixelsBehindBoundaryMinf == -1)
					{
						nWid_BlackPixelsBehindBoundaryMinf = iWidf;
					} //if (nWid_BlackPixelsBehindBoundaryMinf == -1)

					if (nWid_BlackPixelsBehindBoundaryMinf != -1 && iWidf > nWid_BlackPixelsBehindBoundaryMaxf)
					{
						nWid_BlackPixelsBehindBoundaryMaxf = iWidf;

					} //if (nWid_BlackPixelsBehindBoundaryMinf != -1 && iWidf > nWid_BlackPixelsBehindBoundaryMaxf)

					break;  // for (iLenf = sMask_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryCurf; iLenf--)
				} //if (sMask_Imagef->nMaskImage_Arr[nIndexOfPixelCurf] == 0)

			} //for (iLenf = iLenInitf; iLenf < sMask_Imagef->nLength - 1; iLenf++)

			if ((nLineHasBlackPixelsBehindBoundaryf == 0 || iWidf == sMask_Imagef->nWidth - 1) && nWid_BlackPixelsBehindBoundaryMinf != -1 && nWid_BlackPixelsBehindBoundaryMaxf != -1)
			{
				nWidToStartForBlackAreaBehindBoundaryf = nWidToStartForBlackAreaBehindBoundaryf + nNumOfInitLinesWithNoBlackPixelssBehindBoundaryf;

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n 'RangeOfWidthsWithBlackPixelsBehindBoundary': exit at iWidf = %d, nWid_BlackPixelsBehindBoundaryMinf = %d, nWid_BlackPixelsBehindBoundaryMaxf = %d",
					iWidf, nWid_BlackPixelsBehindBoundaryMinf, nWid_BlackPixelsBehindBoundaryMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return SUCCESSFUL_RETURN;
			}// if ( (nLineHasBlackPixelsBehindBoundaryf == 0 || iWidf ==  sMask_Imagef->nWidth - 1) && nWid_BlackPixelsBehindBoundaryMinf != -1 && nWid_BlackPixelsBehindBoundaryMaxf != -1)

		} //if (sMask_Imagef->nSideOfObjectLocation == 1) //right

		if (nLineWithBlackPixelsBehindBoundary_AlreadyFoundf == 0 && nLineHasBlackPixelsBehindBoundaryf == 0)
		{
			nNumOfInitLinesWithNoBlackPixelssBehindBoundaryf += 1;
		} // if (nLineWithBlackPixelsBehindBoundary_AlreadyFoundf == 0 && nLineHasBlackPixelsBehindBoundaryf == 0)

	} // for (iWidf = nWidToStartForBlackAreaBehindBoundaryf; iWidf <  sMask_Imagef->nWidth; iWidf++)

	return (-2);
}//int RangeOfWidthsWithBlackPixelsBehindBoundary(...
 //printf("\n\n Please press any key:"); getchar();






